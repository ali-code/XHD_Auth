﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>

    <link rel="stylesheet" type="text/css" href="../css/imgareaselect-default.css" />
    <link href="../lib/ligerUI/skins/touch/css/ligerui-all.css" rel="stylesheet" />
    <link href="../CSS/input.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script src="../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.imgareaselect.pack.js"></script>
    <script src="../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../JS/ajaxUpload.js" type="text/javascript"></script>
    <script src="../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        function preview(img, selection) {
            if (!selection.width || !selection.height)
                return;

            var scaleX = 120 / selection.width;
            var scaleY = 120 / selection.height;

            $('#preview1 img').css({
                width: Math.round(scaleX * $("#photo").width()),
                height: Math.round(scaleY * $("#photo").height()),
                marginLeft: -Math.round(scaleX * selection.x1),
                marginTop: -Math.round(scaleY * selection.y1)
            });

            $('#x1').val(selection.x1);
            $('#y1').val(selection.y1);
            $('#x2').val(selection.x2);
            $('#y2').val(selection.y2);
            $('#w').val(selection.width);
            $('#h').val(selection.height);
        }

        $(function () {
            //preview($("#photo"), {"x1":50,"y1":50,"x2":170,"y2":170,"width":120,"height":120});
            //api.cancelSelection();

        });
        function checkpath() {
            var path = $("#upload").val();
            $.ligerDialog.confirm("文件已选择，是否开始导入？", function (yes) {
                if (yes) {
                    $.ligerDialog.waitting('数据导入中,请稍候...');
                    ajaxUpload({
                        id: 'upload',
                        frameName: 'a',
                        url: 'upload.upfiles.xhd?ftype=tmp_headimg',
                        format: ['jpg', 'png', 'gif', 'bmp'],
                        onsuccess: success,
                        onerror: onerror
                    });
                }
            });
        }
        var imagename = "";
        function success(serverData) {
            $.ligerDialog.closeWaitting();

            var start = serverData.indexOf(">");
            if (start != -1) {
                var end = serverData.indexOf("<", start + 1);
                if (end != -1) {
                    serverData = serverData.substring(start + 1, end);
                }
            }

            imagename = serverData;
            $("#photo").attr("src", "../images/upload/temp/" + serverData);
            $('#preview1 img').attr("src", "../images/upload/temp/" + serverData);

            api = $('#photo').imgAreaSelect({
                instance: true,
                aspectRatio: '1:1',
                handles: true,
                fadeSpeed: 0,
                onSelectChange: preview,
                show: true,
                x1: 50, y1: 50, x2: 170, y2: 170
            });
            preview($("#photo"), { "x1": 50, "y1": 50, "x2": 170, "y2": 170, "width": 120, "height": 120 });
        }

        function f_save() {
            var src = $('#preview1 img').attr("src");
            if (src == '../images/noheadimage.jpg') {
                alert("请先上传图片");
                return;
            }
            else if ($("#x1").val() == "-" || $("#y1").val() == "-" || $("#x2").val() == "-" || $("#y2").val() == "-" || $("#w").val() == "-" || $("#h").val() == "-") {
                alert("请先选择头像");
                return;
            }
            else {
                var sendtxt = "&txtFileName=" + imagename;
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }
        function onerror(txt) {
            $.ligerDialog.closeWaitting();
            $.ligerDialog.error(txt);
        }
    </script>
    <style type="text/css">
        fieldset { padding: 8px; }
        legend { font-size: 12px; margin-left: 15px; }
        body { font-size: 12px; }

        input { position: absolute; right: 0; top: 0; font-size: 100px; opacity: 0; filter: alpha(opacity=0); }
        .fileInput { position: absolute; left: 0; top: 0; height: 30px; filter: alpha(opacity=0); opacity: 0; background-color: transparent; cursor: pointer; }
        .btn { width: 200px; height: 30px; margin: 10px; background-color: yellow; text-align: center; line-height: 30px; overflow: hidden; display: block; position: relative; box-shadow: 0 0 5px rgba(0,0,0,0.3); border-radius: 3px; text-shadow: 1px 1px 1px #fff; }
    </style>
</head>
<body>
    <form id="form1">
        <div class="container demo">
            <fieldset>
                <legend>原图</legend>
                <div style="float: left; width: 500px;">

                    <div class="frame" style="margin: 0 0.3em; width: 485px; height: 325px; overflow: auto; z-index: 100">
                        <img id="photo" src="../images/noheaderlarger.png" height="300" />
                    </div>

                    <a href="#" class="uploada">
                        <input type="file" class="fileInput" id="upload" name="fileInput" onchange="checkpath()" accept=".jpg,.bmp,.png,.gif" /> 浏  览
                    </a>


                </div>

        <div style="float: left; width: 200px;">

            <div class="frame" style="margin: 0 1em; width: 120px; height: 120px;">
                <div id="preview1" style="width: 120px; height: 120px; overflow: hidden;">
                    <img src="../images/noheaderlarger.png" style="width: 120px; height: 120px;" />
                </div>
            </div>


            <input type="hidden" id="x1" name="x1" value="-" />
            <input type="hidden" id="y1" name="y1" value="-" />
            <input type="hidden" id="x2" name="x2" value="-" />
            <input type="hidden" id="y2" name="y2" value="-" />
            <input type="hidden" id="w" name="w" value="-" />
            <input type="hidden" id="h" name="h" value="-" />
            <div style="line-height: 30px;">

                <br />
                操作步骤：<br />
                1、点击上传头像按钮。<br />
                2、拖动选择框至合适位置，并选择合适大小，右边会有预览。<br />
                3、点击保存。
                   
            </div>
        </div>
        </fieldset>
        </div>
    </form>
</body>
</html>
