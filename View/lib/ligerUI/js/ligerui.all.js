﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
if (typeof (LigerUIManagers) == "undefined") LigerUIManagers = {};
(function($)
{ 
    ///	<param name="$" type="jQuery"></param>

    $.fn.ligerGetAccordionManager = function()
    {
        return LigerUIManagers[this[0].id + "_Accordion"];
    };
    $.fn.ligerRemoveAccordionManager = function()
    {
        return this.each(function()
        {
            LigerUIManagers[this.id + "_Accordion"] = null;
        });
    };

    $.fn.ligerAccordion = function(p)
    { 
        this.each(function()
        {
            p = $.extend({
                height: null,
                speed : "normal",
                changeHeightOnResize: false,
                heightDiff: 0 // 高度补差  
            }, p || {});
            
            //if (this.usedAccordion) return;
            var g = {
                onResize: function()
                {
                    if (!p.height || typeof (p.height) != 'string' || p.height.indexOf('%') == -1) return false;
                    //set accordion height
                    if (g.accordion.parent()[0].tagName.toLowerCase() == "body")
                    {
                        var windowHeight = $(window).height();
                        windowHeight -= parseInt(g.layout.parent().css('paddingTop'));
                        windowHeight -= parseInt(g.layout.parent().css('paddingBottom'));
                        g.height = p.heightDiff + windowHeight * parseFloat(g.height) * 0.01;
                    }
                    else
                    {
                        g.height = p.heightDiff + (g.accordion.parent().height() * parseFloat(p.height) * 0.01);
                    }
                    g.accordion.height(g.height);
                    g.setContentHeight(g.height - g.headerHoldHeight);
                },
                setHeight: function(height)
                {
                    g.accordion.height(height);
                    height -= g.headerHoldHeight;
                    $("> .l-accordion-content", g.accordion).height(height + 1);
                },
                render: function () {
                    this.usedAccordion = false;
                    //g.accordion = $(this);
                    
                    if (!g.accordion.hasClass("l-accordion-panel")) g.accordion.addClass("l-accordion-panel");
                    var selectedIndex = 0;
                    if ($("> div[lselected=true]", g.accordion).length > 0)
                        selectedIndex = $("> div", g.accordion).index($("> div[lselected=true]", g.accordion));

                    $("> div", g.accordion).each(function (i, box) {
                        var header = $('<div class="l-accordion-header"><div class="l-accordion-toggle"></div><div class="l-accordion-header-inner"></div></div>');
                        if (i == selectedIndex)
                            $(".l-accordion-toggle", header).addClass("l-accordion-toggle-open");
                        if ($(box).attr("title")) {
                            $(".l-accordion-header-inner", header).html($(box).attr("title"));
                            $(box).attr("title", "");
                        }
                        $(box).before(header);
                        if (!$(box).hasClass("l-accordion-content")) $(box).addClass("l-accordion-content");
                        //alert(i);
                    });

                    //add Even
                    $(".l-accordion-toggle", g.accordion).each(function () {
                        if (!$(this).hasClass("l-accordion-toggle-open") && !$(this).hasClass("l-accordion-toggle-close")) {
                            $(this).addClass("l-accordion-toggle-close");
                        }
                        if ($(this).hasClass("l-accordion-toggle-close")) {
                            $(this).parent().next(".l-accordion-content:visible").hide();
                        }
                    });
                    $(".l-accordion-header", g.accordion).hover(function () {
                        $(this).addClass("l-accordion-header-over");
                    }, function () {
                        $(this).removeClass("l-accordion-header-over");
                    });
                    $(".l-accordion-toggle", g.accordion).hover(function () {
                        if ($(this).hasClass("l-accordion-toggle-open"))
                            $(this).addClass("l-accordion-toggle-open-over");
                        else if ($(this).hasClass("l-accordion-toggle-close"))
                            $(this).addClass("l-accordion-toggle-close-over");
                    }, function () {
                        if ($(this).hasClass("l-accordion-toggle-open"))
                            $(this).removeClass("l-accordion-toggle-open-over");
                        else if ($(this).hasClass("l-accordion-toggle-close"))
                            $(this).removeClass("l-accordion-toggle-close-over");
                    });
                    $(">.l-accordion-header", g.accordion).click(function () {
                        var togglebtn = $(".l-accordion-toggle:first", this);
                        if (togglebtn.hasClass("l-accordion-toggle-close")) {
                            togglebtn.removeClass("l-accordion-toggle-close")
                            .removeClass("l-accordion-toggle-close-over l-accordion-toggle-open-over")
                            togglebtn.addClass("l-accordion-toggle-open");
                            $(this).next(".l-accordion-content")
                            .show(p.speed)
                            .siblings(".l-accordion-content:visible").hide(p.speed);
                            $(this).siblings(".l-accordion-header").find(".l-accordion-toggle").removeClass("l-accordion-toggle-open").addClass("l-accordion-toggle-close");
                        }
                        //else
                        //{
                        //    togglebtn.removeClass("l-accordion-toggle-open")
                        //    .removeClass("l-accordion-toggle-close-over l-accordion-toggle-open-over")
                        //    .addClass("l-accordion-toggle-close");
                        //    $(this).next(".l-accordion-content").hide(p.speed);
                        //}
                    });
                    //init
                    g.headerHoldHeight = 0;
                    $("> .l-accordion-header", g.accordion).each(function () {
                        g.headerHoldHeight += $(this).height() + 1;
                    });
                    if (p.height && typeof (p.height) == 'string' && p.height.indexOf('%') > 0) {
                        g.onResize();
                        if (p.changeHeightOnResize) {
                            $(window).resize(function () {
                                g.onResize();
                            });
                        }
                    }
                    else {
                        if (p.height) {
                            g.height = p.heightDiff + p.height;
                            g.accordion.height(g.height);
                            g.setHeight(p.height);
                        }
                        else {
                            g.header = g.accordion.height();
                        }
                    }
                    
                    if (this.id == undefined) this.id = "LigerUI_" + new Date().getTime();
                    LigerUIManagers[this.id + "_Accordion"] = g;
                    this.usedAccordion = true;
                }
            };
            g.accordion = $(this);
            if (!g.accordion.hasClass("l-accordion-panel")) g.accordion.addClass("l-accordion-panel");
            var selectedIndex = 0;
            if ($("> div[lselected=true]", g.accordion).length > 0)
                selectedIndex = $("> div", g.accordion).index($("> div[lselected=true]", g.accordion));

            $("> div", g.accordion).each(function(i, box)
            {
                var header = $('<div class="l-accordion-header"><div class="l-accordion-toggle"></div><div class="l-accordion-header-inner"></div></div>');
                if (i == selectedIndex)
                    $(".l-accordion-toggle", header).addClass("l-accordion-toggle-open");
                if ($(box).attr("title"))
                {
                    $(".l-accordion-header-inner", header).html($(box).attr("title"));
                    $(box).attr("title","");
                }
                $(box).before(header);
                if (!$(box).hasClass("l-accordion-content")) $(box).addClass("l-accordion-content");
            });

            //add Even
            $(".l-accordion-toggle", g.accordion).each(function()
            {
                if (!$(this).hasClass("l-accordion-toggle-open") && !$(this).hasClass("l-accordion-toggle-close"))
                {
                    $(this).addClass("l-accordion-toggle-close");
                }
                if ($(this).hasClass("l-accordion-toggle-close"))
                {
                    $(this).parent().next(".l-accordion-content:visible").hide();
                }
            });
            $(".l-accordion-header", g.accordion).hover(function()
            {
                $(this).addClass("l-accordion-header-over");
            }, function()
            {
                $(this).removeClass("l-accordion-header-over");
            });
            $(".l-accordion-toggle", g.accordion).hover(function()
            {
                if ($(this).hasClass("l-accordion-toggle-open"))
                    $(this).addClass("l-accordion-toggle-open-over");
                else if ($(this).hasClass("l-accordion-toggle-close"))
                    $(this).addClass("l-accordion-toggle-close-over");
            }, function()
            {
                if ($(this).hasClass("l-accordion-toggle-open"))
                    $(this).removeClass("l-accordion-toggle-open-over");
                else if ($(this).hasClass("l-accordion-toggle-close"))
                    $(this).removeClass("l-accordion-toggle-close-over");
            });
            $(">.l-accordion-header", g.accordion).click(function()
            {
                var togglebtn = $(".l-accordion-toggle:first",this);
                if (togglebtn.hasClass("l-accordion-toggle-close"))
                {
                    togglebtn.removeClass("l-accordion-toggle-close")
                    .removeClass("l-accordion-toggle-close-over l-accordion-toggle-open-over") 
                    togglebtn.addClass("l-accordion-toggle-open");
                    $(this).next(".l-accordion-content")
                    .show(p.speed)
                    .siblings(".l-accordion-content:visible").hide(p.speed);
                    $(this).siblings(".l-accordion-header").find(".l-accordion-toggle").removeClass("l-accordion-toggle-open").addClass("l-accordion-toggle-close");
                }
                //else
                //{
                //    togglebtn.removeClass("l-accordion-toggle-open")
                //    .removeClass("l-accordion-toggle-close-over l-accordion-toggle-open-over") 
                //    .addClass("l-accordion-toggle-close");
                //    $(this).next(".l-accordion-content").hide(p.speed);
                //}
            });
            //init
            g.headerHoldHeight = 0;
            $("> .l-accordion-header", g.accordion).each(function()
            {
                // g.headerHoldHeight += $(this).height() + 1;
                g.headerHoldHeight += 25;
            });
            if (p.height && typeof (p.height) == 'string' && p.height.indexOf('%') > 0)
            {
                g.onResize();
                if (p.changeHeightOnResize)
                {
                    $(window).resize(function()
                    {
                        g.onResize();
                    });
                }
            }
            else
            {
                if (p.height)
                {
                    g.height = p.heightDiff + p.height;
                    g.accordion.height(g.height);
                    g.setHeight(p.height);
                }
                else
                {
                    g.header = g.accordion.height();
                }
            }

            if (this.id == undefined) this.id = "LigerUI_" + new Date().getTime();
            LigerUIManagers[this.id + "_Accordion"] = g;
            this.usedAccordion = true; 
        });
        if (this.length == 0) return null;
        if (this.length == 1) return LigerUIManagers[this[0].id + "_Accordion"];
        var managers = [];
        this.each(function() {
            managers.push(LigerUIManagers[this.id + "_Accordion"]);
        });
        return managers;
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
if (typeof (LigerUIManagers) == "undefined") LigerUIManagers = {};
(function ($) {
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager) {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr) {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;


    $.fn.ligerGetButtonManager = function () {
        return $.ligerui.getManager(this);
    };

    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Button = { width: 70, text: 'Button', disabled: false };

    //Button manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Button = function (options, po) {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.Button.prototype = {
        setValue: function (text) {
            this.options.text = text;
            $("span", this.button).html(text);
        },
        getValue: function () {
            return this.options.text;
        },
        setEnabled: function () {
            this.button.removeClass("l-btn-disabled");
            this.options.disabled = false;
        },
        setDisabled: function () {
            this.button.addClass("l-btn-disabled");
            this.options.disabled = true;
        }
    };
    $.fn.ligerButton = function (options) {
        this.each(function () {
            if (this.applyligerui) return;
            var p = $.extend({}, $.ligerDefaults.Button, options || {});
            var po = {};
            var g = new $.ligerManagers.Button(p, po);
            g.button = $(this);
            if (!g.button.hasClass("l-btn")) g.button.addClass("l-btn");
            if (p.width) g.button.width(p.width);
            if (p.disabled) g.setDisabled();
            p.text && g.button.append('<span class="l-btn-label">' + p.text + '</span>');

            if (p.icon) {
                $(".l-btn-label", g.button).append("<div class='l-btn-icon'></div>");
                $(".l-btn-icon", g.button).css({ "background": "url(" + p.icon + ") no-repeat 1px 1px", width: "18px", height: "18px" });
                $(".l-btn-label", g.button).addClass("l-btn-hasicon");
            }

            //g.button.append('<div class="l-btn-l"></div><div class="l-btn-r"></div>');

            //设置事件
            $(".l-btn").hover(function () {
                $(this).addClass("l-btn-over");
            }, function () {
                $(this).removeClass("l-btn-over");
            });

            p.click && g.button.click(function () {
                if (!p.disabled)
                    p.click();
            });
            $.ligerui.addManager(this, g);
        });
        return $.ligerui.getManager(this);
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($)
{
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager)
    {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr)
    {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.fn.ligerGetCheckBoxManager = function ()
    {
        return $.ligerui.getManager(this);
    };

    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.CheckBox = { disabled: false };

    //CheckBox manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.CheckBox = function (options, po)
    {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.CheckBox.prototype = {
        setValue: function (value)
        {
            var g = this;
            if (!value)
            {
                g.input[0].checked = false;
                g.link.removeClass('l-checkbox-checked');
            }
            else
            {
                g.input[0].checked = true;
                g.link.addClass('l-checkbox-checked');
            }
        },
        getValue: function ()
        {
            return this.input[0].checked;
        },
        setEnabled: function ()
        {
            this.input.attr('disabled', false);
            this.wrapper.removeClass("l-disabled");
            this.options.disabled = false;
        },
        setDisabled: function ()
        {
            this.input.attr('disabled', true);
            this.wrapper.addClass("l-disabled");
            this.options.disabled = true;
        },
        updateStyle: function ()
        { 
            if (this.input.attr('disabled'))
            {
                this.wrapper.addClass("l-disabled");
                this.options.disabled = true;
            }
            if (this.input[0].checked)
            {
                this.link.addClass('l-checkbox-checked');
            }
            else
            {
                this.link.removeClass('l-checkbox-checked');
            }
        }
    };

    $.fn.ligerCheckBox = function (options)
    {
        this.each(function ()
        {
            if (this.applyligerui) return;
            var p = $.extend({}, $.ligerDefaults.CheckBox, options || {});
            var po = {};
            var g = new $.ligerManagers.CheckBox(p, po);
            g.input = $(this);
            g.link = $('<a class="l-checkbox"></a>');
            g.wrapper = g.input.addClass('l-hidden').wrap('<div class="l-checkbox-wrapper"></div>').parent();
            g.wrapper.prepend(g.link);
            if (p.css) g.wrapper.css(p.css);
            g.link.click(function ()
            {
                if (g.input.attr('disabled')) { return false; }
                if (p.disabled) return false;
                if (p.onBeforeClick)
                {
                    if (!p.onBeforeClick(g.input[0]))
                        return false;
                }
                if ($(this).hasClass("l-checkbox-checked"))
                {
                    g.setValue(false);
                }
                else
                {
                    g.setValue(true);
                }
                g.input.trigger("change");
            });
            g.wrapper.hover(function ()
            {
                if (!p.disabled)
                    $(this).addClass("l-over");
            }, function ()
            {
                $(this).removeClass("l-over");
            });
            this.checked && g.link.addClass('l-checkbox-checked');
            $.ligerui.addManager(this, g);
        });
        return $.ligerui.getManager(this);
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
if (typeof (LigerUIManagers) == "undefined") LigerUIManagers = {};
(function ($) {
    $.fn.ligerGetComboBoxManager = function () {
        return LigerUIManagers[this[0].id + "_ComboBox"];
    };
    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.ComboBox = {
        resize: true,           //是否调整大小
        isMultiSelect: false,   //是否多选
        isShowCheckBox: false,  //是否选择复选框
        columns: false,       //表格状态
        selectBoxWidth: false, //宽度
        selectBoxHeight: false, //高度
        onBeforeSelect: false, //选择前事件
        onSelected: null, //选择值事件 
        initValue: null,
        initText: null,
        valueField: 'id',
        textField: 'text',
        valueFieldID: null,
        slide: true,           //是否以动画的形式显示
        split: ";",
        data: null,
        tree: null,            //下拉框以树的形式显示，tree的参数跟LigerTree的参数一致 
        treeLeafOnly: true,   //是否只选择叶子
        grid: null,              //表格
        onStartResize: null,
        onEndResize: null,
        hideOnLoseFocus: true,
        url: null,              //数据源URL(需返回JSON)
        onSuccess: null,
        onError: null,
        onBeforeOpen: null,      //打开下拉框前事件，可以通过return false来阻止继续操作，利用这个参数可以用来调用其他函数，比如打开一个新窗口来选择值
        render: null,            //文本框显示html函数
        emptyText: null,       //空行
        readonly:false
    };
    ///	<param name="$" type="jQuery"></param>
    $.fn.ligerComboBox = function (options) {
        this.each(function () {
            if (this.usedComboBox) return;
            var p = $.extend({}, options || {});
            if ($(this).attr("ligerui")) {
                try {
                    var attroptions = $(this).attr("ligerui");
                    if (attroptions.indexOf('{') < 0) attroptions = "{" + attroptions + "}";
                    eval("attroptions = " + attroptions + ";");
                    if (attroptions) p = $.extend({}, attroptions, p || {});
                }
                catch (e) {
                }
            }
            p = $.extend({}, $.ligerDefaults.ComboBox, p);
            if (p.columns) {
                p.isShowCheckBox = true;
            }
            if (p.isMultiSelect) {
                p.isShowCheckBox = true;
            }
            if (this.id == undefined) this.id = "LigerUI_" + new Date().getTime();

            var g = {
                //add function()
                setReadOnly:function()
                {
                    p.readonly = true;
                    g.wrapper.addClass('l-text-disabled');
                },
                //add function()
                setUnReadOnly: function () {
                    p.readonly = false;
                    g.wrapper.removeClass('l-text-disabled');
                },
                getDataByUrl:function(url){
                    if(url)
                        p.url=url;
                    g.clearContent();
                    g.bulidContent();  
                },
                //查找Text,适用多选和单选
                findTextByValue: function (value) {
                    if (value == undefined) return "";
                    var texts = "";
                    var contain = function (checkvalue) {
                        var targetdata = value.toString().split(p.split);
                        for (var i = 0; i < targetdata.length; i++) {
                            if (targetdata[i] == checkvalue) return true;
                        }
                        return false;
                    };
                    $(g.data).each(function (i, item) {
                        var val = item[p.valueField];
                        var txt = item[p.textField];
                        if (contain(val)) {
                            texts += txt + p.split;
                        }
                    });
                    if (texts.length > 0) texts = texts.substr(0, texts.length - 1);
                    return texts;
                },
                //查找Value,适用多选和单选
                findValueByText: function (text) {
                    if (!text && text == "") return "";
                    var contain = function (checkvalue) {
                        var targetdata = text.toString().split(p.split);
                        for (var i = 0; i < targetdata.length; i++) {
                            if (targetdata[i] == checkvalue) return true;
                        }
                        return false;
                    };
                    var values = "";
                    $(g.data).each(function (i, item) {
                        var val = item[p.valueField];
                        var txt = item[p.textField];
                        if (contain(txt)) {
                            values += val + p.split;
                        }
                    });
                    if (values.length > 0) values = values.substr(0, values.length - 1);
                    return values;
                },
                removeItem: function () {
                },
                insertItem: function () {
                },
                addItem: function () {

                },
                selectValue: function (value,newid) {
                    var text = g.findTextByValue(value);                    
                    //传递参数值
                    g.newid = newid;

                    if (p.tree) {
                        g.selectValueByTree(value);
                    }
                    else if (!p.isMultiSelect) {                        
                        po.changeValue(value, text, newid);
                        $("tr[value=" + value + "] td", g.selectBox).addClass("l-selected");
                        $("tr[value!=" + value + "] td", g.selectBox).removeClass("l-selected");
                    }
                    else {
                        po.changeValue(value, text,newid);
                        var targetdata = value.toString().split(p.split);
                        $("table.l-table-checkbox :checkbox", g.selectBox).each(function () { this.checked = false; });
                        for (var i = 0; i < targetdata.length; i++) {
                            $("table.l-table-checkbox tr[value=" + targetdata[i] + "] :checkbox", g.selectBox).each(function () { this.checked = true; });
                        }
                    }
                },
                bulidContent: function () {
                    this.clearContent();
                    if (g.select) {
                        g.setSelect();
                    }
                    else if (g.data) {
                        g.setData(g.data);
                    }
                    else if (p.tree) {
                        g.setTree(p.tree);
                    }
                    else if (p.grid) {
                        g.setGrid(p.grid);
                    }
                    else if (p.url) {

                        $.ajax({
                            type: 'post',
                            url: p.url,
                            cache: false,
                            dataType: 'json',
                            success: function (data) {
                                g.data = data;
                                g.setData(g.data);
                                if (p.onSuccess) p.onSuccess(g.data);
                            },
                            error: function (XMLHttpRequest, textStatus) {
                                if (p.onError) p.onError(XMLHttpRequest, textStatus);
                            }
                        });
                    }
                },
                clearContent: function () {
                    $("table", g.selectBox).html("");
                    g.inputText.val("");
                    g.valueField.val("");
                },
                setSelect: function () {
                    this.clearContent();
                    $('option', g.select).each(function (i) {
                        var val = $(this).val();
                        var txt = $(this).html();
                        var tr = $("<tr><td index='" + i + "' value='" + val + "'>" + txt + "</td>");
                        $("table.l-table-nocheckbox", g.selectBox).append(tr);
                        $("td", tr).hover(function () {
                            $(this).addClass("l-over");
                        }, function () {
                            $(this).removeClass("l-over");
                        });
                    });
                    $('td:eq(' + g.select[0].selectedIndex + ')', g.selectBox).each(function () {
                        if ($(this).hasClass("l-selected")) {
                            g.selectBox.hide();
                            return;
                        }
                        $(".l-selected", g.selectBox).removeClass("l-selected");
                        $(this).addClass("l-selected");
                        if (g.select[0].selectedIndex != $(this).attr('index') && g.select[0].onchange) {
                            g.select[0].selectedIndex = $(this).attr('index'); g.select[0].onchange();
                        }
                        var newIndex = parseInt($(this).attr('index'));
                        g.select[0].selectedIndex = newIndex;
                        g.select.trigger("change");
                        g.selectBox.hide();
                        g.inputText.val($(this).html());
                        //init:

                        //alert($(this).val() + "," + $(this).html());
                        //po.changeValue($(this).val(),$(this).html());
                    });
                    po.addClickEven();
                },
                setData: function (data) {
                    this.clearContent();
                    if (!data || !data.length) return;
                    if (g.data != data) g.data = data;
                    if (p.columns) {
                        g.selectBox.table.headrow = $("<tr class='l-table-headerow'><td width='18px'></td></tr>");
                        g.selectBox.table.append(g.selectBox.table.headrow);
                        g.selectBox.table.addClass("l-box-select-grid");
                        for (var j = 0; j < p.columns.length; j++) {
                            var headrow = $("<td columnindex='" + j + "' columnname='" + p.columns[j].name + "'>" + p.columns[j].header + "</td>");
                            if (p.columns[j].width) {
                                headrow.width(p.columns[j].width);
                            }
                            g.selectBox.table.headrow.append(headrow);

                        }
                    }
                    if (p.emptyText && !g.emptyRow && (data.length == 0 || data[0][p.textField] != p.emptyText)) {                       
                        g.emptyRow = {};
                        g.emptyRow[p.textField] = p.emptyText;
                        g.emptyRow[p.valueField] = null;
                        g.data.splice(0, 0, g.emptyRow);
                    }
                    for (var i = 0; i < data.length; i++) {
                        var val = data[i][p.valueField];
                        var txt = data[i][p.textField];
                        if (!p.columns) {
                            $("table.l-table-checkbox", g.selectBox).append("<tr value='" + val + "'><td style='width:18px;'  index='" + i + "' value='" + val + "' text='" + txt + "' ><input type='checkbox' /></td><td index='" + i + "' value='" + val + "' align='left'>" + txt + "</td>");
                            $("table.l-table-nocheckbox", g.selectBox).append("<tr value='" + val + "'><td index='" + i + "' value='" + val + "' align='left'>" + txt + "</td>");
                        } else {
                            var tr = $("<tr value='" + val + "'><td style='width:18px;'  index='" + i + "' value='" + val + "' text='" + txt + "' ><input type='checkbox' /></td></tr>");
                            $("td", g.selectBox.table.headrow).each(function () {
                                var columnname = $(this).attr("columnname");
                                if (columnname) {
                                    var td = $("<td>" + data[i][columnname] + "</td>");
                                    tr.append(td);
                                }
                            });
                            g.selectBox.table.append(tr);
                        }
                    }
                    //自定义复选框支持
                    if (p.isShowCheckBox && $.fn.ligerCheckBox) {
                        $("table input:checkbox", g.selectBox).ligerCheckBox();
                    }
                    $(".l-table-checkbox input:checkbox", g.selectBox).change(function () {
                        if (this.checked && p.onBeforeSelect) {
                            var parentTD = null;
                            if ($(this).parent().get(0).tagName.toLowerCase() == "div") {
                                parentTD = $(this).parent().parent();
                            } else {
                                parentTD = $(this).parent();
                            }
                            if (parentTD != null && !p.onBeforeSelect(parentTD.attr("value"), parentTD.attr("text"))) {
                                g.selectBox.slideToggle("fast");
                                return false;
                            }
                        }
                        if (!p.isMultiSelect) {
                            if (this.checked) {
                                $("input:checked", g.selectBox).not(this).each(function () {
                                    this.checked = false;
                                    $(".l-checkbox-checked", $(this).parent()).removeClass("l-checkbox-checked");
                                });
                                g.selectBox.slideToggle("fast");
                            }
                        }
                        po.checkboxUpdateValue();
                    });
                    $("table.l-table-nocheckbox td", g.selectBox).hover(function () {
                        $(this).addClass("l-over");
                    }, function () {
                        $(this).removeClass("l-over");
                    });
                    po.addClickEven();
                    //选择项初始化
                    po.dataInit();
                },
                //树
                setTree: function (tree) {
                    this.clearContent();
                    g.selectBox.table.remove();
                    if (tree.checkbox != false) {
                        tree.onCheck = function () {
                            var nodes = g.treeManager.getChecked();
                            var value = [];
                            var text = [];
                            $(nodes).each(function (i, node) {
                                if (p.treeLeafOnly && node.data.children) return;
                                value.push(node.data[p.valueField]);
                                text.push(node.data[p.textField]);
                            });
                            po.changeValue(value.join(p.split), text.join(p.split));
                        };
                    }
                    else {
                        tree.onSelect = function (node) {
                            if (p.treeLeafOnly && node.data.children) return;
                            var value = node.data[p.valueField];
                            var text = node.data[p.textField];
                            po.changeValue(value, text);
                        };
                        tree.onCancelSelect = function (node) {
                            po.changeValue("", "");
                        };
                    }
                    tree.onAfterAppend = function (domnode, nodedata) {
                        if (!g.treeManager) return;
                        var value = null;
                        if (p.initValue!=null) value = p.initValue;
                        else if (g.valueField.val() != "") value = g.valueField.val();
                        g.selectValueByTree(value);
                        
                    };
                    g.tree = $("<ul></ul>");
                    $("div:first", g.selectBox).append(g.tree);
                    g.tree.ligerTree(tree);
                    g.treeManager = g.tree.ligerGetTreeManager();
                },
                selectValueByTree: function (value) {
                    if (value != null) {
                        var text = "";
                        //text += g.treeManager.getTextByID(value);
                        //g.treeManager.selectNode(value);
                        var valuelist = value.toString().split(p.split);
                        $(valuelist).each(function (i, item) {
                            g.treeManager.selectNode(item.toString());
                            text += g.treeManager.getTextByID(item);
                            if (i < valuelist.length - 1) text += p.split;
                        });
                        po.changeValue(value, text);
                    }
                },
                //表格
                setGrid: function (grid) {
                    this.clearContent();
                    g.selectBox.table.remove();
                    g.grid = $("div:first", g.selectBox);
                    if (grid.checkbox != false) {
                        grid.onCheckAllRow = grid.onCheckRow = function () {
                            var rowsdata = g.gridManager.getCheckedRows();
                            var value = [];
                            var text = [];
                            $(rowsdata).each(function (i, rowdata) {
                                value.push(rowdata[p.valueField]);
                                text.push(rowdata[p.textField]);
                            });
                            po.changeValue(value.join(p.split), text.join(p.split));
                        };
                    }
                    else {
                        grid.onSelectRow = function (rowdata, rowobj, index) {
                            var value = rowdata[p.valueField];
                            var text = rowdata[p.textField];
                            po.changeValue(value, text);
                        };
                        grid.onUnSelectRow = function (rowdata, rowobj, index) {
                            po.changeValue("", "");
                        };
                    }
                    grid.width = "100%";
                    grid.height = "100%";
                    grid.heightDiff = -2;
                    grid.InWindow = false;
                    g.grid.ligerGrid(grid);
                    g.gridManager = g.grid.ligerGetGridManager();
                    p.hideOnLoseFocus = false;
                    po.onEndResize = function () {
                        g.gridManager && g.gridManager.setHeight(g.selectBox.height() - 2);
                    };
                },
                //add function()

                changeValue: function(newValue, newText) {
                    po.changeValue(newValue,newText);
                },

                data: p.data,
                inputText: null,
                select: null,
                textFieldID: "",
                valueFieldID: "",
                valueField: null //隐藏域(保存值)
            };
            //private object
            var po = {
                dataInit: function () {
                    var value = null;
                    if (p.initValue != undefined && p.initValue != null && p.initText != undefined && p.initText != null
                    ) {
                        po.changeValue(p.initValue, p.initText);
                    }
                    //根据值来初始化
                    if (p.initValue != undefined && p.initValue != null) {
                        value = p.initValue;
                        var text = g.findTextByValue(value);
                        po.changeValue(value, text);
                    }
                    //根据文本来初始化 
                    else if (p.initText != undefined && p.initText != null) {
                        value = g.findValueByText(p.initText);
                        po.changeValue(value, p.initText);
                    }
                    else if (g.valueField.val() != "") {
                        value = g.valueField.val();
                        var text = g.findTextByValue(value);
                        po.changeValue(value, text);
                    }
                    if (!p.isShowCheckBox && value != null) {
                        $("table tr", g.selectBox).find("td:first").each(function () {
                            if (value == $(this).attr("value")) {
                                $(this).addClass("l-selected");
                            }
                        });
                    }
                    if (p.isShowCheckBox && value != null) {
                        $(":checkbox", g.selectBox).each(function () {
                            var parentTD = null;
                            var checkbox = $(this);
                            if (checkbox.parent().get(0).tagName.toLowerCase() == "div") {
                                parentTD = checkbox.parent().parent();
                            } else {
                                parentTD = checkbox.parent();
                            }
                            if (parentTD == null) return;
                            var valuearr = value.toString().split(p.split);
                            $(valuearr).each(function (i, item) {
                                if (item == parentTD.attr("value")) {
                                    $(".l-checkbox", parentTD).addClass("l-checkbox-checked");
                                    checkbox[0].checked = true;
                                }
                            });
                        });
                    }
                },
                changeValue: function (newValue, newText) {                    
                    g.valueField.val(newValue);
                    g.inputText.val(newText);
                    g.selectedValue = newValue;
                    g.selectedText = newText;
                    g.inputText.trigger("change").focus();
                    if (newValue == 'null' || newText == p.emptyText) {
                        g.inputText.val('');
                        g.valueField.val('');
                    }
                    if (p.onSelected)
                        //p.onSelected(newValue, newText,g.newid);
                        p.onSelected(newValue, newText);
                },
                //更新选中的值(复选框)
                checkboxUpdateValue: function () {
                    var valueStr = "";
                    var textStr = "";
                    $("input:checked", g.selectBox).each(function () {
                        var parentTD = null;
                        if ($(this).parent().get(0).tagName.toLowerCase() == "div") {
                            parentTD = $(this).parent().parent();
                        } else {
                            parentTD = $(this).parent();
                        }
                        if (!parentTD) return;
                        valueStr += parentTD.attr("value") + p.split;
                        textStr += parentTD.attr("text") + p.split;
                    });
                    if (valueStr.length > 0) valueStr = valueStr.substr(0, valueStr.length - 1);
                    if (textStr.length > 0) textStr = textStr.substr(0, textStr.length - 1);
                    po.changeValue(valueStr, textStr);
                },
                addClickEven: function () {
                    //选项点击
                    $(".l-table-nocheckbox td", g.selectBox).click(function () {
                        if (p.onBeforeSelect && !p.onBeforeSelect($(this).attr("value"), $(this).html())) {
                            if (p.slide) g.selectBox.slideToggle("fast");
                            else g.selectBox.hide();
                            return false;
                        }
                        if ($(this).hasClass("l-selected")) {
                            if (p.slide) g.selectBox.slideToggle("fast");
                            else g.selectBox.hide();
                            return;
                        }
                        $(".l-selected", g.selectBox).removeClass("l-selected");
                        $(this).addClass("l-selected");
                        if (g.select) {
                            if (g.select[0].selectedIndex != $(this).attr('index')) {
                                var newIndex = parseInt($(this).attr('index'));
                                g.select[0].selectedIndex = newIndex;
                                g.select.trigger("change");
                            }
                        }
                        if (p.slide) {
                            g.boxToggling = true;
                            g.selectBox.hide("fast", function () {
                                g.boxToggling = false;
                            })
                        } else g.selectBox.hide();
                        po.changeValue($(this).attr("value"), $(this).html());
                    });
                },
                toggleSelectBox: function (isHide) {
                    var textHeight = g.wrapper.height();
                    g.boxToggling = true;
                    if (isHide) {
                        if (p.slide) {
                            g.selectBox.slideToggle('fast', function () {
                                g.boxToggling = false;
                            });
                        }
                        else {
                            g.selectBox.hide();
                            g.boxToggling = false;
                        }
                    }
                    else {
                        var topheight = g.wrapper.offset().top - $(window).scrollTop();
                        var selfheight = g.selectBox.height() + textHeight + 4;
                        if (topheight + selfheight > $(window).height() && topheight > selfheight) {
                            g.selectBox.css("marginTop", -1 * (g.selectBox.height() + textHeight + 5));
                        }
                        if (p.slide) {
                            g.selectBox.slideToggle('fast', function () {
                                g.boxToggling = false;
                                if (!p.isShowCheckBox && $('td.l-selected', g.selectBox).length > 0) {
                                    var offSet = ($('td.l-selected', g.selectBox).offset().top - g.selectBox.offset().top);
                                    $(".l-box-select-inner", g.selectBox).animate({ scrollTop: offSet });
                                }
                            });
                        }
                        else {
                            g.selectBox.show();
                            g.boxToggling = false;
                            if (!g.tree && !g.grid && !p.isShowCheckBox && $('td.l-selected', g.selectBox).length > 0) {
                                var offSet = ($('td.l-selected', g.selectBox).offset().top - g.selectBox.offset().top);
                                $(".l-box-select-inner", g.selectBox).animate({ scrollTop: offSet });
                            }
                        }
                    }
                    g.isShowed = g.selectBox.is(":visible");
                }
            };
            //文本框初始化
            if (this.tagName.toLowerCase() == "input") {
                this.readOnly = true;
                g.inputText = $(this);
                g.textFieldID = this.id;
            }
            else if (this.tagName.toLowerCase() == "select") {
                $(this).addClass('l-hidden');
                g.select = $(this);
                p.isMultiSelect = false;
                p.isShowCheckBox = false;
                g.textFieldID = this.id + "_txt";
                g.inputText = $('<input type="text" readonly="true"/>');
                g.inputText.attr("id", g.textFieldID).insertAfter($(this));
            } else {
                //不支持其他类型
                return;
            }
            if (g.inputText[0].name == undefined) g.inputText[0].name = g.textFieldID;
            //隐藏域初始化
            g.valueField = null;
            if (p.valueFieldID) {
                g.valueField = $("#" + p.valueFieldID + ":input");
                if (g.valueField.length == 0) g.valueField = $('<input type="hidden"/>');
                g.valueField[0].id = g.valueField[0].name = p.valueFieldID;
            }
            else {
                g.valueField = $('<input type="hidden"/>');
                g.valueField[0].id = g.valueField[0].name = g.textFieldID + "_val";
            }
            if (g.valueField[0].name == undefined) g.valueField[0].name = g.valueField[0].id;
            //开关
            g.link = $('<div class="l-trigger"><div class="l-trigger-icon"></div></div>');
            //下拉框
            g.selectBox = $('<div class="l-box-select"><div class="l-box-select-inner"><table cellpadding="0" cellspacing="0" border="0" class="l-box-select-table"></table></div></div>');
            g.selectBox.table = $("table:first", g.selectBox);
            //外层
            g.wrapper = g.inputText.wrap('<div class="l-text l-text-combobox"></div>').parent();
            g.wrapper.append('<div class="l-text-l"></div><div class="l-text-r"></div>');
            g.wrapper.append(g.link).after(g.selectBox).after(g.valueField);

            g.inputText.addClass("l-text-field");
            if (p.width) {
                g.wrapper.css({ width: p.width });
                g.inputText.css({ width: p.width - 20 });
            }
            if (p.height) {
                g.wrapper.height(p.height);
                g.inputText.height(p.height - 2);
                g.link.height(p.height - 4);
            }
            if (p.isShowCheckBox && !g.select) {
                $("table", g.selectBox).addClass("l-table-checkbox");
            } else {
                p.isShowCheckBox = false;
                $("table", g.selectBox).addClass("l-table-nocheckbox");
            }
            //调整大小支持
            if (p.resize && $.fn.ligerResizable) {
                g.selectBox.ligerResizable({ handles: 'se,s', onStartResize: function () {
                    g.resizing = true;
                    p.onStartResize && onStartResize();
                }
                , onEndResize: function () {
                    g.resizing = false;
                    po.onEndResize && po.onEndResize();
                    p.onEndResize && p.onEndResize();
                }
                });
                g.selectBox.append("<div class='l-btn-nw-drop'></div>");
            }
            if (p.readonly)
            {
                g.wrapper.addClass('l-text-disabled');
            }
            //开关 事件
            g.link.hover(function () {
                this.className = "l-trigger-hover";
            }, function () {
                this.className = "l-trigger";
            }).mousedown(function () {
                this.className = "l-trigger-pressed";
            }).mouseup(function () {
                this.className = "l-trigger-hover";
            }).click(function () {
                if (p.readonly) return false;
                if (p.onBeforeOpen && p.onBeforeOpen() == false) return false;
                po.toggleSelectBox(g.selectBox.is(":visible"));
            });
            g.inputText.click(function () {
                if (p.readonly) return false;
                if (p.onBeforeOpen && p.onBeforeOpen() == false) return false;
                po.toggleSelectBox(g.selectBox.is(":visible"));
            }).blur(function () {
                g.wrapper.removeClass("l-text-focus");
            }).focus(function () {
                g.wrapper.addClass("l-text-focus");
            });
            g.wrapper.hover(function () {
                g.wrapper.addClass("l-text-over");
            }, function () {
                g.wrapper.removeClass("l-text-over");
            });

            g.resizing = false;
            g.selectBox.hover(null, function (e) {
                if (p.hideOnLoseFocus && g.selectBox.is(":visible") && !g.boxToggling && !g.resizing) {
                    po.toggleSelectBox(true);
                }
            });
            //下拉框宽度、高度初始化
            if (p.selectBoxWidth) {
                g.selectBox.width(p.selectBoxWidth);
            }
            else {
                g.selectBox.css('width', g.wrapper.css('width'));
            }
            var itemsleng = $("tr", g.selectBox.table).length;
            if (!p.selectBoxHeight && itemsleng < 8) p.selectBoxHeight = itemsleng * 30;
            if (p.selectBoxHeight) {
                g.selectBox.height(p.selectBoxHeight);
            }

            //下拉框内容初始化
            g.bulidContent();
            if (p.render)
                g.inputText.val(p.render());
            if (this.id == undefined) this.id = "LigerUI_" + new Date().getTime();
            LigerUIManagers[this.id + "_ComboBox"] = g;
            this.usedComboBox = true;
        });
        if (this.length == 0) return null;
        if (this.length == 1) return LigerUIManagers[this[0].id + "_ComboBox"];
        var managers = [];
        this.each(function () {
            managers.push(LigerUIManagers[this.id + "_ComboBox"]);
        });
        return managers;
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($)
{
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager)
    {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr)
    {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.fn.ligerGetDateEditorManager = function ()
    {
        return $.ligerui.getManager(this);
    };

    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.DateEditor = {
        format: "yyyy-MM-dd hh:mm",
        showTime: false,
        onChangeDate: false
    };
    $.ligerDefaults.DateEditorString = {
        dayMessage: ["日", "一", "二", "三", "四", "五", "六"],
        monthMessage: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
        todayMessage: "今天",
        closeMessage: "关闭"
    };

    //DateEditor manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.DateEditor = function (options, po)
    {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.DateEditor.prototype = {
        bulidContent: function ()
        {
            var po = this.po, g = this, p = this.options;
            //当前月第一天星期
            var thismonthFirstDay = new Date(g.currentDate.year, g.currentDate.month - 1, 1).getDay();
            //当前月天数
            var nextMonth = g.currentDate.month;
            var nextYear = g.currentDate.year;
            if (++nextMonth == 13)
            {
                nextMonth = 1;
                nextYear++;
            }
            var monthDayNum = new Date(nextYear, nextMonth - 1, 0).getDate();
            //当前上个月天数
            var prevMonthDayNum = new Date(g.currentDate.year, g.currentDate.month - 1, 0).getDate();

            g.buttons.btnMonth.html(p.monthMessage[g.currentDate.month - 1]);
            g.buttons.btnYear.html(g.currentDate.year);
            g.toolbar.time.hour.html(g.currentDate.hour);
            g.toolbar.time.minute.html(g.currentDate.minute);
            if (g.toolbar.time.hour.html().length == 1)
                g.toolbar.time.hour.html("0" + g.toolbar.time.hour.html());
            if (g.toolbar.time.minute.html().length == 1)
                g.toolbar.time.minute.html("0" + g.toolbar.time.minute.html());
            $("td", this.body.tbody).each(function () { this.className = "" });
            $("tr", this.body.tbody).each(function (i, tr)
            {
                $("td", tr).each(function (j, td)
                {
                    var id = i * 7 + (j - thismonthFirstDay);
                    var showDay = id + 1;
                    if (g.selectedDate && g.currentDate.year == g.selectedDate.year &&
                            g.currentDate.month == g.selectedDate.month &&
                            id + 1 == g.selectedDate.date)
                    {
                        if (j == 0 || j == 6)
                        {
                            $(td).addClass("l-box-dateeditor-holiday")
                        }
                        $(td).addClass("l-box-dateeditor-selected");
                        $(td).siblings().removeClass("l-box-dateeditor-selected");
                    }
                    else if (g.currentDate.year == g.now.year &&
                            g.currentDate.month == g.now.month &&
                            id + 1 == g.now.date)
                    {
                        if (j == 0 || j == 6)
                        {
                            $(td).addClass("l-box-dateeditor-holiday")
                        }
                        $(td).addClass("l-box-dateeditor-today");
                    }
                    else if (id < 0)
                    {
                        showDay = prevMonthDayNum + showDay;
                        $(td).addClass("l-box-dateeditor-out")
                                .removeClass("l-box-dateeditor-selected");
                    }
                    else if (id > monthDayNum - 1)
                    {
                        showDay = showDay - monthDayNum;
                        $(td).addClass("l-box-dateeditor-out")
                                .removeClass("l-box-dateeditor-selected");
                    }
                    else if (j == 0 || j == 6)
                    {
                        $(td).addClass("l-box-dateeditor-holiday")
                                .removeClass("l-box-dateeditor-selected");
                    }
                    else
                    {
                        td.className = "";
                    }

                    $(td).html(showDay);
                });
            });
        },
        toggleDateEditor: function (isHide)
        {
            var po = this.po, g = this, p = this.options;
            var textHeight = g.text.height();
            g.editorToggling = true;
            if (isHide)
            {
                g.dateeditor.hide('fast', function ()
                {
                    g.editorToggling = false;
                });
            }
            else
            {
                if (g.text.offset().top + 4 > g.dateeditor.height() && g.text.offset().top + g.dateeditor.height() + textHeight + 4 - $(window).scrollTop() > $(window).height())
                {
                    g.dateeditor.css("marginTop", -1 * (g.dateeditor.height() + textHeight + 5));
                    g.showOnTop = true;
                }
                else
                {
                    g.showOnTop = false;
                }
                g.dateeditor.slideDown('fast', function ()
                {
                    g.editorToggling = false;
                });
            }
        },
        showDate: function ()
        {
            var po = this.po, g = this, p = this.options;
            if (!this.selectedDate) return;
            var dateStr = g.selectedDate.year + "/" + g.selectedDate.month + "/" + g.selectedDate.date;
            this.currentDate.hour = parseInt(g.toolbar.time.hour.html());
            this.currentDate.minute = parseInt(g.toolbar.time.minute.html());
            if (p.showTime)
            {
                dateStr += " " + this.currentDate.hour + ":" + this.currentDate.minute;
            }
            this.inputText.val(dateStr);
            this.inputText.trigger("change").focus();
        },
        isDateTime: function (dateStr)
        {
            var po = this.po, g = this, p = this.options;
            var r = dateStr.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
            if (r == null) return false;
            var d = new Date(r[1], r[3] - 1, r[4]);
            if (d == "NaN") return false;
            return (d.getFullYear() == r[1] && (d.getMonth() + 1) == r[3] && d.getDate() == r[4]);
        },
        isLongDateTime: function (dateStr)
        {
            var po = this.po, g = this, p = this.options;
            var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2})$/;
            var r = dateStr.match(reg);
            if (r == null) return false;
            var d = new Date(r[1], r[3] - 1, r[4], r[5], r[6]);
            if (d == "NaN") return false;
            return (d.getFullYear() == r[1] && (d.getMonth() + 1) == r[3] && d.getDate() == r[4] && d.getHours() == r[5] && d.getMinutes() == r[6]);
        },
        getFormatDate: function (date)
        {
            var po = this.po, g = this, p = this.options;
            if (date == "NaN") return null;
            var format = p.format;
            var o = {
                "M+": date.getMonth() + 1,
                "d+": date.getDate(),
                "h+": date.getHours(),
                "m+": date.getMinutes(),
                "s+": date.getSeconds(),
                "q+": Math.floor((date.getMonth() + 3) / 3),
                "S": date.getMilliseconds()
            }
            if (/(y+)/.test(format))
            {
                format = format.replace(RegExp.$1, (date.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
            }
            for (var k in o)
            {
                if (new RegExp("(" + k + ")").test(format))
                {
                    format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
                }
            }
            return format;
        },
        onTextChange: function ()
        {
            var po = this.po, g = this, p = this.options;
            var val = g.inputText.val();
            if (val == "")
            {
                g.selectedDate = null;
                return true;
            }
            if (!p.showTime && !g.isDateTime(val))
            {
                //恢复
                if (!g.usedDate)
                {
                    g.inputText.val("");
                } else
                {
                    g.inputText.val(g.getFormatDate(g.usedDate));
                }
            }
            else if (p.showTime && !g.isLongDateTime(val))
            {
                //恢复
                if (!g.usedDate)
                {
                    g.inputText.val("");
                } else
                {
                    g.inputText.val(g.getFormatDate(g.usedDate));
                }
            }
            else
            {
                while (val.indexOf("-") > -1)
                    val = val.replace("-", "/"); // do it for ie
                var formatVal = g.getFormatDate(new Date(val));
                if (formatVal == null)
                {
                    //恢复
                    if (!g.usedDate)
                    {
                        g.inputText.val("");
                    } else
                    {
                        g.inputText.val(g.getFormatDate(g.usedDate));
                    }
                }
                g.usedDate = new Date(val); //记录
                g.selectedDate = {
                    year: g.usedDate.getFullYear(),
                    month: g.usedDate.getMonth() + 1, //注意这里
                    day: g.usedDate.getDay(),
                    date: g.usedDate.getDate(),
                    hour: g.usedDate.getHours(),
                    minute: g.usedDate.getMinutes()
                };
                g.currentDate = {
                    year: g.usedDate.getFullYear(),
                    month: g.usedDate.getMonth() + 1, //注意这里
                    day: g.usedDate.getDay(),
                    date: g.usedDate.getDate(),
                    hour: g.usedDate.getHours(),
                    minute: g.usedDate.getMinutes()
                };
                g.inputText.val(formatVal);
                if (p.onChangeDate)
                {
                    p.onChangeDate(formatVal);
                }
                if ($(g.dateeditor).is(":visible"))
                    g.bulidContent();
            }
        }, 
        getValue: function ()
        {
            return this.inputText.val();
        },
        setValue: function (value)
        {
            this.inputText.val(value);
        },
        setEnabled: function ()
        {
            var po = this.po, g = this, p = this.options;
            this.inputText.removeAttr("readonly");
            this.text.removeClass('l-text-disabled');
            p.disabled = false;
        },
        setDisabled: function ()
        {
            var po = this.po, g = this, p = this.options;
            this.inputText.attr("readonly", "readonly");
            this.text.addClass('l-text-disabled');
            p.disabled = true;
        }
    };

    $.fn.ligerDateEditor = function (options)
    {
        this.each(function ()
        {
            if (typeof (this.applyligerui) == Boolean && this.applyligerui) return;
            var p = $.extend({}, options || {});
            if ($(this).attr("ligerui"))
            {
                try
                {
                    var attroptions = $(this).attr("ligerui");
                    if (attroptions.indexOf('{') < 0) attroptions = "{" + attroptions + "}";
                    eval("attroptions = " + attroptions + ";");
                    if (attroptions) p = $.extend({}, attroptions, p || {});
                }
                catch (e) { }
            }
            p = $.extend({}, $.ligerDefaults.DateEditor, $.ligerDefaults.DateEditorString, p || {});
            if (!p.showTime && p.format.indexOf(" hh:mm") > -1)
                p.format = p.format.replace(" hh:mm", "");
            if (this.tagName.toLowerCase() != "input" || this.type != "text") return;
            var po = {};
            var g = new $.ligerManagers.DateEditor(p, po);

            g.inputText = $(this);
            if (!g.inputText.hasClass("l-text-field"))
                g.inputText.addClass("l-text-field");
            g.link = $('<div class="l-trigger"><div class="l-trigger-icon"></div></div>');
            g.text = g.inputText.wrap('<div class="l-text l-text-date"></div>').parent();
            g.text.append('<div class="l-text-l"></div><div class="l-text-r"></div>');
            g.text.append(g.link);
            //添加个包裹，
            g.textwrapper = g.text.wrap('<div class="l-text-wrapper"></div>').parent();
            if (p.width)
            {
                g.text.css({ width: p.width });
                g.inputText.css({ width: p.width - 20 });
                g.textwrapper.css({ width: p.width });
            }
            var dateeditorHTML = "";
            dateeditorHTML += "<div class='l-box-dateeditor' style='display:none'>";
            dateeditorHTML += "    <div class='l-box-dateeditor-header'>";
            dateeditorHTML += "        <div class='l-box-dateeditor-header-btn l-box-dateeditor-header-prevyear'><span></span></div>";
            dateeditorHTML += "        <div class='l-box-dateeditor-header-btn l-box-dateeditor-header-prevmonth'><span></span></div>";
            dateeditorHTML += "        <div class='l-box-dateeditor-header-text'><a class='l-box-dateeditor-header-month'></a> , <a  class='l-box-dateeditor-header-year'></a></div>";
            dateeditorHTML += "        <div class='l-box-dateeditor-header-btn l-box-dateeditor-header-nextmonth'><span></span></div>";
            dateeditorHTML += "        <div class='l-box-dateeditor-header-btn l-box-dateeditor-header-nextyear'><span></span></div>";
            dateeditorHTML += "    </div>";
            dateeditorHTML += "    <div class='l-box-dateeditor-body'>";
            dateeditorHTML += "        <table cellpadding='0' cellspacing='0' border='0' class='l-box-dateeditor-calendar'>";
            dateeditorHTML += "            <thead>";
            dateeditorHTML += "                <tr><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td></tr>";
            dateeditorHTML += "            </thead>";
            dateeditorHTML += "            <tbody>";
            dateeditorHTML += "                <tr><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td></tr><tr><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td></tr><tr><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td></tr><tr><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td></tr><tr><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td></tr><tr><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td><td align='center'></td></tr>";
            dateeditorHTML += "            </tbody>";
            dateeditorHTML += "        </table>";
            dateeditorHTML += "        <ul class='l-box-dateeditor-monthselector'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul>";
            dateeditorHTML += "        <ul class='l-box-dateeditor-yearselector'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul>";
            dateeditorHTML += "        <ul class='l-box-dateeditor-hourselector'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul>";
            dateeditorHTML += "        <ul class='l-box-dateeditor-minuteselector'><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul>";
            dateeditorHTML += "    </div>";
            dateeditorHTML += "    <div class='l-box-dateeditor-toolbar'>";
            dateeditorHTML += "        <div class='l-box-dateeditor-time'></div>";
            dateeditorHTML += "        <div class='l-button l-button-today'></div>";
            dateeditorHTML += "        <div class='l-button l-button-close'></div>";
            dateeditorHTML += "        <div class='l-clear'></div>";
            dateeditorHTML += "    </div>";
            dateeditorHTML += "</div>";
            g.dateeditor = $(dateeditorHTML);
            g.textwrapper.append(g.dateeditor);
            g.header = $(".l-box-dateeditor-header", g.dateeditor);
            g.body = $(".l-box-dateeditor-body", g.dateeditor);
            g.toolbar = $(".l-box-dateeditor-toolbar", g.dateeditor);

            g.body.thead = $("thead", g.body);
            g.body.tbody = $("tbody", g.body);
            g.body.monthselector = $(".l-box-dateeditor-monthselector", g.body);
            g.body.yearselector = $(".l-box-dateeditor-yearselector", g.body);
            g.body.hourselector = $(".l-box-dateeditor-hourselector", g.body);
            g.body.minuteselector = $(".l-box-dateeditor-minuteselector", g.body);

            g.toolbar.time = $(".l-box-dateeditor-time", g.toolbar);
            g.toolbar.time.hour = $("<a></a>");
            g.toolbar.time.minute = $("<a></a>");
            g.buttons = {
                btnPrevYear: $(".l-box-dateeditor-header-prevyear", g.header),
                btnNextYear: $(".l-box-dateeditor-header-nextyear", g.header),
                btnPrevMonth: $(".l-box-dateeditor-header-prevmonth", g.header),
                btnNextMonth: $(".l-box-dateeditor-header-nextmonth", g.header),
                btnYear: $(".l-box-dateeditor-header-year", g.header),
                btnMonth: $(".l-box-dateeditor-header-month", g.header),
                btnToday: $(".l-button-today", g.toolbar),
                btnClose: $(".l-button-close", g.toolbar)
            };
            var nowDate = new Date();
            g.now = {
                year: nowDate.getFullYear(),
                month: nowDate.getMonth() + 1, //注意这里
                day: nowDate.getDay(),
                date: nowDate.getDate(),
                hour: nowDate.getHours(),
                minute: nowDate.getMinutes()
            };
            //当前的时间
            g.currentDate = {
                year: nowDate.getFullYear(),
                month: nowDate.getMonth() + 1,
                day: nowDate.getDay(),
                date: nowDate.getDate(),
                hour: nowDate.getHours(),
                minute: nowDate.getMinutes()
            };
            //选择的时间
            g.selectedDate = null;
            //使用的时间
            g.usedDate = null;



            //初始化数据
            //设置周日至周六
            $("td", g.body.thead).each(function (i, td)
            {
                $(td).html(p.dayMessage[i]);
            });
            //设置一月到十一二月
            $("li", g.body.monthselector).each(function (i, li)
            {
                $(li).html(p.monthMessage[i]);
            });
            //设置按钮
            g.buttons.btnToday.html(p.todayMessage);
            g.buttons.btnClose.html(p.closeMessage);
            //设置时间
            if (p.showTime)
            {
                g.toolbar.time.show();
                g.toolbar.time.append(g.toolbar.time.hour).append(":").append(g.toolbar.time.minute);
                $("li", g.body.hourselector).each(function (i, item)
                {
                    var str = i;
                    if (i < 10) str = "0" + i.toString();
                    $(this).html(str);
                });
                $("li", g.body.minuteselector).each(function (i, item)
                {
                    var str = i;
                    if (i < 10) str = "0" + i.toString();
                    $(this).html(str);
                });
            }
            //设置主体
            g.bulidContent();
            //初始化   
            if (g.inputText.val() != "")
                g.onTextChange();
            /**************
            **bulid evens**
            *************/
            g.dateeditor.hover(null, function (e)
            {
                if (g.dateeditor.is(":visible") && !g.editorToggling)
                {
                    g.toggleDateEditor(true);
                }
            });
            //toggle even
            g.link.hover(function ()
            {
                if (p.disabled) return;
                this.className = "l-trigger-hover";
            }, function ()
            {
                if (p.disabled) return;
                this.className = "l-trigger";
            }).mousedown(function ()
            {
                if (p.disabled) return;
                this.className = "l-trigger-pressed";
            }).mouseup(function ()
            {
                if (p.disabled) return;
                this.className = "l-trigger-hover";
            }).click(function ()
            {
                if (p.disabled) return;
                g.bulidContent();
                g.toggleDateEditor(g.dateeditor.is(":visible"));
            });
            //不可用属性时处理
            if (p.disabled)
            {
                g.inputText.attr("readonly", "readonly");
                g.text.addClass('l-text-disabled');
            }
            //初始值
            if (p.initValue)
            {
                g.inputText.val(p.initValue);
            }
            g.buttons.btnClose.click(function ()
            {
                g.toggleDateEditor(true);
            });
            //日期 点击
            $("td", g.body.tbody).hover(function ()
            {
                if ($(this).hasClass("l-box-dateeditor-today")) return;
                $(this).addClass("l-box-dateeditor-over");
            }, function ()
            {
                $(this).removeClass("l-box-dateeditor-over");
            }).click(function ()
            {
                $(".l-box-dateeditor-selected", g.body.tbody).removeClass("l-box-dateeditor-selected");
                if (!$(this).hasClass("l-box-dateeditor-today"))
                    $(this).addClass("l-box-dateeditor-selected");
                g.currentDate.date = parseInt($(this).html());
                g.currentDate.day = new Date(g.currentDate.year, g.currentDate.month - 1, 1).getDay();
                if ($(this).hasClass("l-box-dateeditor-out"))
                {
                    if ($("tr", g.body.tbody).index($(this).parent()) == 0)
                    {
                        if (--g.currentDate.month == 0)
                        {
                            g.currentDate.month = 12;
                            g.currentDate.year--;
                        }
                    } else
                    {
                        if (++g.currentDate.month == 13)
                        {
                            g.currentDate.month = 1;
                            g.currentDate.year++;
                        }
                    }
                }
                g.selectedDate = {
                    year: g.currentDate.year,
                    month: g.currentDate.month,
                    date: g.currentDate.date
                };
                g.showDate();
                g.editorToggling = true;
                g.dateeditor.slideToggle('fast', function ()
                {
                    g.editorToggling = false;
                });
            });

            $(".l-box-dateeditor-header-btn", g.header).hover(function ()
            {
                $(this).addClass("l-box-dateeditor-header-btn-over");
            }, function ()
            {
                $(this).removeClass("l-box-dateeditor-header-btn-over");
            });
            //选择年份
            g.buttons.btnYear.click(function ()
            {
                //build year list
                if (!g.body.yearselector.is(":visible"))
                {
                    $("li", g.body.yearselector).each(function (i, item)
                    {
                        var currentYear = g.currentDate.year + (i - 4);
                        if (currentYear == g.currentDate.year)
                            $(this).addClass("l-selected");
                        else
                            $(this).removeClass("l-selected");
                        $(this).html(currentYear);
                    });
                }

                g.body.yearselector.slideToggle();
            });
            g.body.yearselector.hover(function () { }, function ()
            {
                $(this).slideUp();
            });
            $("li", g.body.yearselector).click(function ()
            {
                g.currentDate.year = parseInt($(this).html());
                g.body.yearselector.slideToggle();
                g.bulidContent();
            });
            //select month
            g.buttons.btnMonth.click(function ()
            {
                $("li", g.body.monthselector).each(function (i, item)
                {
                    //add selected style
                    if (g.currentDate.month == i + 1)
                        $(this).addClass("l-selected");
                    else
                        $(this).removeClass("l-selected");
                });
                g.body.monthselector.slideToggle();
            });
            g.body.monthselector.hover(function () { }, function ()
            {
                $(this).slideUp("fast");
            });
            $("li", g.body.monthselector).click(function ()
            {
                var index = $("li", g.body.monthselector).index(this);
                g.currentDate.month = index + 1;
                g.body.monthselector.slideToggle();
                g.bulidContent();
            });

            //选择小时
            g.toolbar.time.hour.click(function ()
            {
                $("li", g.body.hourselector).each(function (i, item)
                {
                    //add selected style
                    if (g.currentDate.hour == i)
                        $(this).addClass("l-selected");
                    else
                        $(this).removeClass("l-selected");
                });
                g.body.hourselector.slideToggle();
            });
            g.body.hourselector.hover(function () { }, function ()
            {
                $(this).slideUp("fast");
            });
            $("li", g.body.hourselector).click(function ()
            {
                var index = $("li", g.body.hourselector).index(this);
                g.currentDate.hour = index;
                g.body.hourselector.slideToggle();
                g.bulidContent();
            });
            //选择分钟
            g.toolbar.time.minute.click(function ()
            {
                $("li", g.body.minuteselector).each(function (i, item)
                {
                    //add selected style
                    if (g.currentDate.minute == i)
                        $(this).addClass("l-selected");
                    else
                        $(this).removeClass("l-selected");
                });
                g.body.minuteselector.slideToggle("fast", function ()
                {
                    var index = $("li", this).index($('li.l-selected', this));
                    if (index > 29)
                    {
                        var offSet = ($('li.l-selected', this).offset().top - $(this).offset().top);
                        $(this).animate({ scrollTop: offSet });
                    }
                });
            });
            g.body.minuteselector.hover(function () { }, function ()
            {
                $(this).slideUp("fast");
            });
            $("li", g.body.minuteselector).click(function ()
            {
                var index = $("li", g.body.minuteselector).index(this);
                g.currentDate.minute = index;
                g.body.minuteselector.slideToggle("fast");
                g.bulidContent();
            });

            //上个月
            g.buttons.btnPrevMonth.click(function ()
            {
                if (--g.currentDate.month == 0)
                {
                    g.currentDate.month = 12;
                    g.currentDate.year--;
                }
                g.bulidContent();
            });
            //下个月
            g.buttons.btnNextMonth.click(function ()
            {
                if (++g.currentDate.month == 13)
                {
                    g.currentDate.month = 1;
                    g.currentDate.year++;
                }
                g.bulidContent();
            });
            //上一年
            g.buttons.btnPrevYear.click(function ()
            {
                g.currentDate.year--;
                g.bulidContent();
            });
            //下一年
            g.buttons.btnNextYear.click(function ()
            {
                g.currentDate.year++;
                g.bulidContent();
            });
            //今天
            g.buttons.btnToday.click(function ()
            {
                g.currentDate = {
                    year: g.now.year,
                    month: g.now.month,
                    day: g.now.day,
                    date: g.now.date
                };
                g.selectedDate = {
                    year: g.now.year,
                    month: g.now.month,
                    day: g.now.day,
                    date: g.now.date
                };
                g.showDate();
                g.dateeditor.slideToggle("fast");
            });
            //文本框
            g.inputText.change(function ()
            {
                g.onTextChange();
            }).blur(function ()
            {
                g.text.removeClass("l-text-focus");
            }).focus(function ()
            {
                g.text.addClass("l-text-focus");
            });
            g.text.hover(function ()
            {
                g.text.addClass("l-text-over");
            }, function ()
            {
                g.text.removeClass("l-text-over");
            });
            //LEABEL 支持
            if (p.label)
            {
                g.labelwrapper = g.textwrapper.wrap('<div class="l-labeltext"></div>').parent();
                g.labelwrapper.prepend('<div class="l-text-label" style="float:left;display:inline;">' + p.label + ':&nbsp</div>');
                g.textwrapper.css('float', 'left');
                if (!p.labelWidth)
                {
                    p.labelWidth = $('.l-text-label', g.labelwrapper).outerWidth();
                } else
                {
                    $('.l-text-label', g.labelwrapper).outerWidth(p.labelWidth);
                }
                $('.l-text-label', g.labelwrapper).width(p.labelWidth);
                $('.l-text-label', g.labelwrapper).height(g.text.height());
                g.labelwrapper.append('<br style="clear:both;" />');
                if (p.labelAlign)
                {
                    $('.l-text-label', g.labelwrapper).css('text-align', p.labelAlign);
                }
                g.textwrapper.css({ display: 'inline' });
                g.labelwrapper.width(g.text.outerWidth() + p.labelWidth + 2);
            }

            $.ligerui.addManager(this, g);
        });
        return $.ligerui.getManager(this);
    }
})(jQuery);﻿/**
* jQuery ligerUI 1.1.0.1
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
//dialog 图片文件夹的路径 针对于IE6设置
var ligerDialogImagePath = "../../lib/ligerUI/skins/touch/images/dialog/";
(function ($) {
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager) {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.removeManager = function (dom, manager) {

        $.ligerui.ManagerCount--;
        delete $.ligerui.Managers[dom.id]
        dom.applyligerui = false;
    }
    $.ligerui.getManager = function (domArr) {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Dialog = {
        cls: null,       //给dialog附加css class
        id: null,        //给dialog附加id
        buttons: null, //按钮集合 
        isDrag: true,   //是否拖动
        width: 300,     //宽度
        height: null,   //高度，默认自适应 
        content: '',    //内容
        target: null,   //目标对象，指定它将以appendTo()的方式载入
        url: null,      //目标页url，默认以iframe的方式载入
        load: false,     //是否以load()的方式加载目标页的内容
        type: 'none',   //类型 warn、success、error、question
        left: null,     //位置left
        top: null,      //位置top
        modal: true,    //是否模态对话框
        name: null,     //创建iframe时 作为iframe的name和id 
        isResize: false, // 是否调整大小
        allowClose: true, //允许关闭
        opener: null,
        timeParmName: null,  //是否给URL后面加上值为new Date().getTime()的参数，如果需要指定一个参数名即可
        closeWhenEnter: null, //回车时是否关闭dialog
        isHidden: false,        //关闭对话框时是否只是隐藏，还是销毁对话框
        zindex: 9000,
        showToggle: false,                          //是否显示收缩窗口按钮
        showMax: false,
        slide: true
    };
    $.ligerDefaults.DialogString = {
        titleMessage: '提示',                     //提示文本标题
        waittingMessage: '正在等待中,请稍候...'
    };

    //dialog manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Dialog = function (options, po) {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.Dialog.prototype = {
        //按下回车
        enter: function () {
            var g = this; var po = this.po;
            var isClose;
            if (po.closeWhenEnter != undefined) {
                isClose = po.closeWhenEnter;
            }
            else if (po.type == "warn" || po.type == "error" || po.type == "success" || po.type == "question") {
                isClose = true;
            }
            if (isClose) {
                g.close();
            }
        },
        esc: function () {

        },
        close: function () {
            var g = this; var po = this.po, p = this.options;

            $.ligerui.removeManager(g.dialog[0], g);

            if (g.frame) {
                var jframe = $('iframe', g.dialog);
                if (jframe.length) {
                    var frame = jframe[0];
                    frame.src = "about:blank";
                    if (frame.contentWindow && frame.contentWindow.document) {
                        try {
                            frame.contentWindow.document.write('');
                        } catch (e) {
                        }
                    }
                    $.browser.msie && CollectGarbage();
                    jframe.remove();

                }

            }
            g.dialog.remove();
            if (g.windowMask) {
                if (p.isHidden) g.windowMask.hide();
                else g.windowMask.remove();
            }

            $('body').unbind('keydown.dialog');
        },
        hidden: function () {
            var g = this; var po = this.po;

            if (g.windowMask) g.windowMask.hide();
            // po.removeWindowMask();
            g.dialog.hide();
        },
        show: function () {
            var g = this, po = this.po, p = this.options;
            if (g.windowMask)
                g.windowMask.show()
            else if (p.modal)
                po.applyWindowMask();
            g.dialog.show();
        },
        url: function (url) {
            var g = this, po = this.po, p = this.options;
            p.url = url;
            if (g.jiframe)
                g.jiframe.attr("src", p.url);
        },
        setContent: function (content) {
            var g = this, po = this.po, p = this.options;
            p.content = content;
            $(".l-dialog-content", g.dialog.body).html(p.content);
        },
        //展开 收缩
        toggle: function () {
            var g = this, p = this.options;
            if (!g.wintoggle) return;
            if (g.wintoggle.hasClass("l-dialog-extend"))
                g.extend();
            else
                g.collapse();
        },

        //收缩
        collapse: function () {
            var g = this, p = this.options;
            if (!g.wintoggle) return;
            if (p.slide)
                g.dialog.content.animate({ height: 1 }, p.slide);
            else
                g.dialog.content.height(1);
        },

        //展开
        extend: function () {
            var g = this, p = this.options;
            if (!g.wintoggle) return;
            var contentHeight = g._height - g._borderY - g.dialog.buttons.outerHeight();
            if (p.slide)
                g.dialog.content.animate({ height: g.dialog.content.currentheight }, p.slide);
            else
                g.dialog.content.height(g.dialog.content.currentheight);
        },
        setShowToggle: function (value) {
            var g = this, p = this.options;
            if (value) {
                if (!g.wintoggle) {
                    g.wintoggle = $('<div class="l-dialog-winbtn l-dialog-collapse"></div>').appendTo(g.dialog.winbtns)
                   .hover(function () {
                       if ($(this).hasClass("l-dialog-extend"))
                           $(this).addClass("l-dialog-extend-over");
                       else
                           $(this).addClass("l-dialog-collapse-over");
                   }, function () {
                       $(this).removeClass("l-dialog-extend-over l-dialog-collapse-over");
                   }).click(function () {
                       if (g.wintoggle.hasClass("l-dialog-extend")) {
                           g.wintoggle.removeClass("l-dialog-extend");
                           g.extend();
                       }
                       else {
                           g.wintoggle.addClass("l-dialog-extend");
                           g.collapse();
                       }
                   });
                }
            }
            else if (g.wintoggle) {
                g.wintoggle.remove();
                g.wintoggle = null;
            }
        },
        setShowMax: function (value) {
            var g = this, p = this.options;
            if (value) {
                if (!g.winmax) {
                    g.winmax = $('<div class="l-dialog-winbtn l-dialog-max"></div>').appendTo(g.dialog.winbtns)
                   .hover(function () {
                       if ($(this).hasClass("l-dialog-regain"))
                           $(this).addClass("l-dialog-regain-over");
                       else
                           $(this).addClass("l-dialog-max-over");
                   }, function () {
                       $(this).removeClass("l-dialog-regain-over l-dialog-max-over");
                   }).click(function () {
                       if (g.winmax.hasClass("l-dialog-regain")) {
                           g.winmax.removeClass("l-dialog-regain");
                           if (p.onRegain && p.onRegain() == false) return false;
                           g.dialog.width(g.lastWindowWidth).height(g.lastWindowHeight).css({ left: g.lastWindowLeft, top: g.lastWindowTop });
                           g.dialog.body.css({
                               width: g.lastBodyWidth
                           });
                           g.dialog.content.height(g.lastWindowHeight - 42 - $(".l-dialog-buttons", g.dialog).height());
                           g.dialog.content.currentheight = g.dialog.content.height();
                       }
                       else {
                           g.winmax.addClass("l-dialog-regain");
                           if (p.onMax && p.onMax() == false) return false;
                           g.lastWindowWidth = g.dialog.width();
                           g.lastWindowHeight = g.dialog.height();
                           g.lastWindowLeft = g.dialog.css('left');
                           g.lastWindowTop = g.dialog.css('top');
                           g.lastBodyWidth = g.dialog.body.width();
                           g.lastBodyHeight = g.dialog.body.height();
                           g.dialog.body.css({
                               width: $(window).width()
                           });
                           g.dialog.content.height($(window).height() - 42 - $(".l-dialog-buttons", g.dialog).height());
                           g.dialog.width($(window).width()).height($(window).height()).css({ left: 0, top: 0 });
                           g.dialog.content.currentheight = g.dialog.content.height();
                       }
                   });
                }
            }
            else if (g.winmax) {
                g.winmax.remove();
                g.winmax = null;
            }
        },
        updateBtnsWidth: function () {
            var g = this;
            var btnscount = $(">div", g.dialog.winbtns).length;
            g.dialog.winbtns.width(22 * btnscount);
        }
    };
    $.ligerManagers.Dialog.prototype.hide = $.ligerManagers.Dialog.prototype.hidden;

    ///	<param name="$" type="jQuery"></param>
    $.ligerDialog = {};
    $.ligerDialog.open = function (p) {
        p = $.extend({}, $.ligerDefaults.Dialog, $.ligerDefaults.DialogString, p || {});
        var zindex = p.zindex;
        var po = {
            applyWindowMask: function () {
                //$(".l-window-mask").remove();
                if (g.windowMask)
                    g.windowMask.remove();
                g.windowMask = g.dialog.WindowMask = $("<div class='l-window-mask' style='display: block;'></div>");
                g.dialog.WindowMask.height($(window).height() + $(window).scrollTop()).appendTo('body');
                g.dialog.WindowMask.css({ zIndex: zindex });
            },
            removeWindowMask: function () {
                //$(".l-window-mask").remove();
                g.dialog.WindowMask.remove();
            },
            applyDrag: function () {
                if ($.fn.ligerDrag)
                    g.dialog.ligerDrag({ handler: '.l-dialog-title', breakout: true });
            },
            applyResize: function () {
                if ($.fn.ligerResizable) {
                    g.dialog.ligerResizable({
                        onStopResize: function (current, e) {
                            var top = 0;
                            var left = 0;
                            if (!isNaN(parseInt(g.dialog.css('top'))))
                                top = parseInt(g.dialog.css('top'));
                            if (!isNaN(parseInt(g.dialog.css('left'))))
                                left = parseInt(g.dialog.css('left'));
                            if (current.diffTop != undefined) {
                                g.dialog.css({
                                    top: top + current.diffTop,
                                    left: left + current.diffLeft
                                });
                                g.dialog.body.css({
                                    width: current.newWidth - 26
                                });
                                $(".l-dialog-content", g.dialog.body).height(current.newHeight - 46 - $(".l-dialog-buttons", g.dialog).height());
                                g.dialog.content.currentheight = g.dialog.content.height();
                            }
                            return false;
                        }
                    });
                }
            },
            setImage: function () {
                if (p.type) {
                    if (p.type == 'success' || p.type == 'donne' || p.type == 'ok') {
                        $(".l-dialog-image", g.dialog).addClass("l-dialog-image-donne").show();
                        $(".l-dialog-content", g.dialog).css({ paddingLeft: 64, paddingBottom: 30, paddingRight: 5 });
                    }
                    else if (p.type == 'error') {
                        $(".l-dialog-image", g.dialog).addClass("l-dialog-image-error").show();
                        $(".l-dialog-content", g.dialog).css({ paddingLeft: 64, paddingBottom: 30, paddingRight: 5 });
                    }
                    else if (p.type == 'warn') {
                        $(".l-dialog-image", g.dialog).addClass("l-dialog-image-warn").show();
                        $(".l-dialog-content", g.dialog).css({ paddingLeft: 64, paddingBottom: 30, paddingRight: 5 });
                    }
                    else if (p.type == 'question') {
                        $(".l-dialog-image", g.dialog).addClass("l-dialog-image-question").show();
                        $(".l-dialog-content", g.dialog).css({ paddingLeft: 64, paddingBottom: 40, paddingRight: 5 });
                    }
                    else if (p.type == 'waitting') {
                        $(".l-dialog-image", g.dialog).addClass("l-dialog-image-waitting").show();
                        $(".l-dialog-image", g.dialog).css({ 'margin-left': 14 });
                    }
                }
            }
        };
        //public Object
        var g = new $.ligerManagers.Dialog(p, po);
        g.dialog = $('<div class="l-dialog"><table class="l-dialog-table" cellpadding="0" cellspacing="0" border="0"><tbody><tr><td class="l-dialog-tl"></td><td class="l-dialog-tc"><div class="l-dialog-tc-inner"><div class="l-dialog-icon"></div><div class="l-dialog-title"></div><div class="l-dialog-winbtns"><div class="l-dialog-winbtn l-dialog-close"></div></div></div></td><td class="l-dialog-tr"></td></tr><tr><td class="l-dialog-cl"></td><td class="l-dialog-cc"><div class="l-dialog-body"><div class="l-dialog-image"></div> <div class="l-dialog-content"></div><div class="l-dialog-buttons"><div class="l-dialog-buttons-inner"></div></td><td class="l-dialog-cr"></td></tr><tr><td class="l-dialog-bl"></td><td class="l-dialog-bc"></td><td class="l-dialog-br"></td></tr></tbody></table></div>');
        $('body').append(g.dialog);
        g.dialog.body = $(".l-dialog-body:first", g.dialog);
        if (p.allowClose == false) $(".l-dialog-close", g.dialog).remove();
        g.dialog.body = $(".l-dialog-body:first", g.dialog);
        g.dialog.header = $(".l-dialog-tc-inner:first", g.dialog);
        g.dialog.winbtns = $(".l-dialog-winbtns:first", g.dialog.header);
        g.dialog.buttons = $(".l-dialog-buttons:first", g.dialog);
        g.dialog.content = $(".l-dialog-content:first", g.dialog);
        if (p.target || p.url || p.type == "none") p.type = null;
        if (p.cls) g.dialog.addClass(p.cls);
        if (p.id) g.dialog.attr("id", p.id);
        if (p.showMax) g.setShowMax(true);
        if (p.showToggle) g.setShowToggle(true);

        g.updateBtnsWidth();

        //设置锁定屏幕、拖动支持 和设置图片
        if (p.modal)
            po.applyWindowMask();
        if (p.isDrag)
            po.applyDrag();
        if (p.isResize)
            po.applyResize();
        if (p.type)
            po.setImage();
        else {
            $(".l-dialog-image", g.dialog).remove();
            $(".l-dialog-content", g.dialog.body).addClass("l-dialog-content-noimage");
        }
        //设置主体内容
        if (p.target) {
            $(".l-dialog-content", g.dialog.body).prepend(p.target);
        }
        else if (p.url) {
            if (p.timeParmName) {
                p.url += p.url.indexOf('?') == -1 ? "?" : "&";
                p.url += p.timeParmName + "=" + new Date().getTime();
            }
            g.jiframe = $("<iframe frameborder='0'></iframe>");
            var framename = p.name ? p.name : "ligerwindow" + new Date().getTime();
            g.jiframe.attr("name", framename);
            g.jiframe.attr("id", framename);
            $(".l-dialog-content", g.dialog.body).prepend(g.jiframe);
            $(".l-dialog-content", g.dialog.body).addClass("l-dialog-content-nopadding");
            setTimeout(function () {
                g.jiframe.attr("src", p.url);
                g.jiframe[0].dialog = g;//增加窗口对dialog对象的引用
                g.frame = window.frames[g.jiframe.attr("name")];
            }, 0);
        }
        else if (p.content) {
            $(".l-dialog-content", g.dialog.body).html(p.content);
        }
        if (p.opener) g.dialog.opener = p.opener;
        //设置按钮
        if (p.buttons) {
            $(p.buttons).each(function (i, item) {
                var btn = $('<div class="l-dialog-btn"><div class="l-dialog-btn-l"></div><div class="l-dialog-btn-r"></div><div class="l-dialog-btn-inner"></div></div>');
                $(".l-dialog-btn-inner", btn).html(item.text);
                $(".l-dialog-buttons-inner", g.dialog.body).prepend(btn);
                item.width && btn.width(item.width);
                item.onclick && btn.click(function () { item.onclick(item, g, i) });
            });
        } else {
            $(".l-dialog-buttons", g.dialog).remove();
        }
        $(".l-dialog-buttons-inner", g.dialog).append("<div class='l-clear'></div>");

        //设置参数属性
        p.width && g.dialog.body.width(p.width - 26);
        if (p.height) {
            $(".l-dialog-content", g.dialog.body).height(p.height - 46 - $(".l-dialog-buttons", g.dialog).height());
            g.dialog.content.currentheight = p.height - 46 - $(".l-dialog-buttons", g.dialog).height();
        }
        p.title = p.title || p.titleMessage;
        p.title && $(".l-dialog-title", g.dialog).html(p.title);
        $(".l-dialog-title", g.dialog).bind("selectstart", function () { return false; });


        //设置事件
        $(".l-dialog-btn", g.dialog.body).hover(function () {
            $(this).addClass("l-dialog-btn-over");
        }, function () {
            $(this).removeClass("l-dialog-btn-over");
        });
        $(".l-dialog-tc .l-dialog-close", g.dialog).hover(function () {
            $(this).addClass("l-dialog-close-over");
        }, function () {
            $(this).removeClass("l-dialog-close-over");
        }).click(function () {
            if (p.isHidden)
                g.hidden();
            else
                g.close();
        });


        //位置初始化
        var left = 0;
        var top = 0;
        var width = p.width || g.dialog.width();
        if (p.left != null) left = p.left;
        else left = 0.5 * ($(window).width() - width);
        if (p.top != null) top = p.top;
        else top = 0.5 * ($(window).height() - g.dialog.height()) + $(window).scrollTop() - 10;
        if (left < 0) left = 0;
        if (top < 0) top = 0;
        g.dialog.css({ left: left, top: top });
        g.dialog.css({ zIndex: Math.abs(p.zindex) + 1 });
        g.dialog.show();


        $('body').bind('keydown.dialog', function (e) {
            var key = e.which;
            if (key == 13) {
                g.enter();
            }
            else if (key == 27) {
                g.esc();
            }
        });
        $.ligerui.addManager(g.dialog[0], g);
        return g;
    };
    $.ligerDialog.close = function () {
        var manager = $.ligerui.getManager(this);
        manager.close();
    };
    $.ligerDialog.show = function (p) {
        if ($(".l-dialog").length > 0) {
            $(".l-dialog,.l-window-mask").show();
            return;
        }
        return $.ligerDialog.open(p);
    };
    $.ligerDialog.hide = function () {
        $(".l-dialog,.l-window-mask").hide();
    };
    $.ligerDialog.alert = function (content, title, type, callback, zindex) {
        content = content || "";
        if (typeof (title) == "function") {
            callback = title;
            type = null;
        }
        else if (typeof (type) == "function") {
            callback = type;
        }
        var btnclick = function (item, Dialog, index) {
            Dialog.close();
            if (callback)
                callback(item, Dialog, index);
        };
        p = {
            content: content,
            showMax: false,
            buttons: [{ text: '确定', onclick: btnclick }]
        };
        if (typeof (title) == "string" && title != "") p.title = title;
        if (typeof (type) == "string" && type != "") p.type = type;
        p.zindex = zindex || 9000;
        $.ligerDialog.open(p);
    };

    $.ligerDialog.confirm = function (content, title, callback, zindex) {
        if (typeof (title) == "function") {
            callback = title;
            type = null;
        }
        var btnclick = function (item, Dialog) {
            Dialog.close();
            if (callback) {
                callback(item.type == 'ok');
            }
        };
        p = {
            type: 'question',
            content: content,
            showMax: false,
            buttons: [{ text: '是', onclick: btnclick, type: 'ok' }, { text: '否', onclick: btnclick, type: 'no' }]
        };
        if (typeof (title) == "string" && title != "") p.title = title;
        $.ligerDialog.open(p);
    };
    $.ligerDialog.warning = function (content, title, callback, zindex) {
        if (typeof (title) == "function") {
            callback = title;
            type = null;
        }
        var btnclick = function (item, Dialog) {
            Dialog.close();
            if (callback) {
                callback(item.type);
            }
        };
        p = {
            type: 'question',
            content: content,
            showMax: false,
            buttons: [{ text: '是', onclick: btnclick, type: 'yes' }, { text: '否', onclick: btnclick, type: 'no' }, { text: '取消', onclick: btnclick, type: 'cancel' }]
        };
        if (typeof (title) == "string" && title != "") p.title = title;
        p.zindex = zindex;
        $.ligerDialog.open(p);
    };
    $.ligerDialog.waitting = function (title, zindex) {
        title = title || $.ligerDefaults.Dialog.waittingMessage;
        zindex = zindex || 9000;
        $.ligerDialog.open({ cls: 'l-dialog-waittingdialog',  type: 'none', content: '<div style="padding:4px">'+title+'</div>', allowClose: false, showMax: false, zindex: zindex });
    };
    $.ligerDialog.closeWaitting = function () {
        $(".l-dialog-waittingdialog,.l-window-mask").remove();
    };
    $.ligerDialog.success = function (content, title, onBtnClick, zindex) {
        $.ligerDialog.alert(content, title, 'success', onBtnClick, zindex);
    };
    $.ligerDialog.error = function (content, title, onBtnClick, zindex) {
        $.ligerDialog.alert(content, title, 'error', onBtnClick, zindex);
    };
    $.ligerDialog.warn = function (content, title, onBtnClick, zindex) {
        $.ligerDialog.alert(content, title, 'warn', onBtnClick, zindex);
    };
    $.ligerDialog.question = function (content, title, zindex) {
        $.ligerDialog.alert(content, title, 'question', zindex);
    }


    $.ligerDialog.prompt = function (title, value, multi, callback) {
        var target = $('<input type="text" class="l-dialog-inputtext"/>');
        if (typeof (multi) == "function") {
            callback = multi;
        }
        if (typeof (value) == "function") {
            callback = value;
        }
        else if (typeof (value) == "boolean") {
            multi = value;
        }
        if (typeof (multi) == "boolean" && multi) {
            target = $('<textarea class="l-dialog-textarea"></textarea>');
        }
        if (typeof (value) == "string" || typeof (value) == "int") {
            target.val(value);
        }
        var btnclick = function (item, Dialog, index) {
            Dialog.close();
            if (callback) {
                callback(item.type == 'yes', target.val());
            }
        }
        p = {
            title: title,
            target: target,
            width: 320,
            showMax: false,
            buttons: [{ text: '确定', onclick: btnclick, type: 'yes' }, { text: '取消', onclick: btnclick, type: 'cancel' }]
        };
        return $.ligerDialog.open(p);
    };


})(jQuery);﻿/**
* jQuery ligerUI 1.0.1
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/

(function ($) {
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager) {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr) {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.ligerDefaults = $.ligerDefaults || {};

    //CheckBox manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Drag = function (options, po) {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.Drag.prototype = {};
    $.ligerDefaults.Drag = {
        onStartDrag: false,
        onDrag: false,
        onStopDrag: false,
        handler: null,
        //代理 拖动时的主体,可以是'clone'或者是函数,放回jQuery 对象
        proxy: true,
        revert: false,
        breakout: false,
        animate: true,
        onRevert: null,
        onEndRevert: null,
        //接收区域 jQuery对象或者jQuery选择字符
        receive: null,
        //进入区域
        onDragEnter: null,
        //在区域移动
        onDragOver: null,
        //离开区域
        onDragLeave: null,
        //在区域释放
        onDrop: null,
        disabled: false,
        proxyX: null,     //代理相对鼠标指针的位置,如果不设置则对应target的left
        proxyY: null
    };

    ///	<param name="$" type="jQuery"></param>  
    $.fn.ligerDrag = function (p) {
        p = $.extend({}, $.ligerDefaults.Drag, p || {});
        this.each(function () {
            if (this.useDrag) return;
            var g = {
                start: function (e) {
                    $('body').css('cursor', 'move');
                    g.current = {
                        target: g.target,
                        left: g.target.offset().left,
                        top: g.target.offset().top,
                        startX: e.pageX || e.screenX,
                        startY: e.pageY || e.clientY
                    };

                    g.cursor = "move";
                    g._createProxy(p.proxy, e);
                    //代理没有创建成功
                    if (p.proxy && !g.proxy) return false;
                    (g.proxy || g.handler).css('cursor', g.cursor);
                    $(document).bind("selectstart.drag", function () { return false; });
                    $(document).bind('mousemove.drag', function () {
                        g.drag.apply(g, arguments);
                    });

                    $(document).bind('mouseup.drag', function () {
                        g.stop.apply(g, arguments);
                    });
                    p.onStartDrag && p.onStartDrag();
                },
                drag: function (e) {
                    if (!g.current) return;
                    var pageX = e.pageX || e.screenX;
                    var pageY = e.pageY || e.screenY;
                    g.current.diffX = pageX - g.current.startX;
                    g.current.diffY = pageY - g.current.startY;
                    (g.proxy || g.handler).css('cursor', g.cursor);

                    var diffxx = pageX - g.current.left;

                    g._applyDrag();
                    //g._removeProxy();
                    p.onDrag && p.onDrag(g.current, e);
                },
                stop: function (e) {
                    $(document).unbind('mousemove.drag');
                    $(document).unbind('mouseup.drag');
                    $(document).unbind("selectstart.drag");

                    if (g.proxy) {
                        g._applyDrag(g.current.target);
                        g._removeProxy();
                    }
                    g.cursor = 'move';
                    g.current = null;
                    g.handler.css('cursor', g.cursor);
                    //alert(JSON.stringify($( g.target)));
                    p.onDrop && p.onDrop(g.target, p.receive, e);
                    p.onStopDrag && p.onStopDrag(g.target, e);
                },
                _revert: function (e) {
                    g.reverting = true;
                    g.proxy.animate({
                        left: g.current.left,
                        top: g.current.top
                    }, function () {
                        g.reverting = false;
                        g._removeProxy();
                        g.trigger('endRevert', [g.current, e]);
                        g.current = null;
                    });
                },
                _applyDrag: function (applyResultBody) {
                    applyResultBody = applyResultBody || g.proxy || g.target;
                    var cur = {}, changed = false;
                    var noproxy = applyResultBody == g.target;
                    if (g.current.diffX) {
                        if (noproxy || p.proxyX == null)
                            cur.left = g.current.left + g.current.diffX;
                        else
                            cur.left = g.current.startX + p.proxyX + g.current.diffX;
                        changed = true;
                    }
                    if (g.current.diffY) {
                        if (noproxy || p.proxyY == null)
                            cur.top = g.current.top + g.current.diffY;
                        else
                            cur.top = g.current.startY + p.proxyY + g.current.diffY;
                        changed = true;
                    }
                    if (p.breakout) {
                        if (cur.left < 0)
                            cur.left = 0;
                        if (cur.left + g.target.width() > $(document.body).width())
                            cur.left = $(document.body).width() - g.target.width();


                        if (cur.top < 0)
                            cur.top = 0;
                        if (cur.top + g.target.height() > $(document.body).height() && $(document.body).height() != 0)
                            cur.top = $(document.body).height() - g.target.height();
                    }
                    if (applyResultBody == g.target && g.proxy && p.animate) {
                        g.reverting = true;
                        applyResultBody.animate(cur, function () {
                            g.reverting = false;
                        });
                    }
                    else {
                        applyResultBody.css(cur);
                    }
                },
                _setReceive: function (receive) {
                    this.receiveEntered = {};
                    if (!receive) return;
                    if (typeof receive == 'string')
                        this.receive = $(receive);
                    else
                        this.receive = receive;
                },

                _createProxy: function (proxy, e) {
                    if (!proxy) return;
                    if (typeof proxy == 'function') {
                        g.proxy = proxy.call(this.options.target, g, e);
                    }
                    else if (proxy == 'clone') {
                        g.proxy = g.target.clone().css('position', 'absolute');
                        g.proxy.appendTo('body');
                    }
                    else {
                        g.proxy = $("<div style='position:absolute;border:1px solid #aaa;z-index:10001; background:#f2f1f1;opacity:0.5; filter:alpha(opacity=50);'></div>");
                        g.proxy.width(g.target.width()).height(g.target.height());
                        g.proxy.appendTo('body');
                    }
                    g.proxy.css({
                        left: p.proxyX == null ? g.current.left : g.current.startX + p.proxyX,
                        top: p.proxyY == null ? g.current.top : g.current.startY + p.proxyY
                    });
                },
                _removeProxy: function () {
                    if (g.proxy) {
                        g.proxy.remove();
                        g.proxy = null;
                    }
                }
            };
            g.target = $(this);
            if (p.handler == undefined || p.handler == null)
                g.handler = $(this);
            else
                g.handler = (typeof p.handler == 'string' ? $(p.handler, this) : p.handle);
            g.handler.hover(function () {
                $('body').css('cursor', 'move');
            }, function () {
                $("body").css("cursor", "default");
            }).mousedown(function (e) {
                g.start(e);
                return false;
            });
            this.useDrag = true;
            $.ligerui.addManager(this, g);
        });
        return $.ligerui.getManager(this);
    };
})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($)
{
    ///	<param name="$" type="jQuery"></param>
    $.fn.ligerEasyTab = function (p)
    {
        p = p || {};
        return this.each(function ()
        {
            if (this.manager) return;
            if ($(this).hasClass('l-hidden')) { return; }
            var g = {};
            this.manager = g;
            g.tabs = $(this);
            if (!g.tabs.hasClass("l-easytab")) g.tabs.addClass("l-easytab");
            var selectedIndex = 0;
            if ($("> div[lselected=true]", g.tabs).length > 0)
                selectedIndex = $("> div", g.tabs).index($("> div[lselected=true]", g.tabs));
            g.tabs.ul = $('<ul class="l-easytab-header"></ul>');
            $("> div", g.tabs).each(function (i, box)
            {
                var li = $('<li><span></span></li>');
                if (i == selectedIndex)
                    $("span", li).addClass("l-selected");
                if ($(box).attr("title"))
                    $("span", li).html($(box).attr("title"));
                g.tabs.ul.append(li);
                if (!$(box).hasClass("l-easytab-panelbox")) $(box).addClass("l-easytab-panelbox");
            });
            g.tabs.ul.prependTo(g.tabs); 
            //init  
            $(".l-easytab-panelbox:eq(" + selectedIndex + ")",g.tabs).show().siblings(".l-easytab-panelbox").hide();

            //add even 
            $("> ul:first span", g.tabs).click(function ()
            {
                if ($(this).hasClass("l-selected")) return;
                var i = $("> ul:first span", g.tabs).index(this);
                $(this).addClass("l-selected").parent().siblings().find("span.l-selected").removeClass("l-selected");
                $(".l-easytab-panelbox:eq(" + i + ")", g.tabs).show().siblings(".l-easytab-panelbox").hide();
            }).not("l-selected").hover(function ()
            {
                $(this).addClass("l-over");
            }, function ()
            {
                $(this).removeClass("l-over");
            });
        });
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.1.1 up by xhdcrm 
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/

(function ($) {
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager) {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr) {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.fn.ligerGetFormManager = function () {
        return $.ligerui.getManager(this);
    };

    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Form = {
        inputWidth: 180,//控件宽度        
        labelWidth: 90,//标签宽度        
        space: 40,//间隔宽度
        rightToken: '：',
        labelAlign: 'left', //标签对齐方式        
        align: 'left',//控件对齐方式        
        fields: [],//字段        
        appendID: true,//创建的表单元素是否附加ID        
        prefixID: "",//生成表单元素ID的前缀       
        toJSON: $.ligerui.toJSON //json解析函数
    };

    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Form = function (options, po) {
        this.options = options;
        this.po = po;
    };

    $.ligerManagers.Form.prototype = {
        getType: function () {
            return 'Form';
        },
        idPrev: function () {
            return 'Form';
        },
        render: function (form) {
            var g = this, p = this.options;

            var out = [];

            if (p.fields && p.fields.length) {
                $(p.fields).each(function (i, item) {
                    if (item.type == "group") {
                        out.push('<fieldset class="l-group">');
                        if (item.display) {
                            out.push('<legend >' + item.display + '</legend>')
                            //out.push('<div class="l-group-header"><span>' + item.display + '</span></div>');
                        }
                        var dom = g.buildDom(item.rows);
                        out.push(dom);
                        out.push('</fieldset>')
                    }
                })
            }
            form.append(out.join(''));
            form.ligerForm();
        },
        buildDom: function (field) {
            var g = this, p = this.options;
            var out = [];
            $(field).each(function (i, rows) {
                out.push('<dl>');
                $(rows).each(function (i, item) {
                    var label = item.label || item.display;
                    var labelWidth = item.labelWidth || item.labelwidth || p.labelWidth;
                    if (item.type == "icon") labelWidth = 0;
                    var labelAlign = item.labelAlign || p.labelAlign;
                    if (label) label += p.rightToken;

                    //label
                    if (item.type != "html" && item.type != "hidden") {
                        out.push('<dt style="');
                        if (labelWidth) {
                            out.push('width:' + labelWidth + 'px;');
                        }
                        if (labelAlign) {
                            out.push('text-align:' + labelAlign + ';');
                        }
                        out.push('">');
                        if (label) {
                            out.push(label);
                        }
                        out.push('</dt>');
                    }
                    else if (item.type == "html")
                    {
                        out.push('<dt style="');
                        if (item.width) {
                            out.push('width:' + item.width + 'px;');
                        }
                        
                        out.push('">');
                        if (item.html) {
                            out.push(item.html);
                        }
                        out.push('</dt>');
                    }


                    //input
                    var width = item.width || p.inputWidth;
                    if (item.type == "icon") width = 18;
                    if (item.type == "hidden") width = 1;
                    var align = item.align || item.textAlign || item.textalign || p.align;
                    if(item.type != "html" && item.type != "hidden")
                    {
                        out.push('<dt style="');
                        if (width) {
                            out.push('width:' + (width + 5) + 'px;');
                        }

                        if (align) {
                            out.push('text-align:' + align + ';');
                        }
                        out.push('">');
                        out.push(g.buildInput(item));                    

                        out.push('</dt>');
                        out.push(g.buildSpace(item));
                    }
                    
                });
                out.push('</dl>');
            })
            return out.join('');
        },
        buildInput: function (field) {
            var g = this, p = this.options;
            var width = field.width || p.inputWidth;
            var name = field.name || field.id;
            var out = [];
            if (field.type == "hidden") {
                out.push('<input type="hidden" id="' + name + '" name="' + name + '" />');
                return out.join('');
            }
            if (field.type == "html") {
                out.push(field.content);
                return out.join('');
            }
            if (field.textarea || field.type == "textarea") {
                out.push('<textarea ');
                field.cols && out.push('cols="' + field.cols + '" ');
                field.rows && out.push('rows="' + field.rows + '" ');
            }
            else if (field.type == "checkbox") {
                out.push('<input type="checkbox" style="margin-top:2px;" ');
            }
            else if (field.type == "radio") {
                out.push('<input type="radio" ');
            }
            else if (field.type == "password") {
                out.push('<input type="password" ');
            }
            else {
                out.push('<input type="text" ');
            }
            if (field.cssClass) {
                out.push('class="' + field.cssClass + '" ');
            }
            if (field.type) {
                out.push('ltype="' + field.type + '" ');
            }
            if (field.comboboxName && field.type == "select") {
                out.push('name="' + field.comboboxName + '"');
                if (p.appendID) {
                    out.push(' id="' + p.prefixID + field.comboboxName + '" ');
                }
            }
            else {
                out.push('name="' + name + '"');
                if (p.appendID) {
                    out.push(' id="' + name + '" ');
                }
            }
            //参数
            var fieldOptions = $.extend({
                width: width - 2
            }, field.options || {});

            if (field.options)
                out.push(" ligerui=\"" + field.options + "\" ");
            //验证参数
            if (field.validate) {
                out.push(" validate=\"" + field.validate + "\" ");
            }
            if (field.type == "textarea") {
                out.push(">");
                if (field.initValue)
                    out.push(field.initValue);
                out.push("</textarea>");
            }
            else {
                if (field.initValue)
                    out.push(" value = \"" + field.initValue + "\"");
                out.push(' />');
            }

            return out.join('');
        },
        buildSpace: function (field) {
            var g = this, p = this.options;
            var spaceWidth = field.space || field.spaceWidth || p.space;
            var out = [];
            out.push('<dt style="color:red;');
            if (spaceWidth) {
                out.push('width:' + spaceWidth + 'px;');
            }
            out.push('">');
            if (field.validate) {
                var p = field.validate;
                if (p.indexOf('{') != 0) p = "{" + p + "}";
                eval("p = " + p + ";");
                if (p.required)
                    out.push('*');
            }
            out.push('</dt>');
            return out.join('');
        }
    }


    ///	<param name="$" type="jQuery"></param>
    $.fn.ligerAutoForm = function (p) {
        this.each(function () {
            p = $.extend({}, $.ligerDefaults.Form, p || {});
            var po = {};
            var g = new $.ligerManagers.Form(p, po);

            g.render($(this));

            $.ligerui.addManager(this, g);
        })
        return $.ligerui.getManager(this);
    };
    $.fn.ligerForm = function (p) {
        p = $.extend({}, $.ligerDefaults.Form, p || {});
        return this.each(function () {
            $("input[ltype=text],input[ltype=password]", this).ligerTextBox();

            $("input[ltype=select],select[ltype=select]", this).ligerComboBox();

            $("input[ltype=spinner]", this).ligerSpinner();

            $("input[ltype=date]", this).ligerDateEditor();

            //$("input[ltype=radio]", this).ligerRadio();

            //$('input[ltype=checkbox]', this).ligerCheckBox();
        });
    }

})(jQuery);﻿/**
* jQuery ligerUI  1.1.1
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/

(function ($) {
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager) {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr) {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.fn.ligerGetGridManager = function () {
        return $.ligerui.getManager(this);
    };

    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Grid = {
        title: null,
        width: 'auto',                          //宽度值
        columnWidth: null,                      //默认列宽度
        resizable: true,                        //table是否可伸缩
        url: false,                             //ajax url
        usePager: true,                         //是否分页
        page: 1,                                //默认当前页 
        pageSize: 10,                           //每页默认的结果数
        pageSizeOptions: [10, 20, 30, 40, 50],  //可选择设定的每页结果数
        parms: [],                         //提交到服务器的参数
        columns: [],                          //数据源
        minColToggle: 1,                        //最小显示的列
        dataType: 'server',                     //数据源：本地(local)或(server),本地是将读取p.data
        dataAction: 'server',                    //提交数据的方式：本地(local)或(server),选择本地方式时将在客服端分页、排序
        showTableToggleBtn: false,              //是否显示'显示隐藏Grid'按钮 
        switchPageSizeApplyComboBox: false,     //切换每页记录数是否应用ligerComboBox
        allowAdjustColWidth: true,              //是否允许调整列宽     
        checkbox: false,                         //是否显示复选框
        allowHideColumn: true,                 //是否显示'切换列层'按钮
        enabledEdit: false,                      //是否允许编辑
        isScroll: true,                         //是否滚动
        onDragCol: null,                       //拖动列事件
        onToggleCol: null,                     //切换列事件
        onChangeSort: null,                    //改变排序事件
        onSuccess: null,                       //成功获取服务器数据的事件
        onDblClickRow: null,                     //双击行事件
        onSelectRow: null,                    //选择行事件
        onUnSelectRow: null,                   //取消选择行事件
        onBeforeCheckRow: null,                 //选择前事件，可以通过return false阻止操作(复选框)
        onCheckRow: null,                    //选择事件(复选框) 
        onBeforeCheckAllRow: null,              //选择前事件，可以通过return false阻止操作(复选框 全选/全不选)
        onCheckAllRow: null,                    //选择事件(复选框 全选/全不选)
        onBeforeShowData: null,                  //显示数据前事件，可以通过reutrn false阻止操作
        onAfterShowData: null,                 //显示完数据事件
        onError: null,                         //错误事件
        onSubmit: null,                         //提交前事件
        dateFormat: 'yyyy-MM-dd',              //默认时间显示格式
        InWindow: true,                        //是否以窗口的高度为准 height设置为百分比时可用
        statusName: '__status',                    //状态名
        method: 'post',                         //提交方式
        fixedCellHeight: true,                       //是否固定单元格的高度
        heightDiff: 0,                         //高度补差,当设置height:100%时，可能会有高度的误差，可以通过这个属性调整
        cssClass: null,                    //类名
        root: 'Rows',                       //数据源字段名
        record: 'Total',                     //数据源记录数字段名
        pageParmName: 'page',               //页索引参数名，(提交给服务器)
        pagesizeParmName: 'pagesize',        //页记录数参数名，(提交给服务器)
        sortnameParmName: 'sortname',        //页排序列名(提交给服务器)
        sortorderParmName: 'sortorder',      //页排序方向(提交给服务器)
        onReload: null,                    //刷新事件，可以通过return false来阻止操作
        onToFirst: null,                     //第一页，可以通过return false来阻止操作
        onToPrev: null,                      //上一页，可以通过return false来阻止操作
        onToNext: null,                      //下一页，可以通过return false来阻止操作
        onToLast: null,                      //最后一页，可以通过return false来阻止操作
        allowUnSelectRow: false,           //是否允许反选行
        dblClickToEdit: false,            //是否双击的时候才编辑
        alternatingRow: true,           //间隔行效果
        mouseoverRowCssClass: 'l-grid-row-over',
        enabledSort: true,                      //是否允许排序
        rowAttrRender: null,                  //行自定义属性渲染器(包括style，也可以定义)
        groupColumnName: null,                 //分组 - 列名
        groupColumnDisplay: '分组',             //分组 - 列显示名字
        groupRender: null,                     //分组 - 渲染器
        totalRender: null,                       //统计行(全部数据)
        delayLoad: false,                        //初始化时是否不加载
        where: null,                           //数据过滤查询函数,(参数一 data item，参数二 data item index)
        selectRowButtonOnly: false,            //复选框模式时，是否只允许点击复选框才能选择行
        onAfterAddRow: null,                     //增加行后事件
        onBeforeEdit: null,                      //编辑前事件
        onBeforeSubmitEdit: null,               //验证编辑器结果是否通过
        onAfterEdit: null,                       //结束编辑后事件
        onLoading: null,                        //加载时函数
        onbeforeLoaded: null,
        onLoaded: null,                          //加载完函数
        onContextmenu: null,                   //右击事件
        onRClickToSelect: false,                //右击行时是否选中
        contentType: null,                     //Ajax contentType参数
        checkboxColWidth: 27,                  //复选框列宽度
        detailColWidth: 29,                     //明细列宽度
        defaultCloseGroup: false,
        rowid: null,
        rowtype: null,
        onHideCol: null,
        /*
        treeGrid模式
        例子:tree:{
        columnName :'name', //如果不指定。将不会出现 可折叠的+/-。例子给出的是默认参数
        childrenName : 'children', //children的字段名。例子给出的是默认参数
        isParent : function(rowData){    //判断是否为父节点的（即显示-)判断函数。例子给出的是默认参数
        var exist = 'children' in rowData;
        return exist;
        },
        isExtend : function(rowData){     //判断是否张开。例子给出的是默认参数
        if('isextend' in rowData && rowData['isextend'] == false) 
        return false;
        return true;
        }
        }
        json格式：
        Rows:[{tite:'11.11',chidren:[...] }]
        */
        tree: null,                            //treeGrid模式
        isChecked: null,                       //复选框 初始化函数
        //获取时间
        renderDate: function (value) {
            var da;
            if (!value) return null;
            if (typeof value == 'object') {
                return value;
            }
            if (value.indexOf('Date') > -1) {
                da = eval('new ' + value.replace('/', '', 'g').replace('/', '', 'g'));
            } else {
                da = eval('new Date("' + value + '");');
            }
            return da;
        }
    };
    $.ligerDefaults.GridString = {
        errorMessage: '发生错误',
        pageStatMessage: '显示从{from}到{to}，总 {total} 条 。每页显示：{pagesize}',
        pageTextMessage: 'Page',
        loadingMessage: '加载中...',
        findTextMessage: '查找',
        noRecordMessage: '没有符合条件的记录存在',
        isContinueByDataChanged: '数据已经改变,如果继续将丢失数据,是否继续?'
    };

    //Grid manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Grid = function (options, po) {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.Grid.prototype = {
        //刷新数据
        loadData: function (loadDataParm) {
            var po = this.po, g = this, p = this.options;
            g.loading = true;
            var clause = null;
            var loadServer = true;
            if (typeof (loadDataParm) == "function") {
                clause = loadDataParm;
                loadServer = false;
            }
            else if (typeof (loadDataParm) == "boolean") {
                loadServer = loadDataParm;
            }
            else if (typeof (loadDataParm) == "object" && loadDataParm) {
                loadServer = false;
                p.dataType = "local";
                p.data = loadDataParm;
            }
            //参数初始化
            if (!p.newPage) p.newPage = 1;
            if (p.dataAction == "server") {
                if (!p.sortOrder) p.sortOrder = "asc";
            }
            var param = [];
            if (p.parms && p.parms.length) {
                $(p.parms).each(function () {
                    param.push({ name: this.name, value: this.value });
                });
            }
            if (p.dataAction == "server") {
                if (p.usePager) {
                    param.push({ name: p.pageParmName, value: p.newPage });
                    param.push({ name: p.pagesizeParmName, value: p.pageSize });
                }
                if (p.sortName) {
                    param.push({ name: p.sortnameParmName, value: p.sortName });
                    param.push({ name: p.sortorderParmName, value: p.sortOrder });
                }
                if (p.HttpContext) {
                    param.push({ name: "HttpContext", value: true });
                }
            };
            $(".l-bar-btnload span", g.toolbar).addClass("l-disabled");
            if (p.dataType == "local") {
                g.data = $.extend({}, p.data);
                g.filteredData = $.extend({}, g.data);
                if (clause)
                    g.filteredData[p.root] = po.searchData(g.filteredData[p.root], clause);
                if (p.usePager)
                    g.currentData = po.getCurrentPageData(g.filteredData);
                else {
                    g.currentData = $.extend({}, g.filteredData);
                }
                po.showData(g.currentData);
            }
            else if (p.dataAction == "local" && !loadServer) {
                if (g.data && g.data[p.root]) {
                    g.filteredData = $.extend({}, g.data);
                    if (clause)
                        g.filteredData[p.root] = po.searchData(g.filteredData[p.root], clause);
                    g.currentData = po.getCurrentPageData(g.filteredData);
                    po.showData(g.currentData);
                }
            }
            else {
                if (p.onLoading) p.onLoading(g);
                else g.gridloading.show();
                setTimeout(function () {
                    g.loadServerData(param, clause);
                }, 10);
            }
            g.loading = false;
        },
        loadServerData: function (param, clause) {
            var po = this.po, g = this, p = this.options;
            var ajaxOptions = {
                type: p.method,
                url: p.url,
                data: param,
                async: false,
                dataType: 'json',
                beforeSend: function () {
                    p.onbeforeLoaded && p.onbeforeLoaded();
                },
                success: function (data) {
                    if (p.onSuccess) p.onSuccess(data, g);
                    if (!data || !data[p.root] || !data[p.root].length) {
                        g.currentData = g.data = { Rows: [], Total: 0 };
                        g.currentData[p.root] = g.data[p.root] = [];
                        g.currentData[p.record] = g.data[p.record] = 0;
                        po.showData(g.currentData);
                        return;
                    }
                    g.data = $.extend({}, data);
                    for (var rowindex in g.data[p.root]) {
                        if (g.data[p.root][rowindex][p.statusName] == undefined)
                            g.data[p.root][rowindex][p.statusName] = '';
                    }
                    if (p.dataAction == "server") {
                        g.currentData = g.data;
                    }
                    else {
                        g.filteredData = $.extend({}, g.data);
                        if (clause) g.filteredData[p.root] = po.searchData(g.filteredData[p.root], clause);
                        g.currentData = po.getCurrentPageData(g.filteredData);
                    }
                    setTimeout(function () { po.showData(g.currentData); }, 10);
                },
                complete: function () {
                    if (p.onLoaded) {
                        p.onLoaded(g);
                    }
                    else {
                        setTimeout(function () {
                            g.gridloading.hide();
                        }, 10);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    g.currentData = g.data = {};
                    g.currentData[p.root] = g.data[p.root] = [];
                    g.currentData[p.record] = g.data[p.record] = 0;
                    g.gridloading.hide();
                    $(".l-bar-btnload span", g.toolbar).removeClass("l-disabled");
                    try { if (p.onError) p.onError(XMLHttpRequest, textStatus, errorThrown); } catch (e) { }
                }
            };
            if (p.contentType) ajaxOptions.contentType = p.contentType;
            $.ajax(ajaxOptions);
        },
        //add function()
        setURL: function (linkurl) {
            var po = this.po, g = this, p = this.options;
            p.newPage = 1;
            p.url = linkurl;
        },
        //add function()
        GetDataByURL: function (linkurl) {
            var po = this.po, g = this, p = this.options;

            p.newPage = 1;
            p.url = linkurl;
            g.loadData(true);
        },
        test: function () {
            var po = this.po, g = this, p = this.options;
            var rows = g.getRow(g.currentData[p.root]);
            for (var row in rows) {
                g.deleteRow(row);
            }
        },
        //add function
        setCheck: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            var row = $("tbody:first > tr[growid=" + rowParm + "]", g.gridbody);
            row.addClass("l-checked");
        },
        //add function
        setUnCheck: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            var row = $("tbody:first > tr[growid=" + rowParm + "]", g.gridbody);
            row.removeClass("l-checked");
        },
        //add function
        collapseleaf: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            var row = $("tbody:first > tr[treelevel=" + rowParm + "]", g.gridbody);
            for (var j = 0; j < row.length; j++) {
                g.collapse(row[j]);
            }
        },
        //add function
        expandleaf: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            var row = $("tbody:first > tr[treelevel=" + rowParm + "]", g.gridbody);
            for (var j = 0; j < row.length; j++) {
                g.expand(row[j]);
            }
        },
        //add function
        collapseAll: function () {
            var po = this.po, g = this, p = this.options;
            $(".l-grid-tree-link-open", g.gridbody).click();
        },
        //add function
        expandAll: function () {
            var po = this.po, g = this, p = this.options;
            $(".l-grid-tree-link-close", g.gridbody).click();
        },
        //addfunction()
        getColumnDateByType: function (columnName, type) {
            var po = this.po, g = this, p = this.options;

            var totalsummaryArr = [];

            var data = g.getCurrentData();
            var sum = 0, count = 0, avg = 0;
            var max = parseFloat(g.getColumn(columnName));
            var min = parseFloat(g.getColumn(columnName));
            for (var i = 0; i < data.length; i++) {
                var value;
                //alert(JSON.stringify(data[i]));
                if (typeof (data[i][columnName]) == 'number')
                    value = parseFloat(data[i][columnName]);
                else {
                    //alert(data.length)                    
                    value = parseFloat(data[i][columnName].replace(/\$|\,/g, ''));
                }
                if (!value) continue;
                sum += value;
                count += 1;
                if (value > max) max = value;
                if (value < min) min = value;
            }
            avg = sum * 1.0 / data.length;

            switch (type) {
                case "sum": return sum;
                    break;
                case "max": return max;
                    break;
                case "min": return min;
                    break;
                case "count": return count;
                    break;
                case "avg": return avg;
                    break;
                default: return sum;
            }


        },
        //add function
        showData: function (data) {
            var po = this.po, g = this, p = this.options;
            po.showData(data);
        },
        //add function
        onResize: function () {
            var po = this.po, g = this, p = this.options;
            po.onResize();
        },
        //add function
        getCurrentData: function () {
            var po = this.po, g = this, p = this.options;

            var rows = $("tbody:first > l-grid-row", g.gridbody);
            var rowdata = [];
            $("tbody > .l-grid-row", g.gridbody).each(function (i, row) {
                var rowid = $(row).attr("rowid");
                rowdata.push(g.getRow(rowid));
            });

            return rowdata;
        },

        setOptions: function (parms) {
            var po = this.po, g = this, p = this.options;
            $.extend(p, parms);
            if (parms.data) {
                g.data = parms.data;
                p.dataType = "local";
            }
        },
        reColumns: function (columns) {
            var po = this.po, g = this, p = this.options;
            g.columns = [];
            //$("div:first", g.gridheader).empty();
            $(".l-grid-hd-cell").each(function () {
                $(this).remove();
            });

            g.setOptions({ columns: columns });
            po.init();
        },
        stringToDate: function (obj) {
            var po = this.po, g = this, p = this.options;
            if (obj instanceof Date) return obj;
            var myDate = new Date();
            try {
                myDate.setYear(parseInt(obj.substring(0, 4), 10));
                myDate.setMonth(parseInt(obj.substring(5, 7) - 1, 10));
                myDate.setDate(parseInt(obj.substring(8, 10), 10));
                if (obj.length > 10) {
                    myDate.setHours(parseInt(obj.substring(11, 13), 10));
                    myDate.setMinutes(parseInt(obj.substring(14, 16), 10));
                }
                if (obj.length > 16) {
                    myDate.setSeconds(parseInt(obj.substring(17, 19), 10));
                }
            }
            catch (e) {
            }
            return myDate;
        },
        getFormatDate: function (date, dateformat) {
            var po = this.po, g = this, p = this.options;
            if (isNaN(date)) return null;
            var format = dateformat;
            var o = {
                "M+": date.getMonth() + 1,
                "d+": date.getDate(),
                "h+": date.getHours(),
                "m+": date.getMinutes(),
                "s+": date.getSeconds(),
                "q+": Math.floor((date.getMonth() + 3) / 3),
                "S": date.getMilliseconds()
            }
            if (/(y+)/.test(format)) {
                format = format.replace(RegExp.$1, (date.getFullYear() + "")
            .substr(4 - RegExp.$1.length));
            }
            for (var k in o) {
                if (new RegExp("(" + k + ")").test(format)) {
                    format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                : ("00" + o[k]).substr(("" + o[k]).length));
                }
            }
            return format;
        },
        endEdit: function () {
            var po = this.po, g = this, p = this.options;
            if (!g.grideditor.editingCell) return;
            var cell = g.grideditor.editingCell;
            var value = g.grideditor.editingValue;
            g.grideditor.html("").hide();
            var row = $(cell).parent();
            var rowindex = row.attr("rowindex");
            var rowid = row.attr("rowid");
            var columnindex = $(cell).attr("columnindex");
            var columnname = $(cell).attr("columnname");
            var column = g.columns[columnindex];
            var rowdata = g.getRow(rowid);
            var editParm = {
                record: rowdata,
                value: value,
                column: column,
                columnname: columnname,
                columnindex: columnindex,
                rowindex: rowindex,
                rowObj: row[0],
                cellObj: cell
            };
            if (p.onAfterEdit) p.onAfterEdit(editParm);
            g.grideditor.editingCell = null;
            g.grideditor.editingValue = null;
        },
        setWidth: function (w) {

        },
        setHeight: function (h) {
            var po = this.po, g = this, p = this.options;
            if (p.title) h -= 24;
            if (p.usePager) h -= 32;
            if (p.totalRender) h -= 25;
            h -= 23 * (g.getMulHeaderLevel() - 1);
            h -= 22;
            h > 0 && g.gridbody.height(h);
        },
        //是否启用明细模式
        enabledDetail: function () {
            if (this.options.detail && this.options.detail.onShowDetail) return true;
            return false;
        },
        deleteSelectedRow: function () {
            var po = this.po, g = this, p = this.options;
            if (p.checkbox) {
                $("tbody:first > tr.l-checked", g.gridbody).each(function () {
                    g.deleteRow(this);
                });
            }
            else {
                var row = $("tbody:first > tr.l-selected", g.gridbody);
                if (row.length == 0) return;
                g.deleteRow(row[0]);
            }
        },
        deleteRow: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            var rowObj = g.getRowObj(rowParm);
            if (!rowObj) return;
            g.popup.hide();
            g.endEdit();
            var rowid = $(rowObj).attr("rowid");
            if (p.tree && g.hasChildren(rowObj)) {
                $("tbody:first > tr[parentrowid=" + rowid + "]", g.gridbody).each(function () {
                    g.deleteRow(this);
                });
            }

            $(rowObj).remove();
            //alert(JSON.stringify(g.data.Rows));
            g.deleteData(rowid);
            g.isDataChanged = true;
        },
        deleteData: function (rowid) {
            var po = this.po, g = this, p = this.options;
            g.records[rowid][p.statusName] = 'delete';
            //alert(JSON.stringify(g.records));
        },
        //add function
        updateCelldata: function (rowid, columindex, value) {
            var po = this.po, g = this, p = this.options;

            var rowdata = g.getRow(rowid);
            var column = g.columns[columindex];
            if (!column) return;
            var columnname = column.name;
            if (!columnname) return;

            //alert(column.type);

            if (column.type == 'int')
                rowdata[columnname] = parseInt(value);
            else if (column.type == 'float')
                rowdata[columnname] = parseFloat(value);
            else if (column.type == 'date') {
                var dv = p.renderDate(value);
                if (!dv || isNaN(dv))
                    dv = g.stringToDate(value);
                rowdata[columnname] = dv;
            }
            else {
                rowdata[columnname] = value;
            }

            if (rowdata[p.statusName] != 'add')
                rowdata[p.statusName] = 'update';
            g.isDataChanged = true;
        },
        //add function
        updateCelldataByColumname: function (rowid, columname, value) {
            var po = this.po, g = this, p = this.options;

            var rowdata = g.getRow(rowid);
            if (!rowdata) return;

            rowdata[columname] = value;

            if (rowdata[p.statusName] != 'add')
                rowdata[p.statusName] = 'update';
            g.isDataChanged = true;
        },
        //add function
        getAllRows: function () {
            var po = this.po, g = this, p = this.options;
            var rows = $("tbody:first > .l-grid-row", g.gridbody);
            var rowdata = [];
            $("tbody:first > .l-grid-row", g.gridbody).each(function (i, row) {
                var rowid = $(row).attr("rowid");
                rowdata.push(g.getRow(rowid));
            });
            return rowdata;
        },
        updateCell: function (cell, value, rowParm) {
            var po = this.po, g = this, p = this.options;
            var columnindex;
            var column;
            var cellObj;
            if (typeof (cell) == "number") {
                columnindex = cell;
                column = g.columns[columnindex];
                cellObj = $("td[columnindex=" + columnindex + "]", rowObj)[0];
            }
            else if (typeof (cell) == "string") {
                var rowObj = g.getRowObj(rowParm);
                cellObj = $("td[columnname=" + cell + "]", rowObj)[0];
                columnindex = $(cellObj).attr("columnindex");
                column = g.columns[columnindex];
            }
            else {
                cellObj = cell;
                columnindex = $(cellObj).attr("columnindex");
                column = g.columns[columnindex];
            }

            var row = $(cellObj).parent();
            var rowindex = row.attr("rowindex");
            var rowid = row.attr("rowid");
            var rowData = g.getRow(rowid);
            g.updateData(cellObj, value);
            var cellContent = po.getCellContent(rowData, rowindex, value, column, p.tree, row.attr("treelevel"), rowid);
            $(".l-grid-row-cell-inner:first", cellObj).html(cellContent); //alert(cellContent);
        },
        updateData: function (cell, value, rowObj) {
            var po = this.po, g = this, p = this.options;
            if (typeof (cell) == "string") {
                var rowindex = $(rowObj).attr("rowindex");
                var rowid = $(rowObj).attr("rowid");
                var rowdata = g.getRow(rowid);
                rowdata[cell] = value;
                if (rowdata[p.statusName] != 'add')
                    rowdata[p.statusName] = 'update';
                g.isDataChanged = true;
                return;
            }
            var columnindex = $(cell).attr("columnindex");
            var column = g.columns[columnindex];
            if (!column) return;
            var columnname = column.name;
            if (!columnname) return;
            var row = $(cell).parents(".l-grid-row:eq(0)");
            var rowindex = row.attr("rowindex");
            var rowid = row.attr("rowid");
            var rowdata = g.getRow(rowid);
            if (column.type == 'int')
                rowdata[columnname] = parseInt(value);
            else if (column.type == 'float')
                rowdata[columnname] = parseFloat(value);
            else if (column.type == 'date') {
                var dv = p.renderDate(value);
                if (!dv || isNaN(dv))
                    dv = g.stringToDate(value);
                rowdata[columnname] = dv;
            }
            else
                rowdata[columnname] = value;
            if (rowdata[p.statusName] != 'add')
                rowdata[p.statusName] = 'update';
            g.isDataChanged = true;
        },
        addRows: function (rowdataArr) {
            var g = this;
            $(rowdataArr).each(function () {
                g.addRow(this);
            });
        },
        addRow: function (rowdata, rowParm, isBefore, parentRow) {
            var po = this.po, g = this, p = this.options;
            if (!rowdata) rowdata = {};
            var treelevel, parentrowid, parentRowObj, parentRowData, parentRowIsOpened;
            if (parentRow) {
                parentRowObj = g.getRowObj(parentRow);
                treelevel = parseInt($(parentRowObj).attr("treelevel")) + 1;
                parentrowid = $(parentRowObj).attr("rowid");
                parentRowData = g.getRow(parentrowid);
                parentRowIsOpened = $(".l-grid-tree-link:first", parentRowObj).hasClass("l-grid-tree-link-open");
            }

            var olddatanumber = parentRowData ? parentRowData[p.tree.childrenName].length : g.currentData[p.root].length;
            var rowObj = g.getRowObj(rowParm);
            var rowindex = rowObj
                ? (parseInt($(rowObj).attr("rowindex")) + (isBefore ? 0 : 1))
                : olddatanumber;
            var rowHTML = po.getHtmlFromData([rowdata], p.tree, treelevel ? treelevel : 1, parentrowid);
            var row = $(rowHTML);
            po.recordRow(row[0]);
            row.attr("rowindex", rowindex).removeClass("l-grid-row-last");
            if (parentRow && !parentRowIsOpened) row.hide();
            if (rowindex == olddatanumber) {
                if (parentRowData)
                    parentRowData[p.tree.childrenName][rowindex] = rowdata;
                else
                    g.currentData[p.root][rowindex] = rowdata;
                if (!p.usePager && !g.isTotalSummary()) {
                    $("tbody:first > .l-grid-row:last", g.gridbody).removeClass("l-grid-row-last");
                    row.addClass("l-grid-row-last");
                }
            }
            else {
                if (parentRowData)
                    parentRowData[p.tree.childrenName].splice(rowindex, 0, rowdata);
                else
                    g.currentData[p.root].splice(rowindex, 0, rowdata);
                var selectexpr = p.tree ? "tr[parentrowid=" + parentrowid + "][treelevel=" + treelevel + "]" : "tr";
                $(rowObj).nextAll(selectexpr).add(rowObj).each(function () {
                    var ri = $(this).attr("rowindex");
                    if (ri >= rowindex)
                        $(this).attr("rowindex", parseInt(ri) + 1);
                });
            }
            if ($("tbody", g.gridbody).length == 0) {
                g.gridbody.html('<div class="l-grid-body-inner"><table class="l-grid-body-table" cellpadding=0 cellspacing=0><tbody></tbody></table></div>');
            }
            if (rowObj != undefined) {
                if (isBefore)
                    $(rowObj).before(row);
                else
                    $(rowObj).after(row);
            }
            else {
                $("tbody:first", g.gridbody).append(row);
            }
            rowdata[p.statusName] = 'add';
            //添加事件
            po.setRowEven(row[0]);
            //标识状态
            g.isDataChanged = true;

            p.total = p.total ? (p.total + 1) : 1;
            p.pageCount = Math.ceil(p.total / p.pageSize);
            po.buildPager();
            if (p.onAfterAddRow) p.onAfterAddRow(row, rowdata);
            return row;
        },
        updateRow: function (rowDom, newRowData) {
            var po = this.po, g = this, p = this.options;
            var rowdata = g.getRow(rowDom);
            var rowObj = g.getRowObj(rowDom);
            //标识状态
            g.isDataChanged = true;
            if (newRowData) {
                for (var columnname in newRowData) { //alert(columnname);
                    if (columnname == p.statusName) continue;
                    rowdata[columnname] = newRowData[columnname];
                    var cellobj = $("> .l-grid-row-cell[columnname=" + columnname + "]", rowObj); //alert(JSON.stringify( cellobj));
                    //if (cellobj.length == 0) continue;
                    var columnindex = cellobj.attr("columnindex");
                    var column = g.columns[columnindex];
                    g.updateCell(cellobj, newRowData[columnname]);
                }
                rowdata[p.statusName] = 'update';
            }
            return rowdata;
        },
        getData: function () {
            var po = this.po, g = this, p = this.options;
            if (g.currentData == null) return null;
            return g.currentData[p.root];
        },
        getColumn: function (columnname) {
            var po = this.po, g = this, p = this.options;
            for (i = 0; i < g.columns.length; i++) {
                if (g.columns[i].name == columnname) {
                    return g.columns[i];
                }
            }
            return null;
        },
        getColumnType: function (columnname) {
            var po = this.po, g = this, p = this.options;
            for (i = 0; i < g.columns.length; i++) {
                if (g.columns[i].name == columnname) {
                    if (g.columns[i].type) return g.columns[i].type;
                    return "string";
                }
            }
            return null;
        },
        //是否包含汇总
        isTotalSummary: function () {
            var po = this.po, g = this, p = this.options;
            for (var i = 0; i < g.columns.length; i++) {
                if (g.columns[i].totalSummary) return true;
            }
            return false;
        },
        getMulHeaderLevel: function () {
            var po = this.po, g = this, p = this.options;
            if (!p.columns.length) return 1;
            var level = 0;
            var currentColumn = p.columns[0];
            while (currentColumn) {
                level++;
                if (!currentColumn.columns || !currentColumn.columns.length) break;
                currentColumn = currentColumn.columns[0];
            }
            return level;
        },
        getColumns: function (columnLevel) {
            var po = this.po, g = this, p = this.options;
            if (columnLevel <= 1) return p.columns;
            return g.getLevelColumns({ columns: p.columns }, 0, columnLevel);
        },
        getLevelColumns: function (column, level, columnLevel) {
            var po = this.po, g = this, p = this.options;
            if (level == columnLevel) return [column];
            var columns = [];
            for (var i = 0; column.columns && i < column.columns.length; i++) {
                var currentColumns = g.getLevelColumns(column.columns[i], level + 1, columnLevel);
                $(currentColumns).each(function () {
                    columns.push(this);
                });
            }
            return columns;
        },
        getMulHeaders: function (columnLevel) {
            var po = this.po, g = this, p = this.options;
            var getColumnNumber = function (column) {
                //if (!column) return 1;
                if (!column.columns || !column.columns.length) return 1;
                var number = 0;
                for (var i = 0; i < column.columns.length; i++) {
                    number += getColumnNumber(column.columns[i]);
                }
                return number;
            };
            var currentLevelColumns = g.getColumns(columnLevel);
            var mulHeaders = [];
            for (var i = 0; i < currentLevelColumns.length; i++) {
                mulHeaders.push({
                    display: currentLevelColumns[i]['display'],
                    number: getColumnNumber(currentLevelColumns[i])
                });
            }
            return mulHeaders;
        },
        //改变排序
        changeSort: function (columnName, sortOrder) {
            var po = this.po, g = this, p = this.options;
            if (g.loading) return true;
            if (p.dataAction == "local") {
                var columnType = g.getColumnType(columnName);
                if (!g.sortedData)
                    g.sortedData = $.extend({}, g.filteredData);
                if (p.sortName == columnName) {
                    g.sortedData[p.root].reverse();
                } else {
                    g.sortedData[p.root].sort(function (data1, data2) {
                        return po.compareData(data1, data2, columnName, columnType);
                    });
                }
                if (p.usePager)
                    g.currentData = po.getCurrentPageData(g.sortedData);
                else
                    g.currentData = g.sortedData;
                po.showData(g.currentData);
            }
            p.sortName = columnName;
            p.sortOrder = sortOrder;
            if (p.dataAction == "server") {
                g.loadData(p.where);
            }
        },
        //改变分页
        changePage: function (ctype) {
            var po = this.po, g = this, p = this.options;
            if (g.loading) return true;
            if (g.isDataChanged && !confirm(p.isContinueByDataChanged))
                return false;
            //计算新page
            switch (ctype) {
                case 'first': if (p.page == 1) return; p.newPage = 1; break;
                case 'prev': if (p.page == 1) return; if (p.page > 1) p.newPage = parseInt(p.page) - 1; break;
                case 'next': if (p.page >= p.pageCount) return; p.newPage = parseInt(p.page) + 1; break;
                case 'last': if (p.page >= p.pageCount) return; p.newPage = p.pageCount; break;
                case 'input':
                    var nv = parseInt($('.pcontrol input', g.toolbar).val());
                    if (isNaN(nv)) nv = 1;
                    if (nv < 1) nv = 1;
                    else if (nv > p.pageCount) nv = p.pageCount;
                    $('.pcontrol input', g.toolbar).val(nv);
                    p.newPage = nv;
                    break;
            }
            if (p.newPage == p.page) return false;
            if (p.newPage == 1) {
                $(".l-bar-btnfirst span", g.toolbar).addClass("l-disabled");
                $(".l-bar-btnprev span", g.toolbar).addClass("l-disabled");
            }
            else {
                $(".l-bar-btnfirst span", g.toolbar).removeClass("l-disabled");
                $(".l-bar-btnprev span", g.toolbar).removeClass("l-disabled");
            }
            if (p.newPage == p.pageCount) {
                $(".l-bar-btnlast span", g.toolbar).addClass("l-disabled");
                $(".l-bar-btnnext span", g.toolbar).addClass("l-disabled");
            }
            else {
                $(".l-bar-btnlast span", g.toolbar).removeClass("l-disabled");
                $(".l-bar-btnnext span", g.toolbar).removeClass("l-disabled");
            }
            if (p.onChangePage)
                p.onChangePage(p.newPage);
            if (p.dataAction == "server") {
                g.loadData(p.where);
            }
            else {
                g.currentData = po.getCurrentPageData(g.filteredData);
                po.showData(g.currentData);
            }
        },
        getCheckedRows: function () {
            var po = this.po, g = this, p = this.options;
            var rows = $("tbody:first > .l-checked", g.gridbody);
            var rowdata = [];
            $("tbody:first > .l-checked", g.gridbody).each(function (i, row) {
                var rowid = $(row).attr("rowid");
                rowdata.push(g.getRow(rowid));
            });
            return rowdata;
        },

        getSelectedRow: function () {
            var po = this.po, g = this, p = this.options;
            var row = $("tbody:first > .l-selected", g.gridbody);
            var rowid = row.attr("rowid");
            return g.getRow(rowid);
        },
        getCheckedRowObjs: function () {
            var po = this.po, g = this, p = this.options;
            return $("tbody:first > .l-checked", g.gridbody).get();
        },
        getSelectedRowObj: function () {
            var po = this.po, g = this, p = this.options;
            var row = $("tbody:first > .l-selected", g.gridbody);
            if (row.length == 0) return null;
            return row[0];
        },
        getRowObj: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            if (typeof (rowParm) == "string" || typeof (rowParm) == "number") {
                return $("tbody:first > .l-grid-row[rowid=" + rowParm + "]", g.gridbody).get(0);
            }
            else if (typeof (rowParm) == "object") {
                if (!rowParm) return null;
                if (typeof (rowParm.nodeType) != "undefined" && rowParm.nodeType == 1)
                    return rowParm;
                else {
                    for (var p in g.records) {
                        if (g.records[p] == rowParm)
                            return $("tbody:first > .l-grid-row[rowid=" + p + "]", g.gridbody).get(0);
                    }
                }
            }
            return null;
        },
        getRow: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            if (typeof (rowParm) == "string" || typeof (rowParm) == "number") {
                return g.records[parseInt(rowParm)];
            }
            else if (typeof (rowParm) == "object") {
                if (!rowParm) return null;
                if (typeof (rowParm.nodeType) != "undefined" && rowParm.nodeType == 1)
                    return g.records[$(rowParm).attr("rowid")];
                else
                    return rowParm;
            }
            return null;
        },

        changeHeaderText: function (columnparm, headerText) {
            var po = this.po, g = this, p = this.options;
            var headercell = null;
            var columnindex = -1;
            if (typeof (columnparm) == "number") {
                columnindex = columnparm;
                headercell = $(".l-grid-hd-cell[columnindex='" + columnparm + "']", g.gridheader);
            }
            else if (typeof (columnparm) == "string") {
                headercell = $(".l-grid-hd-cell[columnname='" + columnparm + "']", g.gridheader);
                if (!headercell) return;
                columnindex = headercell.attr("columnindex");
            }
            if (!headercell) return;
            $(".l-grid-hd-cell-text", headercell).html(headerText);
            if (p.allowHideColumn) {
                $(':checkbox[columnindex=' + columnindex + "]", g.popup).parent().next().html(headerText);
            }
        },
        getParent: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            var rowObj = g.getRowObj(rowParm);
            if (!rowObj) return;
            var parentrowid = $(rowObj).attr("parentrowid");
            if (parentrowid == undefined) return null;
            return g.getRow(parentrowid);
        },
        getChidren: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            if (!p.tree) return null;
            var rowData = g.getRow(rowParm);
            if (!rowData) return null;
            return rowData[p.tree.childrenName];
        },
        isLeaf: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            var rowObj = g.getRowObj(rowParm);
            if (!rowObj) return;
            return !$("> td > div > .l-grid-tree-space:last", rowObj).hasClass("l-grid-tree-link");
        },
        hasChildren: function (rowParm) {
            var po = this.po, g = this, p = this.options;
            var rowObj = g.getRowObj(rowParm);
            if (!rowObj) return;
            var treelevel = $(rowObj).attr("treelevel");
            var nextRow = $(rowObj).next(".l-grid-row");
            if (nextRow.length == 0) return false;
            var nextRowTreelevel = nextRow.attr("treelevel");
            return parseInt(treelevel) < parseInt(nextRowTreelevel);
        },
        appendRow: function (rowData, targetRow, nearRow, isBefore) {
            var po = this.po, g = this, p = this.options;
            var targetRowObj = g.getRowObj(targetRow);
            if (!targetRow) {
                g.addRow(rowData);
                return;
            }
            if (nearRow) {
                g.addRow(rowData, nearRow, isBefore ? true : false, targetRowObj);
                return;
            }
            var rowid = $(targetRowObj).attr("rowid");
            var children = $(targetRowObj).nextAll("tr[parentrowid=" + rowid + "]").get();
            if (!children) return;
            if (children.length == 0)
                g.addRow(rowData, targetRowObj, false, targetRowObj);
            else
                g.addRow(rowData, children[children.length - 1], false, targetRowObj);
        },
        upgrade: function (targetRow) {
            var po = this.po, g = this, p = this.options;
            if (!targetRow || !p.tree) return;
            var targetRowData = g.getRow(targetRow);
            var targetRowObj = g.getRowObj(targetRow);
            if (!targetRowData[p.tree.childrenName])
                targetRowData[p.tree.childrenName] = [];
            $("> td > div > .l-grid-tree-space:last", targetRow).addClass("l-grid-tree-link l-grid-tree-link-open");
        },
        demotion: function (targetRow) {
            var po = this.po, g = this, p = this.options;
            if (!targetRow || !p.tree) return;
            var targetRowData = g.getRow(targetRow);
            var targetRowObj = g.getRowObj(targetRow);
            var rowid = $(targetRowObj).attr("rowid");
            $("> td > div > .l-grid-tree-space:last", targetRow).removeClass("l-grid-tree-link l-grid-tree-link-open l-grid-tree-link-close");
            if (g.hasChildren(targetRowObj)) {
                $("tbody:first > tr[parentrowid=" + rowid + "]", g.gridbody).each(function () {
                    g.deleteRow(this);
                });
            }
        },
        collapse: function (targetRow) {
            var po = this.po, g = this, p = this.options;
            var targetRowObj = g.getRowObj(targetRow);
            var linkbtn = $(".l-grid-tree-link", targetRowObj);
            if (linkbtn.hasClass("l-grid-tree-link-close")) return;
            g.toggle(targetRow);
        },
        expand: function (targetRow) {
            var po = this.po, g = this, p = this.options;
            var targetRowObj = g.getRowObj(targetRow);
            var linkbtn = $(".l-grid-tree-link", targetRowObj);
            if (linkbtn.hasClass("l-grid-tree-link-open")) return;
            g.toggle(targetRow);
        },
        toggle: function (targetRow) {
            var po = this.po, g = this, p = this.options;
            var targetRowObj = g.getRowObj(targetRow);
            var treerow = $(targetRowObj);
            var level = treerow.attr("treelevel");
            var linkbtn = $(".l-grid-tree-link", treerow);
            var opening = true;
            if (linkbtn.hasClass("l-grid-tree-link-close")) {
                linkbtn.removeClass("l-grid-tree-link-close").addClass("l-grid-tree-link-open");
            }
            else {
                opening = false;
                linkbtn.addClass("l-grid-tree-link-close").removeClass("l-grid-tree-link-open");
            }
            var currentRow = treerow.next(".l-grid-treerow");
            while (true) {
                if (currentRow.length == 0) break;
                var treelevel = currentRow.attr("treelevel");
                if (treelevel <= level) break;
                if (opening) {
                    $(".l-grid-tree-link", currentRow).removeClass("l-grid-tree-link-close").addClass("l-grid-tree-link-open");
                    currentRow.show();
                }
                else {
                    $(".l-grid-tree-link", currentRow).removeClass("l-grid-tree-link-open").addClass("l-grid-tree-link-close");
                    currentRow.hide();
                }
                currentRow = currentRow.next(".l-grid-treerow");
            }
        },
        reSetColumn: function (newData) {
            var po = this.po, g = this, p = this.options;
            if (newData.lenth == 0) return;

            for (var i = 0; i < newData.length; i++) {
                if (newData[i].isHide != 0) continue;
                g.toggleCol(newData[i].column_id, newData[i].isHide, false);

                if (!newData[i].column_width) continue;
                g.reColumnWidth(newData[i].column_id, newData[i].column_width);


            }
        },
        toggleCol: function (columnparm, visible, toggleByPopup) {
            var po = this.po, g = this, p = this.options;
            var headercell = null;
            var columnindex = -1;
            if (typeof (columnparm) == "number") {
                columnindex = columnparm;
                headercell = $(".l-grid-hd-cell[columnindex='" + columnparm + "']", g.gridheader);
            }
            else if (typeof (columnparm) == "string") {
                headercell = $(".l-grid-hd-cell[columnname='" + columnparm + "']", g.gridheader);
                if (!headercell) return;
                columnindex = headercell.attr("columnindex");
            }
            if (!headercell) return;
            var cellWidth = headercell.width();
            var cellVisible = $(headercell).is(":visible");
            if (visible) {
                if (!cellVisible)
                    g.gridtablewidth += cellWidth + 1;
                headercell.show();
                g.columns[columnindex].hide = false;
                if (g.columnCells[columnindex])
                    $(g.columnCells[columnindex]).show();
                $("td[columnindex=" + columnindex + "]", g.totalRows).show();
            } else {
                if (cellVisible)
                    g.gridtablewidth -= cellWidth + 1;
                headercell.hide();
                g.columns[columnindex].hide = true;
                if (g.columnCells[columnindex])
                    $(g.columnCells[columnindex]).hide();
                $("td[columnindex=" + columnindex + "]", g.totalRows).hide();
            }
            $("div:first", g.gridheader).width(g.gridtablewidth + 40);
            $(".l-grid-body-inner", g.gridbody).width(g.gridtablewidth);
            if (!toggleByPopup) {
                $(':checkbox[columnindex=' + columnindex + "]", g.popup).each(function () {
                    this.checked = visible;
                    if ($.fn.ligerCheckBox) {
                        var checkboxmanager = $(this).ligerGetCheckBoxManager();
                        if (checkboxmanager) checkboxmanager.updateStyle();
                    }
                });
            }
            //alert(columnindex + ":" + visible + ":" + toggleByPopup);
            //p.onHideCol && p.onHideCol(columnindex, visible);
        },
        reColumnWidth: function (columnindex, width) {
            var po = this.po, g = this, p = this.options;
            var headercell = null;

            headercell = $(".l-grid-hd-cell[columnindex='" + columnindex + "']", g.gridheader);

            var column = g.columns[columnindex];
            if (!column) return;
            var headerInfo = (function () {
                for (var i = 0; i < g.headers.length; i++)
                    if (g.headers[i].columnindex == columnindex)
                        return g.headers[i];
                return null;
            })();
            if (!headerInfo) return;
            var mincolumnwidth = 40;
            if (column.minWidth) mincolumnwidth = column.minWidth;
            newwidth = width < mincolumnwidth ? mincolumnwidth : width;
            var diff = newwidth - headerInfo.width;
            headerInfo.width = newwidth;
            g.gridtablewidth += diff;
            $("div:first", g.gridheader).width(g.gridtablewidth + 40);
            $("div:first", g.gridbody).width(g.gridtablewidth); //alert(g.gridtablewidth);
            $('td[columnindex=' + columnindex + ']', g.gridheader).css('width', newwidth);
            if (g.recordNumber > 0) {
                $('td[columnindex=' + columnindex + ']', g.totalRows).add(g.columnCells[columnindex]).css('width', newwidth).find("div:first").css('width', newwidth - 8);
            }
        }
    };


    $.ligerAddGrid = function (grid, p) {
        if (grid.applyligerui) return;
        /*----------------------------------
        -------- liger grid 初始化------------
        ----------------------------------*/
        p.cssClass && $(grid).addClass(p.cssClass);
        $(grid).addClass("l-grid-panel");
        var gridhtmlarr = [];
        gridhtmlarr.push("        <div class='l-grid-panel-header'><span class='l-panel-header-text'></span></div>");
        gridhtmlarr.push("                    <div class='l-grid-loading'></div>");
        gridhtmlarr.push("                    <div class='l-grid-editor'></div>");
        gridhtmlarr.push("        <div class='l-panel-bwarp'>");
        gridhtmlarr.push("            <div class='l-panel-body'>");
        gridhtmlarr.push("                <div class='l-grid'>");
        gridhtmlarr.push("                    <div class='l-grid-dragging-line'></div>");
        gridhtmlarr.push("                    <div class='l-grid-popup'><table cellpadding='0' cellspacing='0'><tbody></tbody></table></div>");
        gridhtmlarr.push("                    <div class='l-grid-header'>");
        gridhtmlarr.push("                        <div class='l-grid-header-inner'><table class='l-grid-header-table' cellpadding='0' cellspacing='0'><tbody><tr></tr></tbody></table></div>");
        gridhtmlarr.push("                    </div>");
        gridhtmlarr.push("                    <div class='l-grid-body l-scroll'>");
        gridhtmlarr.push("                    </div>");
        gridhtmlarr.push("                 </div>");
        gridhtmlarr.push("              </div>");
        gridhtmlarr.push("         </div>");
        gridhtmlarr.push("         <div class='l-panel-bar'>");
        gridhtmlarr.push("            <div class='l-panel-bbar-inner'>");
        gridhtmlarr.push("            <div class='l-bar-group l-bar-selectpagesize'></div>");
        gridhtmlarr.push("                <div class='l-bar-separator'></div>");
        gridhtmlarr.push("                <div class='l-bar-group'>");
        gridhtmlarr.push("                    <div class='l-bar-button l-bar-btnfirst'><span></span></div>");
        gridhtmlarr.push("                    <div class='l-bar-button l-bar-btnprev'><span></span></div>");
        gridhtmlarr.push("                </div>");
        gridhtmlarr.push("                <div class='l-bar-separator'></div>");
        gridhtmlarr.push("                <div class='l-bar-group'><span class='pcontrol'> <input type='text' size='4' value='1' style='width:30px' maxlength='3' /> / <span></span></span></div>");
        gridhtmlarr.push("                <div class='l-bar-separator'></div>");
        gridhtmlarr.push("                <div class='l-bar-group'>");
        gridhtmlarr.push("                     <div class='l-bar-button l-bar-btnnext'><span></span></div>");
        gridhtmlarr.push("                    <div class='l-bar-button l-bar-btnlast'><span></span></div>");
        gridhtmlarr.push("                </div>");
        gridhtmlarr.push("                <div class='l-bar-separator'></div>");
        gridhtmlarr.push("                <div class='l-bar-group'>");
        gridhtmlarr.push("                     <div class='l-bar-button l-bar-btnload'><span></span></div>");
        gridhtmlarr.push("                </div>");
        gridhtmlarr.push("                <div class='l-bar-separator'></div>");
        gridhtmlarr.push("                <div class='l-bar-group l-bar-right'><span class='l-bar-text'></span></div>");
        gridhtmlarr.push("                <div class='l-clear'></div>");
        gridhtmlarr.push("            </div>");
        gridhtmlarr.push("         </div>");
        $(grid).html(gridhtmlarr.join(''));
        var po = {
            init: function () {
                po.clearGrid();
                //创建头部
                po.initBuildHeader();
                //创建表头
                po.initBuildGridHeader();
                //创建 显示/隐藏 列 列表
                po.initBuildPopup();
                //宽度高度初始化
                po.initHeight();
                //创建表体
                p.delayLoad || g.loadData(p.where);
                //创建底部工具条
                po.initFootbar();
                //创建分页
                po.buildPager();
                //创建Loading
                g.gridloading.html(p.loadingMessage);
                //创建事件
                po.initEven();
            },
            initBuildHeader: function () {
                if (p.title)
                    $(".l-panel-header-text", g.header).html(p.title);
                else
                    g.header.hide();
            },
            initBuildGridHeader: function () {
                var maxLevel = g.getMulHeaderLevel();
                for (var level = 1; level <= maxLevel - 1; level++) {
                    var mulHeaders = g.getMulHeaders(level);
                    var tr = $("<tr class='l-grid-hd-mul'></tr>");
                    $("tr:last", g.gridheader).before(tr);
                    //如果有复选框列 
                    if (p.checkbox) {
                        var headerCell = $("<td class='l-grid-hd-cell l-grid-hd-cell-checkbox l-grid-hd-cell-mul'></td>");
                        tr.append(headerCell);
                    }
                    //如果有明细，创建列
                    if (g.enabledDetail()) {
                        var detailHeaderCell = $("<td class='l-grid-hd-cell l-grid-hd-cell-detail l-grid-hd-cell-mul'></td>");
                        tr.append(detailHeaderCell);
                    }
                    $(mulHeaders).each(function (i, item) {
                        var $headerCell = $("<td class='l-grid-hd-cell l-grid-hd-cell-mul'><div class='l-grid-hd-cell-inner'><span class='l-grid-hd-cell-text'> </span></div></td>");
                        $headerCell.attr("colSpan", item.number);
                        $(".l-grid-hd-cell-text", $headerCell).html(item.display);
                        tr.append($headerCell);
                    });
                }
                g.columns = g.getColumns(maxLevel);
                if (maxLevel > 1)
                    g.gridheader.height(g.gridheader.height() * maxLevel);

                g.headers = [];
                g.gridtablewidth = 0;
                //如果有复选框列 
                if (p.checkbox) {
                    var headerCell = $("<td class='l-grid-hd-cell l-grid-hd-cell-checkbox'><div class='l-grid-hd-cell-inner'><div class='l-grid-hd-cell-text l-grid-hd-cell-btn-checkbox'></div></div></td>");
                    headerCell.css({ width: p.checkboxColWidth });
                    $("tr:last", g.gridheader).append(headerCell);
                    g.headers.push({
                        width: p.checkboxColWidth,
                        ischeckbox: true
                    });
                    g.gridtablewidth += p.checkboxColWidth + 1;
                }
                //如果有明细，创建列
                if (g.enabledDetail()) {
                    var detailHeaderCell = $("<td class='l-grid-hd-cell l-grid-hd-cell-detail'><div class='l-grid-hd-cell-inner'><div class='l-grid-hd-cell-text'></div></div></td>");
                    detailHeaderCell.css({ width: 29 });
                    $("tr:last", g.gridheader).append(detailHeaderCell);
                    g.headers.push({
                        width: p.detailColWidth,
                        isdetail: true
                    });
                    g.gridtablewidth += p.detailColWidth + 1;
                }
                $(g.columns).each(function (i, item) {
                    var $headerCell = $("<td class='l-grid-hd-cell' columnindex='" + i + "'><div class='l-grid-hd-cell-inner'><span class='l-grid-hd-cell-text'> </span></div></td>");
                    if (i == g.columns.length - 1) {
                        //$(".l-grid-hd-cell-drophandle", $headerCell).remove();
                        $headerCell.addClass("l-grid-hd-cell-last");
                    }
                    if (item.hide)
                        $headerCell.hide();
                    if (item.name)
                        $headerCell.attr({ columnname: item.name });
                    if (item.isSort != undefined)
                        $headerCell.attr({ isSort: item.isSort });
                    if (item.isAllowHide != undefined)
                        $headerCell.attr({ isAllowHide: item.isAllowHide });
                    var headerText = "";
                    if (item.display && item.display != "")
                        headerText = item.display;
                    else if (item.headerRender)
                        headerText = item.headerRender(item);
                    else
                        headerText = "&nbsp;";
                    $(".l-grid-hd-cell-text", $headerCell).html(headerText);
                    //$headerCell.prepend(headerText);
                    $("tr:last", g.gridheader).append($headerCell);
                    var colwidth = item.width;
                    if (item.width) {
                        colwidth = item.width;
                    }
                    else if (item.minWidth) {
                        colwidth = item.minWidth;
                    }
                    else if (p.columnWidth) {
                        colwidth = p.columnWidth;
                    }
                    if (!colwidth) {
                        var lwidth = 4;
                        if (p.checkbox) lwidth += p.checkboxColWidth;
                        if (g.enabledDetail()) lwidth += p.detailColWidth;
                        colwidth = parseInt((g.gridbody.width() - lwidth) / g.columns.length);
                    }
                    if (typeof (colwidth) == "string" && colwidth.indexOf('%') > 0) {
                        item.width = colwidth = parseInt(parseInt(colwidth) * 0.01 * (g.gridbody.width() - g.columns.length));
                    }
                    $headerCell.width(colwidth);
                    g.gridtablewidth += (parseInt(colwidth) ? parseInt(colwidth) : 0) + 1;
                    g.headers.push({
                        width: colwidth,
                        columnname: item.name,
                        columnindex: i,
                        islast: i == g.columns.length - 1,
                        isdetail: false
                    });
                });
                $("div:first", g.gridheader).width(g.gridtablewidth + 40);
            },
            initBuildPopup: function () {
                $("tr:last .l-grid-hd-cell", g.gridheader).each(function (i, td) {
                    if ($(this).hasClass("l-grid-hd-cell-detail")) return;
                    var isAllowHide = $(this).attr("isAllowHide");
                    if (isAllowHide != undefined && isAllowHide.toLowerCase() == "false") return;
                    var chk = 'checked="checked"';
                    var columnindex = $(this).attr("columnindex");
                    var columnname = $(this).attr("columnname");
                    if (!columnindex || !columnname) return;
                    var header = $(".l-grid-hd-cell-text", this).html();
                    if (this.style.display == 'none') chk = '';
                    $('tbody', g.popup).append('<tr><td class="l-column-left"><input type="checkbox" ' + chk + ' class="l-checkbox" columnindex="' + columnindex + '"/></td><td class="l-column-right">' + header + '</td></tr>');
                });
                $.fn.ligerCheckBox && $('input:checkbox', g.popup).ligerCheckBox(
                {
                    onBeforeClick: function (obj) {
                        if (!obj.checked) return true;
                        if ($('input:checked', g.popup).length <= p.minColToggle)
                            return false;
                        return true;
                    }
                });
                //创建 显示/隐藏 列 
                $(".l-grid-hd-cell", g.gridheader).bind("contextmenu", function (e) {
                    if (g.colresize) return true;
                    if (!p.allowHideColumn) return true;
                    var columnindex = $(this).attr("columnindex");
                    if (columnindex == undefined) return true;
                    var left = (e.pageX - g.body.offset().left + parseInt(g.body[0].scrollLeft));
                    if (columnindex == g.columns.length - 1) left -= 50;
                    g.popup.css({ left: left, top: g.gridheader.height() + 1 });
                    g.popup.toggle();
                    return false;
                }
                );
            },
            initHeight: function () {
                if (p.isScroll == false) p.height = 'auto';
                if (p.height == 'auto') {
                    g.gridbody.height('auto');
                }
                if (p.width) {
                    $(grid).width(p.width);
                }
                po.onResize();
            },
            initFootbar: function () {
                if (p.usePager) {
                    //创建底部工具条 - 选择每页显示记录数
                    var optStr = "";
                    var selectedIndex = -1;
                    $(p.pageSizeOptions).each(function (i, item) {
                        var selectedStr = "";
                        if (p.pageSize == item) selectedIndex = i;
                        optStr += "<option value='" + item + "' " + selectedStr + " >" + item + "</option>";
                    });

                    $('.l-bar-selectpagesize', g.toolbar).append("<select name='rp'>" + optStr + "</select>");
                    if (selectedIndex != -1) $('.l-bar-selectpagesize select', g.toolbar)[0].selectedIndex = selectedIndex;
                    if (p.switchPageSizeApplyComboBox && $.fn.ligerComboBox) {
                        $(".l-bar-selectpagesize select", g.toolbar).ligerComboBox(
                        {
                            onBeforeSelect: function () {
                                if (g.isDataChanged && !confirm(p.isContinueByDataChanged))
                                    return false;
                                return true;
                            },
                            width: 45
                        });
                    }
                }
                else {
                    g.toolbar.hide();
                }
            },
            initEven: function () {
                g.header.click(function () {
                    g.popup.hide();
                    g.endEdit();
                });
                $(".l-grid-hd-cell-text", g.gridheader).click(function (e) {
                    var obj = (e.target || e.srcElement);
                    var row = $(this).parent().parent();
                    if (!row.attr("columnname")) return;
                    if (g.colresize) return false; //如果正在调整列宽
                    if (!p.enabledSort) return;
                    if (row.attr("isSort") != undefined && row.attr("isSort").toLowerCase() == "false") return;
                    if (g.isDataChanged && !confirm(p.isContinueByDataChanged))
                        return false;
                    var sort = $(".l-grid-hd-cell-sort", row);
                    var columnName = $(row).attr("columnname");
                    if (sort.length > 0) {
                        if (sort.hasClass("l-grid-hd-cell-sort-asc")) {
                            sort.removeClass("l-grid-hd-cell-sort-asc").addClass("l-grid-hd-cell-sort-desc");
                            row.removeClass("l-grid-hd-cell-asc").addClass("l-grid-hd-cell-desc");
                            g.changeSort(columnName, 'desc');
                        }
                        else if (sort.hasClass("l-grid-hd-cell-sort-desc")) {
                            sort.removeClass("l-grid-hd-cell-sort-desc").addClass("l-grid-hd-cell-sort-asc");
                            row.removeClass("l-grid-hd-cell-desc").addClass("l-grid-hd-cell-asc");
                            g.changeSort(columnName, 'asc');
                        }
                    }
                    else {
                        row.removeClass("l-grid-hd-cell-desc").addClass("l-grid-hd-cell-asc");
                        $(this).after("<span class='l-grid-hd-cell-sort l-grid-hd-cell-sort-asc'>&nbsp;&nbsp;</span>");
                        g.changeSort(columnName, 'asc');
                    }
                    $(".l-grid-hd-cell-sort", row.siblings()).remove();
                    return false;
                });
                g.gridheader.click(function () {
                    g.popup.hide();
                    g.endEdit();
                });
                //调整列宽
                if (p.allowAdjustColWidth) {
                    g.gridheader.mousemove(function (e) {
                        if (g.colresize) return; //如果正在调整列宽
                        var posLeft = e.pageX - $(grid).offset().left; //当前鼠标位置
                        var currentLeft = 0;
                        for (var i = 0; i < g.headers.length; i++) {
                            var hide = false;
                            if (g.headers[i].columnindex != undefined) {
                                hide = g.columns[g.headers[i].columnindex].hide ? true : false;

                            }
                            if (!hide && g.headers[i].width) currentLeft += g.headers[i].width + 1;
                            if (g.headers[i].isdetail || g.headers[i].ischeckbox || hide) continue;

                            if (posLeft >= currentLeft - 2 - g.gridbody[0].scrollLeft && posLeft <= currentLeft + 2 - g.gridbody[0].scrollLeft) {
                                $('body').css({ cursor: 'e-resize' });
                                g.toDragHeaderIndex = i;
                                return;
                            }
                        }
                        $('body').css({ cursor: 'default' });
                        g.toDragHeaderIndex = null;
                    }).mouseout(function (e) {
                        if (g.colresize) return; //如果正在调整列宽
                        $('body').css({ cursor: 'default' });
                    }).mousedown(function (e) {
                        if (e.button == 2) return;
                        if (g.colresize) return; //如果正在调整列宽
                        if (g.toDragHeaderIndex == null) return; //如果不在位置上
                        po.dragStart('colresize', e, g.toDragHeaderIndex);
                    });
                }

                //表头 - 显示/隐藏'列控制'按钮事件
                if (p.allowHideColumn) {

                    $('tr', g.popup).hover(function () { $(this).addClass('l-popup-row-over'); },
                    function () { $(this).removeClass('l-popup-row-over'); });
                    var onPopupCheckboxChange = function () {
                        if ($('input:checked', g.popup).length + 1 <= p.minColToggle) {
                            return false;
                        }
                        var checked = this.checked ? 1 : 0;
                        var columnindex = parseInt($(this).attr("columnindex"));
                        g.toggleCol(columnindex, checked, true);
                        p.onHideCol && p.onHideCol(columnindex, checked);
                    };
                    if ($.fn.ligerCheckBox)
                        $(':checkbox', g.popup).change(onPopupCheckboxChange);
                    else
                        $(':checkbox', g.popup).click(onPopupCheckboxChange);
                }
                //表头 - 调整列宽层事件
                //表体 - 滚动联动事件
                g.gridbody.scroll(function () {

                    var scrollLeft = g.gridbody.scrollLeft();
                    if (scrollLeft == undefined) return;
                    g.gridheader[0].scrollLeft = scrollLeft;
                });
                //表体 - 数据 单元格事件
                $(grid).click(function (e) {
                    var obj = (e.target || e.srcElement);
                    //明细 - 事件
                    if (obj.tagName.toLowerCase() == "span" && $(obj).hasClass("l-grid-row-cell-detailbtn")) {
                        var row = $(obj).parent().parent().parent();
                        //确保不是在内嵌表格点击的 
                        if (row.parent().parent()[0] != $("table:first", g.gridbody)[0]) return;
                        var rowindex = parseInt($(row).attr("rowindex"));
                        var rowid = $(row).attr("rowid");
                        var item = g.getRow(rowid);
                        if ($(obj).hasClass("l-open")) {
                            row.next(".l-grid-detailpanel").hide();
                            $(obj).removeClass("l-open");
                        }
                        else {
                            var nextrow = row.next(".l-grid-detailpanel");
                            if (nextrow.length > 0) {
                                nextrow.show();
                                $(obj).addClass("l-open");
                                return;
                            }
                            var detailRow = $("<tr class='l-grid-detailpanel'><td><div class='l-grid-detailpanel-inner' style='display:none'></div></td></tr>");
                            var detailRowInner = $("div:first", detailRow);
                            detailRowInner.parent().attr("colSpan", g.headers.length);
                            row.after(detailRow);
                            if (p.detail.onShowDetail) {
                                p.detail.onShowDetail(item, detailRowInner[0]);
                                detailRowInner.show();
                            }
                            else if (p.detail.render) {
                                detailRowInner.append(p.detail.render());
                                detailRowInner.show();
                            }
                            $(obj).addClass("l-open");
                        }
                        return;
                    }
                    //树 - 伸展/收缩节点
                    if ($(obj).hasClass("l-grid-tree-link")) {
                        var rowObj = $(obj).parent().parent().parent().get(0);
                        g.toggle(rowObj);
                        return;
                    }
                    //全选
                    if (obj.tagName.toLowerCase() == "div" && $(obj).hasClass("l-grid-hd-cell-btn-checkbox")) {
                        var row = $(obj).parent().parent().parent();
                        var uncheck = row.hasClass("l-checked");
                        if (p.onBeforeCheckAllRow) {
                            if (p.onBeforeCheckAllRow(!uncheck, grid) == false) return false;
                        }
                        if (uncheck) {
                            row.removeClass("l-checked");
                            $("tbody:first > tr.l-grid-row", g.gridbody).removeClass("l-checked");
                        }
                        else {
                            row.addClass("l-checked");
                            $("tbody:first > tr.l-grid-row", g.gridbody).addClass("l-checked");
                        }
                        p.onCheckAllRow && p.onCheckAllRow(!uncheck, grid);
                    }
                    if (obj.tagName.toLowerCase() == "div" || $(obj).hasClass("l-grid-row-cell-inner") || $(obj).hasClass("l-grid-row-cell")) {
                        if (p.enabledEdit && !p.dblClickToEdit) {
                            var row = null;
                            if ($(obj).hasClass("l-grid-row-cell")) row = $(obj).parent();
                            else row = $(obj).parent().parent();
                            //第一次选择的时候不允许编辑，第二次才允许
                            if (p.allowUnSelectRow || row.hasClass("l-selected-again"))
                                po.applyEditor(obj);
                        }
                    }
                });
                //工具条 - 切换每页记录数事件
                $('select', g.toolbar).change(function () {
                    if (g.isDataChanged && !confirm(p.isContinueByDataChanged))
                        return false;
                    p.newPage = 1;
                    p.pageSize = this.value;
                    g.loadData(p.where);
                });
                //工具条 - 切换当前页事件
                $('.pcontrol input', g.toolbar).keydown(function (e) { if (e.keyCode == 13) g.changePage('input') });
                //工具条 - 按钮事件
                $(".l-bar-button", g.toolbar).hover(function () {
                    $(this).addClass("l-bar-button-over");
                }, function () {
                    $(this).removeClass("l-bar-button-over");
                }).click(function () {
                    if ($(this).hasClass("l-bar-btnfirst")) {
                        if (p.onToFirst && p.onToFirst(grid) == false) return false;
                        g.changePage('first');
                    }
                    else if ($(this).hasClass("l-bar-btnprev")) {
                        if (p.onToPrev && p.onToPrev(grid) == false) return false;
                        g.changePage('prev');
                    }
                    else if ($(this).hasClass("l-bar-btnnext")) {
                        if (p.onToNext && p.onToNext(grid) == false) return false;
                        g.changePage('next');
                    }
                    else if ($(this).hasClass("l-bar-btnlast")) {
                        if (p.onToLast && p.onToLast(grid) == false) return false;
                        g.changePage('last');
                    }
                    else if ($(this).hasClass("l-bar-btnload")) {
                        if ($("span", this).hasClass("l-disabled")) return false;
                        if (p.onReload && p.onReload(grid) == false) return false;
                        if (g.isDataChanged && !confirm(p.isContinueByDataChanged))
                            return false;
                        g.loadData(p.where);
                    }
                });
                g.toolbar.click(function () {
                    g.popup.hide();
                    g.endEdit();
                });

                //全局事件
                $(document)
                .mousemove(function (e) { po.dragMove(e) })
                .mouseup(function (e) { po.dragEnd() })
                .click(function (e) { po.onClick(e) });

                $(window).resize(po.onResize);
            },
            searchData: function (data, clause) {
                var newData = new Array();
                for (var i = 0; i < data.length; i++) {
                    if (clause(data[i], i)) {
                        newData[newData.length] = data[i];
                    }
                }
                return newData;
            },
            recordRow: function (rowDom) {
                if (!$(rowDom).hasClass("l-grid-row")) {
                    if ($(rowDom).hasClass("l-grid-totalsummary")) {
                        g.totalRows.push(rowDom);
                    }
                    else if ($(rowDom).hasClass("l-grid-grouprow")) {
                        g.groupRows.push(rowDom);
                    }
                    return;
                }
                var rowid = $(rowDom).attr("rowid");
                g.rows[rowid] = rowDom;
                g.cells[rowid] = {};
                $(" > td", rowDom).each(function () {
                    var columnindex = $(this).attr("columnindex");
                    if (columnindex) {
                        g.columnCells[columnindex] = g.columnCells[columnindex] || [];
                        g.columnCells[columnindex].push(this);
                        g.cells[rowid][columnindex] = this;
                    }
                });
            },
            clearGrid: function () {
                //清空数据
                g.gridbody.html("");
                g.recordNumber = 0;
                g.records = {};
                g.rows = {};
                g.cells = {};
                g.columnCells = {};
                g.totalRows = [];
                g.groupRows = [];
            },
            showData: function (data) {
                if (p.usePager) {
                    if (data) {
                        //更新分页
                        if (data[p.record])
                            p.total = data[p.record];
                        else
                            p.total = data.length;
                    }
                    p.page = p.newPage;
                    if (!p.total) p.total = 0;
                    if (!p.page) p.page = 1;
                    p.pageCount = Math.ceil(p.total / p.pageSize);
                    if (!p.pageCount) p.pageCount = 1;
                    po.buildPager();
                }
                //加载中
                $('.l-bar-btnloading:first', g.toolbar).removeClass('l-bar-btnloading');
                if (!data || !data[p.root]) {
                    g.onResize();
                    return;
                }
                if (p.onBeforeShowData && p.onBeforeShowData(grid, data) == false) {
                    return false;
                }
                g.isDataChanged = false;
                $(".l-bar-btnload:first span", g.toolbar).removeClass("l-disabled");
                po.clearGrid();
                //$(".l-grid-row,.l-grid-detailpanel,.l-grid-totalsummary", g.gridbody).remove();
                //加载数据 
                var gridhtmlarr = ['<div class="l-grid-body-inner"><table class="l-grid-body-table" cellpadding=0 cellspacing=0><tbody>'];
                if (p.groupColumnName) //启用分组模式
                {
                    var groups = []; //分组列名数组
                    var groupsdata = []; //切成几块后的数据
                    $(data[p.root]).each(function (i, item) {
                        var groupColumnValue = item[p.groupColumnName];
                        var valueIndex = $.inArray(groupColumnValue, groups);
                        if (valueIndex == -1) {
                            groups.push(groupColumnValue);
                            valueIndex = groups.length - 1;
                            groupsdata.push([]);
                        }
                        groupsdata[valueIndex].push(item);
                    });
                    $(groupsdata).each(function (i, item) {
                        if (groupsdata.length == 1)
                            gridhtmlarr.push('<tr class="l-grid-grouprow l-grid-grouprow-last l-grid-grouprow-first"');
                        if (i == groupsdata.length - 1)
                            gridhtmlarr.push('<tr class="l-grid-grouprow l-grid-grouprow-last"');
                        else if (i == 0)
                            gridhtmlarr.push('<tr class="l-grid-grouprow l-grid-grouprow-first"');
                        else
                            gridhtmlarr.push('<tr class="l-grid-grouprow"');
                        gridhtmlarr.push(' groupindex"=' + i + '" >');
                        gridhtmlarr.push('<td colSpan="' + g.headers.length + '" class="l-grid-grouprow-cell">');

                        //modify
                        gridhtmlarr.push('<span class="l-grid-group-togglebtn ');
                        if (p.defaultCloseGroup)
                            gridhtmlarr.push(' l-grid-group-togglebtn-close');
                        gridhtmlarr.push('      ">&nbsp;&nbsp;&nbsp;&nbsp;</span>');


                        if (p.groupRender) {
                            gridhtmlarr.push(p.groupRender(groups[i], p.groupColumnDisplay, groupsdata[i]));
                        }
                        else {
                            gridhtmlarr.push(p.groupColumnDisplay + ':' + groups[i]);
                        }


                        gridhtmlarr.push('</td>');
                        gridhtmlarr.push('</tr>');

                        gridhtmlarr.push(po.getHtmlFromData(item));
                        //汇总
                        if (g.isTotalSummary())
                            gridhtmlarr.push(po.getTotalSummaryHtml(item, "l-grid-totalsummary-group"));
                    });
                }
                else if (p.tree)//启用分页模式
                {
                    if (!p.tree.columnName) p.tree.columnName = "name";
                    if (!p.tree.childrenName) p.tree.childrenName = "children";
                    if (!p.tree.isParent) p.tree.isParent = function (rowData) {
                        var exist = 'children' in rowData;
                        return exist;
                    };
                    if (!p.tree.isExtend) p.tree.isExtend = function (rowData) {
                        if ('isextend' in rowData && rowData['isextend'] == false)
                            return false;
                        return true;
                    };
                    gridhtmlarr.push(po.getHtmlFromData(data[p.root], p.tree, 1));
                    //alert(JSON.stringify( p.tree));
                }
                else {
                    gridhtmlarr.push(po.getHtmlFromData(data[p.root]));
                }
                gridhtmlarr.push('</tbody></table></div>');
                var jgridbody = $(gridhtmlarr.join(''));
                $("tbody:first > tr", jgridbody).each(function () {
                    po.recordRow(this);
                });
                g.gridbody.append(jgridbody);

                g.currentData = data;
                //分组时不需要
                if (!p.groupColumnName) {
                    //创建汇总行
                    po.bulidTotalSummary();
                }
                $("> div:first", g.gridbody).width(g.gridtablewidth);

                po.onResize();
                //分组 - 事件
                $("tbody:first > .l-grid-grouprow", g.gridbody).each(function () {
                    var grouprow = $(this);
                    $(".l-grid-group-togglebtn", grouprow).click(function () {
                        var opening = true;
                        if ($(this).hasClass("l-grid-group-togglebtn-close")) {
                            $(this).removeClass("l-grid-group-togglebtn-close");

                            if (grouprow.hasClass("l-grid-grouprow-last")) {
                                $("td:first", grouprow).width('auto');
                            }
                        }
                        else {
                            opening = false;
                            $(this).addClass("l-grid-group-togglebtn-close");
                            if (grouprow.hasClass("l-grid-grouprow-last")) {
                                $("td:first", grouprow).width(g.gridtablewidth);
                            }
                        }
                        var currentRow = grouprow.next(".l-grid-row,.l-grid-totalsummary-group,.l-grid-detailpanel");
                        while (true) {
                            if (currentRow.length == 0) break;
                            if (opening) {
                                currentRow.show();
                                //如果是明细展开的行，并且之前的状态已经是关闭的，隐藏之
                                if (currentRow.hasClass("l-grid-detailpanel") && !currentRow.prev().find("td.l-grid-row-cell-detail:first span.l-grid-row-cell-detailbtn:first").hasClass("l-open")) {
                                    currentRow.hide();
                                }
                            }
                            else {
                                currentRow.hide();
                            }
                            currentRow = currentRow.next(".l-grid-row,.l-grid-totalsummary-group,.l-grid-detailpanel");
                        }
                    });
                });
                //表体 - 行经过事件
                $("tbody:first > .l-grid-row", g.gridbody).each(function () { po.setRowEven(this); });
                if (p.totalRender) {
                    $(".l-panel-bar-total", grid).remove();
                    $(".l-panel-bar", grid).before('<div class="l-panel-bar-total">' + p.totalRender(g.data, g.filteredData) + '</div>');
                }
                if (p.onAfterShowData) {
                    p.onAfterShowData(grid, data, g);
                }
            },
            onClick: function (e) {
                var obj = (e.target || e.srcElement);
                var tagName = obj.tagName.toLowerCase();
                if (g.grideditor.editingCell) {
                    if (tagName == 'html' || tagName == 'body' || $(obj).hasClass("l-grid-body") || $(obj).hasClass("l-grid-row")) {
                        g.endEdit(true);
                    }
                }
                if (p.allowHideColumn) {
                    if (tagName == 'html' || tagName == 'body' || $(obj).hasClass("l-grid-body") || $(obj).hasClass("l-grid-row") || $(obj).hasClass("l-grid-row-cell-inner") || $(obj).hasClass("l-grid-header") || $(obj).hasClass("l-grid-grouprow-cell") || $(obj).hasClass("l-grid-totalsummary-cell") || $(obj).hasClass("l-grid-totalsummary-cell-inner")) {
                        g.popup.hide();
                    }
                }
            },
            getHtmlFromData: function (dataArray, tree, level, parentrowid) {
                if (!dataArray || !dataArray.length) return "";
                var gridhtmlarr = [];
                var rowlenth = dataArray.length;
                $(dataArray).each(function (i, item) {
                    if (!item) return;
                    if (!p.usePager && i == rowlenth - 1 && !g.isTotalSummary())
                        gridhtmlarr.push('<tr class="l-grid-row l-grid-row-last');
                    else
                        gridhtmlarr.push('<tr class="l-grid-row');

                    if (tree) {
                        gridhtmlarr.push(' l-grid-treerow');
                    }
                    if (p.checkbox && p.isChecked && p.isChecked(item)) {
                        gridhtmlarr.push(' l-checked');
                    }
                    if (i % 2 == 1 && p.alternatingRow)
                        gridhtmlarr.push(' l-grid-row-alt');
                    gridhtmlarr.push('" ');

                    if (p.defaultCloseGroup) gridhtmlarr.push(' style="display: none;"');//modify

                    if (tree) gridhtmlarr.push(" treelevel= " + level);
                    if (p.rowAttrRender) gridhtmlarr.push(p.rowAttrRender(item, i));
                    var rowid = g.recordNumber;
                    if (p.rowid) {
                        growid = item[p.rowid];
                        gridhtmlarr.push(" growid= " + growid);
                    }
                    if (p.rowtype) {
                        gridhtmlarr.push(" rowtype='" + item[p.rowtype] + "'");
                    }

                    gridhtmlarr.push(" rowid= " + rowid);
                    g.records[g.recordNumber] = item;
                    g.recordNumber++;
                    if (parentrowid != undefined)
                        gridhtmlarr.push(" parentrowid= " + parentrowid);
                    gridhtmlarr.push(' rowindex="' + i + '">');
                    $(g.headers).each(function (headerCellIndex, headerInfor) {
                        //如果是复选框(系统列)
                        if (this.ischeckbox) {
                            gridhtmlarr.push('<td class="l-grid-row-cell l-grid-row-cell-checkbox" style="width:' + this.width + 'px"><div class="l-grid-hd-cell-inner"><span class="l-grid-row-cell-btn-checkbox"></span></div></td>');
                            return;
                        }
                            //如果是明细列(系统列)
                        else if (this.isdetail) {
                            gridhtmlarr.push('<td class="l-grid-row-cell l-grid-row-cell-detail" style="width:' + this.width + 'px"><div class="l-grid-row-cell-inner"><span class="l-grid-row-cell-detailbtn"></span></div></td>');
                            return;
                        }
                        var column = g.columns[this.columnindex];
                        var colwidth = this.width;
                        if (!this.islast)
                            gridhtmlarr.push('<td class="l-grid-row-cell" columnindex="' + this.columnindex + '" ');
                        else
                            gridhtmlarr.push('<td class="l-grid-row-cell l-grid-row-cell-last" columnindex="' + this.columnindex + '" ');
                        if (this.columnname) gridhtmlarr.push('columnname="' + this.columnname + '"');
                        gridhtmlarr.push(' style = "');
                        if (typeof (colwidth) == "string" && colwidth.indexOf('%') > 0) {
                            gridhtmlarr.push('width:' + colwidth + '; ');
                        }
                        else {
                            gridhtmlarr.push('width:' + colwidth + 'px; ');
                        }
                        if (column && column.hide) {
                            gridhtmlarr.push('display:none;');
                        }
                        if (p.fixedCellHeight)
                            gridhtmlarr.push('"><div class="l-grid-row-cell-inner l-grid-row-cell-inner-fixedheight" ');
                        else
                            gridhtmlarr.push('><div class="l-grid-row-cell-inner" ');
                        if (typeof (colwidth) == "string" && colwidth.indexOf('%') > 0) {
                            gridhtmlarr.push(' style = "width:95%; ');
                        }
                        else {
                            gridhtmlarr.push(' style = "width:' + parseInt(colwidth - 8) + 'px; ');
                        }
                        if (column && column.align) gridhtmlarr.push('text-align:' + column.align + ';');
                        if (column && column.type == "date") {
                            var date = p.renderDate(item[this.columnname]);
                            item[this.columnname] = date;
                        }
                        var content = po.getCellContent(item, i, item[this.columnname], column, tree, level, rowid);

                        gridhtmlarr.push('">' + content + '</div></td>');
                    });
                    gridhtmlarr.push('</tr>');
                    if (tree && tree.isParent(item)) {
                        var childrenData = item[tree.childrenName];
                        if (childrenData)
                            gridhtmlarr.push(po.getHtmlFromData(childrenData, tree, level + 1, rowid));
                    }
                });
                return gridhtmlarr.join('');
            },
            getTreeCellHtml: function (tree, oldContent, rowData, level) {
                var isExtend = tree.isExtend(rowData);
                var isParent = tree.isParent(rowData);
                var content = "";
                for (var i = 1; i < level; i++) {
                    content += "<div class='l-grid-tree-space'></div>";
                }
                if (isExtend && isParent)
                    content += "<div class='l-grid-tree-icon l-grid-tree-link l-grid-tree-link-open'></div>";
                else if (isParent)
                    content += "<div class='l-grid-tree-icon l-grid-tree-link l-grid-tree-link-close'></div>";
                else
                    content += "<div class='l-grid-tree-leaf'></div>";
                content += "<span class='l-grid-tree-content'>" + oldContent + "</span>";
                return content;
            },
            getCellContent: function (rowData, rowindex, value, column, tree, level, rowid) {
                var content = "";
                if (!rowData || !column) return "";
                if (column.render) {
                    if (!p.page) p.page = 1;
                    //if (!p.pagesize) p.page = 1;
                    var page = p.page;
                    var pagesize = p.pageSize;
                    content = column.render(rowData, rowindex, value, column, rowid, page, pagesize);
                }
                else if (column.type == 'date') {
                    if (value != null) content = value.toString();
                    if (value instanceof Date) {
                        if (column.format) content = g.getFormatDate(value, column.format);
                        else content = g.getFormatDate(value, p.dateFormat);
                    }
                }
                else {
                    if (value != null) content = value.toString();
                }
                if (tree && tree.columnName == column.name) {
                    content = po.getTreeCellHtml(tree, content, rowData, level);
                }
                if (content == null || content == undefined) content = "";
                return content;
            },
            setRowEven: function (rowobj) {
                if (p.onRClickToSelect || p.onContextmenu) {
                    $(rowobj).bind("contextmenu", function (e) {
                        var obj = (e.target || e.srcElement);
                        if (p.onRClickToSelect)
                            $(this).addClass("l-selected").siblings(".l-selected").removeClass("l-selected");
                        if (p.onContextmenu) {
                            var rowid = $(this).attr("rowid");
                            var rowindex = $(this).attr("rowindex");
                            var rowdata = g.getRow(rowid);
                            return p.onContextmenu({ data: rowdata, rowindex: rowindex, row: this }, e);
                        }
                        return true;
                    });
                }
                $(rowobj).hover(function (e) {
                    if (p.mouseoverRowCssClass)
                        $(this).addClass(p.mouseoverRowCssClass);

                }, function (e) {
                    if (p.mouseoverRowCssClass)
                        $(this).removeClass(p.mouseoverRowCssClass);
                }).click(function (e) {
                    if (p.checkbox) {
                        var srcObj = (e.target || e.srcElement);
                        var selectRowButtonOnly = p.selectRowButtonOnly ? true : false;
                        if (p.enabledEdit) selectRowButtonOnly = true;
                        if (!selectRowButtonOnly || $(srcObj).hasClass("l-grid-row-cell-btn-checkbox")) {
                            var row = $(this);
                            var index = row.attr('rowindex');
                            var rowid = row.attr("rowid");
                            var uncheck = row.hasClass("l-checked");
                            if (p.onBeforeCheckRow) {
                                if (p.onBeforeCheckRow(!uncheck, g.getRow(rowid), index, row[0]) == false) return false;
                            }
                            if (uncheck)
                                row.removeClass("l-checked");
                            else
                                row.addClass("l-checked");
                            p.onCheckRow && p.onCheckRow(!uncheck, g.getRow(rowid), index, row[0]);
                        }
                        if (!p.enabledEdit)
                            return;
                    }
                    var index = $(this).attr('rowindex');
                    var rowid = $(this).attr("rowid");
                    if ($(this).hasClass("l-selected")) {
                        if (!p.allowUnSelectRow) {
                            $(this).addClass("l-selected-again");
                            return;
                        }
                        $(this).removeClass("l-selected l-selected-again");
                        if (p.onUnSelectRow) {
                            p.onUnSelectRow(g.getRow(rowid), index, this);
                        }
                    }
                    else {
                        $(this).siblings(".l-selected").each(function () {
                            if (p.allowUnSelectRow || $(this).hasClass("l-selected-again"))
                                g.endEdit();
                            $(this).removeClass("l-selected l-selected-again");
                        });
                        $(this).addClass("l-selected");
                        if (p.onSelectRow) {
                            p.onSelectRow(g.getRow(rowid), index, this);
                        }
                    }

                }).dblclick(function () {
                    var index = $(this).attr('rowindex');
                    var rowid = $(this).attr('rowid');
                    if (p.onDblClickRow) {
                        p.onDblClickRow(g.getRow(rowid), index, this);
                    }
                });
            },
            applyEditor: function (obj) {
                if (obj.href || obj.type) return true;
                var rowcell;
                if ($(obj).hasClass("l-grid-row-cell")) rowcell = obj;
                else if ($(obj).parent().hasClass("l-grid-row-cell")) rowcell = $(obj).parent()[0];
                if (!rowcell) return;
                var row = $(rowcell).parent();
                var rowindex = row.attr("rowindex");
                var rowid = row.attr("rowid");
                var columnindex = $(rowcell).attr("columnindex");
                var columnname = $(rowcell).attr("columnname");
                var column = g.columns[columnindex];
                if (!column || !column.editor) return;
                var left = $(rowcell).offset().left - g.body.offset().left;
                var top = $(rowcell).offset().top - $(grid).offset().top;
                var rowdata = g.getRow(rowid);
                var currentdata = rowdata[columnname];
                var editParm = {
                    record: rowdata,
                    value: currentdata,
                    column: column,
                    columnname: columnname,
                    columnindex: columnindex,
                    rowindex: rowindex,
                    rowObj: row[0],
                    cellObj: rowcell
                };
                if (p.onBeforeEdit) {
                    if (!p.onBeforeEdit(editParm))
                        return false;
                }
                g.grideditor.css({ left: left - 1, top: top - 2, width: $(rowcell).css('width'), height: $(rowcell).css('height') }).html("");
                g.grideditor.editingCell = rowcell;
                if (column.editor.type == 'date') {
                    if ("" == currentdata || null == currentdata || isNaN(currentdata)) {
                        currentdata = new Date();
                    } else {
                        //注：该字段的type不能为date了，数据形式如："IncomeDay": '2009-08-09'
                        currentdata = g.stringToDate(currentdata);
                    }
                    var $inputText = $("<input type='text'/>");
                    g.grideditor.append($inputText);
                    $inputText.val(g.getFormatDate(currentdata, p.dateFormat));
                    var options = {
                        width: $(rowcell).width(),
                        onChangeDate: function (newValue) {
                            $(rowcell).addClass("l-grid-row-cell-edited");
                            if (column.editor.onChange) column.editor.onChange(rowcell, newValue);
                            editParm.value = newValue;
                            if (po.checkEditAndUpdateCell(editParm)) {
                                if (column.editor.onChanged) column.editor.onChanged(rowcell, newValue);
                            }
                        }
                    };
                    if (column.editor.p)
                        options = $.expend({}, typeof (column.editor.p) == 'function' ? column.editor.p(rowdata, rowindex, currentdata, column) : column.editor.p, options);
                    $inputText.ligerDateEditor(options);
                }
                else if (column.editor.type == 'select') {
                    var $inputText = $("<input type='text'/>");
                    g.grideditor.append($inputText);
                    //$inputText.val(currentdata);
                    var options = {
                        width: $(rowcell).width(),
                        onSelected: function (newValue, newText) {
                            $(rowcell).addClass("l-grid-row-cell-edited");
                            if (column.editor.valueColumnName)
                                rowdata[column.editor.valueColumnName] = newValue;
                            if (column.editor.displayColumnName)
                                rowdata[column.editor.displayColumnName] = newText;
                            if (column.editor.onChange) column.editor.onChange(rowcell, newValue);
                            editParm.value = newValue;
                            if (po.checkEditAndUpdateCell(editParm)) {
                                if (column.editor.onChanged) column.editor.onChanged(rowcell, newValue);
                            }
                        }
                    };
                    if (column.editor.data) options.data = column.editor.data;
                    if (column.editor.dataValueField) options.valueField = column.editor.dataValueField;
                    else if (column.editor.valueColumnName) options.valueField = column.editor.valueColumnName;
                    if (column.editor.dataDisplayField) options.displayField = options.textField = column.editor.dataDisplayField;
                    else if (column.editor.displayColumnName) options.displayField = options.textField = column.editor.displayColumnName;
                    if (column.editor.valueColumnName)
                        options.initValue = rowdata[column.editor.valueColumnName];
                    else if (column.editor.dataDisplayField)
                        options.initText = rowdata[column.editor.dataDisplayField];
                    if (column.editor.p) {
                        var tmp = typeof (column.editor.p) == 'function'
                        ? column.editor.p(rowdata, rowindex, currentdata, column)
                        : column.editor.p;
                        options = $.extend({}, options, tmp);
                    }
                    $inputText.ligerComboBox(options);
                }
                else if (column.editor.type == 'int' || column.editor.type == 'float' || column.editor.type == 'spinner') {
                    var $inputText = $("<input type='text'/>");
                    g.grideditor.append($inputText);
                    $inputText.attr({ style: 'border:#6E90BE' }).val(currentdata);
                    var options = {
                        width: $(rowcell).width() - 2,
                        height: $(rowcell).height(),
                        type: column.editor.type == 'float' ? 'float' : 'int',
                        isNegative: column.editor.isNegative,
                        onChangeValue: function (newValue) {
                            $(rowcell).addClass("l-grid-row-cell-edited");
                            if (column.editor.onChange) column.editor.onChange(rowcell, newValue);
                            if (newValue <= options.minValue) newValue = options.minValue;
                            if (newValue >= options.maxValue) newValue = options.maxValue;
                            editParm.value = newValue;
                            if (po.checkEditAndUpdateCell(editParm)) {
                                if (column.editor.onChanged) column.editor.onChanged(rowcell, newValue);
                            }
                        }
                    };
                    if (column.editor.minValue != undefined) options.minValue = column.editor.minValue;
                    if (column.editor.maxValue != undefined) options.maxValue = column.editor.maxValue;
                    $inputText.ligerSpinner(options);
                }
                else if (column.editor.type == 'string' || column.editor.type == 'text') {
                    var $inputText = $("<input type='text' class='l-text-editing'/>");

                    g.grideditor.append($inputText);
                    $inputText.val(currentdata);

                    var options = {
                        width: $(rowcell).width() - 1,
                        height: $(rowcell).height(),
                        onChangeValue: function (newValue) {
                            $(rowcell).addClass("l-grid-row-cell-edited");
                            if (column.editor.onChange) column.editor.onChange(rowcell, newValue);
                            editParm.value = newValue;
                            if (po.checkEditAndUpdateCell(editParm)) {
                                if (column.editor.onChanged) column.editor.onChanged(rowcell, newValue);
                            }
                        }
                    };
                    $inputText.ligerTextBox(options);
                    $inputText.bind('keydown', function (e) {
                        var key = e.which;
                        if (key == 13) {
                            $inputText.trigger("change");
                            g.endEdit();
                        }
                    });
                    $inputText.parent().addClass("l-text-editing");
                }
                else if (column.editor.type == 'chk' || column.editor.type == 'checkbox') {
                    var $input = $("<input type='checkbox'/>");
                    g.grideditor.append($input);
                    $input[0].checked = currentdata == 1 ? true : false;
                    $input.ligerCheckBox();
                    $input.change(function () {
                        if (column.editor.onChange) column.editor.onChange(rowcell, this.checked);
                        editParm.value = this.checked ? 1 : 0;
                        if (po.checkEditAndUpdateCell(editParm)) {
                            if (column.editor.onChanged) column.editor.onChanged(rowcell, this.checked);
                        }
                    });
                }
                g.grideditor.show();
                //$(":input", g.grideditor).focus();//add
                try {
                    if (typeof (eval(moveEnd)) == "function") {
                        moveEnd($(":input", g.grideditor));
                    }
                } catch (e) { //alert(e);
                }
            },
            checkEditAndUpdateCell: function (editParm) {
                if (p.onBeforeSubmitEdit) {
                    if (!p.onBeforeSubmitEdit(editParm)) return false;
                }
                g.grideditor.editingValue = editParm.value;
                g.updateCell(editParm.cellObj, editParm.value);
                return true;
            },
            getCurrentPageData: function (jsonObj) {
                var data = $.extend({}, jsonObj);
                data[p.root] = new Array();
                if (!jsonObj || !jsonObj[p.root] || !jsonObj[p.root].length) {
                    data[p.record] = 0;
                    return data;
                }
                data[p.record] = jsonObj[p.root].length ? jsonObj[p.root].length : 0;
                if (!p.newPage) p.newPage = 1;
                for (i = (p.newPage - 1) * p.pageSize; i < jsonObj[p.root].length && i < p.newPage * p.pageSize; i++) {
                    var obj = $.extend({}, jsonObj[p.root][i]);
                    data[p.root].push(obj);
                }
                return data;
            },
            //比较某一列两个数据
            compareData: function (data1, data2, columnName, columnType) {
                if (data1[columnName] == null && data2[columnName] != null)
                    return 1;
                else if (data1[columnName] == null && data2[columnName] == null)
                    return 0;
                else if (data1[columnName] != null && data2[columnName] == null)
                    return -1;
                switch (columnType) {
                    case "int":
                        return parseInt(data1[columnName]) < parseInt(data2[columnName]) ? -1 : parseInt(data1[columnName]) > parseInt(data2[columnName]) ? 1 : 0;
                    case "float":
                        return parseFloat(data1[columnName]) < parseFloat(data2[columnName]) ? -1 : parseFloat(data1[columnName]) > parseFloat(data2[columnName]) ? 1 : 0;
                    case "string":
                        return data1[columnName].localeCompare(data2[columnName]);
                    case "date":
                        return data1[columnName] < data2[columnName] ? -1 : data1[columnName] > data2[columnName] ? 1 : 0;
                }
                return data1[columnName].localeCompare(data2[columnName]);
            },
            getTotalSummaryHtml: function (data, classCssName) {
                var totalsummaryArr = [];
                if (classCssName)
                    totalsummaryArr.push('<tr class="l-grid-totalsummary ' + classCssName + '"');
                else
                    totalsummaryArr.push('<tr class="l-grid-totalsummary"');
                if (p.defaultCloseGroup) totalsummaryArr.push(' style="display: none;"');//modify
                totalsummaryArr.push(' >');//modify

                $(g.headers).each(function (headerCellIndex, headerInfor) {
                    //如果是复选框(系统列)
                    if (this.ischeckbox) {
                        totalsummaryArr.push('<td class="l-grid-totalsummary-cell l-grid-totalsummary-cell-checkbox" style="width:' + this.width + 'px"></td>');
                        return;
                    }
                        //如果是明细列(系统列)
                    else if (this.isdetail) {
                        totalsummaryArr.push('<td class="l-grid-totalsummary-cell l-grid-totalsummary-cell-detail" style="width:' + this.width + 'px"></td>');
                        return;
                    }
                    totalsummaryArr.push('<td class="l-grid-totalsummary-cell');
                    if (this.islast)
                        totalsummaryArr.push(" l-grid-totalsummary-cell-last");
                    totalsummaryArr.push('" ');
                    totalsummaryArr.push('width="' + this.width + '" ');
                    columnname = this.columnname;
                    columnindex = this.columnindex;
                    if (columnname) {
                        totalsummaryArr.push('columnname="' + columnname + '" ');
                    }
                    totalsummaryArr.push('columnindex="' + columnindex + '" ');
                    totalsummaryArr.push('><div class="l-grid-totalsummary-cell-inner"');

                    var column = g.columns[columnindex];
                    if (column.align)
                        totalsummaryArr.push(' textAlign="' + column.align + '"');
                    totalsummaryArr.push('>');

                    if (column.totalSummary) {
                        var isExist = function (type) {
                            for (var i = 0; i < types.length; i++)
                                if (types[i].toLowerCase() == type.toLowerCase()) return true;
                            return false;
                        };
                        var sum = 0, count = 0, avg = 0;
                        var max = parseFloat(data[0][column.name]);
                        var min = parseFloat(data[0][column.name]);
                        for (var i = 0; i < data.length; i++) {
                            count += 1;
                            var value = parseFloat(data[i][column.name]);
                            if (!value) continue;
                            sum += value;
                            if (value > max) max = value;
                            if (value < min) min = value;
                        }
                        avg = sum * 1.0 / data.length;
                        if (column.totalSummary.render) {
                            var renderhtml = column.totalSummary.render({
                                sum: sum,
                                count: count,
                                avg: avg,
                                min: min,
                                max: max
                            }, column, g.data);
                            totalsummaryArr.push(renderhtml);
                        }
                        else if (column.totalSummary.type) {
                            var types = column.totalSummary.type.split(',');
                            if (isExist('sum'))
                                //totalsummaryArr.push("<div>Sum=" + sum.toFixed(2) + "</div>");
                                totalsummaryArr.push("<div>" + sum.toFixed(0) + "</div>");
                            if (isExist('count'))
                                totalsummaryArr.push("<div>Count=" + count + "</div>");
                            if (isExist('max'))
                                totalsummaryArr.push("<div>Max=" + max.toFixed(2) + "</div>");
                            if (isExist('min'))
                                totalsummaryArr.push("<div>Min=" + min.toFixed(2) + "</div>");
                            if (isExist('avg'))
                                totalsummaryArr.push("<div>Avg=" + avg.toFixed(2) + "</div>");
                            if (isExist('total'))
                                totalsummaryArr.push("<div style='text-align:right;'>总计：</div>");
                            if (isExist('sum_money'))
                                //totalsummaryArr.push("<div>Sum=" + sum.toFixed(2) + "</div>");
                                totalsummaryArr.push("<div style='text-align:right;'>￥" + toMoney(sum.toFixed(0)) + "</div>");
                        }
                        totalsummaryArr.push('</div></td>');
                    }
                });
                totalsummaryArr.push('</tr>');
                return totalsummaryArr.join('');
            },
            bulidTotalSummary: function () {
                if (!g.isTotalSummary()) return false;
                if (!g.currentData || g.currentData[p.root].length == 0) return false;
                var totalRow = $(po.getTotalSummaryHtml(g.currentData[p.root]));
                po.recordRow(totalRow[0]);
                $("tbody:first", g.gridbody).append(totalRow);
            },
            buildPager: function () {
                $('.pcontrol input', g.toolbar).val(p.page);
                //$.fn.ligerTextBox && $('.pcontrol input', g.toolbar).ligerTextBox({ width: 30 });
                if (!p.pageCount) p.pageCount = 1;
                $('.pcontrol span', g.toolbar).html(p.pageCount);
                var r1 = parseInt((p.page - 1) * p.pageSize) + 1.0;
                var r2 = parseInt(r1) + parseInt(p.pageSize) - 1;
                if (!p.total) p.total = 0;
                if (p.total < r2) r2 = p.total;
                if (!p.total) r1 = r2 = 0;
                if (r1 < 0) r1 = 0;
                if (r2 < 0) r2 = 0;
                var stat = p.pageStatMessage;
                stat = stat.replace(/{from}/, r1);
                stat = stat.replace(/{to}/, r2);
                stat = stat.replace(/{total}/, p.total);
                stat = stat.replace(/{pagesize}/, p.pageSize);
                $('.l-bar-text', g.toolbar).html(stat);
                if (!p.total) {
                    $(".l-bar-btnfirst span,.l-bar-btnprev span,.l-bar-btnnext span,.l-bar-btnlast span", g.toolbar)
                    .addClass("l-disabled");
                }
                if (p.page == 1) {
                    $(".l-bar-btnfirst span", g.toolbar).addClass("l-disabled");
                    $(".l-bar-btnprev span", g.toolbar).addClass("l-disabled");
                }
                else if (p.page > p.pageCount && p.pageCount > 0) {
                    $(".l-bar-btnfirst span", g.toolbar).removeClass("l-disabled");
                    $(".l-bar-btnprev span", g.toolbar).removeClass("l-disabled");
                }
                if (p.page == p.pageCount) {
                    $(".l-bar-btnlast span", g.toolbar).addClass("l-disabled");
                    $(".l-bar-btnnext span", g.toolbar).addClass("l-disabled");
                }
                else if (p.page < p.pageCount && p.pageCount > 0) {
                    $(".l-bar-btnlast span", g.toolbar).removeClass("l-disabled");
                    $(".l-bar-btnnext span", g.toolbar).removeClass("l-disabled");
                }
            },
            onResize: function () {
                if (p.height && p.height != 'auto') {
                    var windowHeight = $(window).height();
                    //if(g.windowHeight != undefined && g.windowHeight == windowHeight) return;

                    var h = 0;
                    var parentHeight = null;
                    if (typeof (p.height) == "string" && p.height.indexOf('%') > 0) {
                        var gridparent = $(grid).parent();
                        if (p.InWindow || gridparent[0].tagName.toLowerCase() == "body") {
                            parentHeight = windowHeight;
                            parentHeight -= parseInt($('body').css('paddingTop'));
                            parentHeight -= parseInt($('body').css('paddingBottom'));
                        }
                        else {
                            parentHeight = gridparent.height();
                        }
                        h = parentHeight * parseFloat(p.height) * 0.01;
                        if (p.InWindow || gridparent[0].tagName.toLowerCase() == "body")
                            h -= ($(grid).offset().top - parseInt($('body').css('paddingTop')));
                    }
                    else {
                        h = parseInt(p.height);
                    }

                    h += p.heightDiff;
                    g.windowHeight = windowHeight;
                    po.setHeight(h);
                }
            },
            setHeight: function (h) {
                if (p.title) h -= 24;
                if (p.usePager) h -= $(".l-panel-bar").height() * 1.2;
                if (p.totalRender) h -= 25;
                h -= 23 * (g.getMulHeaderLevel() - 1);
                h -= 22;
                h > 0 && g.gridbody.height(h);
            },
            dragStart: function (dragtype, e, toDragHeaderIndex) {
                if (dragtype == 'colresize') //列宽调整
                {
                    g.popup.hide();
                    var columnindex = g.headers[g.toDragHeaderIndex].columnindex;
                    var width = g.headers[g.toDragHeaderIndex].width;
                    if (columnindex == undefined) return;
                    g.colresize = { startX: e.pageX, width: width, columnindex: columnindex };
                    $('body').css('cursor', 'e-resize');
                    g.draggingline.css({ height: g.body.height(), left: e.pageX - $(grid).offset().left + parseInt(g.body[0].scrollLeft), top: 0 }).show();

                    $('body').bind('selectstart', function () { return false; });
                }
                $.fn.ligerNoSelect && $('body').ligerNoSelect();
            },
            dragMove: function (e) {
                if (g.colresize) //列 调整
                {
                    var diff = e.pageX - g.colresize.startX;
                    var newwidth = g.colresize.width + diff;
                    g.colresize.newwidth = newwidth;
                    $('body').css('cursor', 'e-resize');
                    g.draggingline.css({ left: e.pageX - $(grid).offset().left + parseInt(g.body[0].scrollLeft) });

                    $('body').unbind('selectstart');
                }
            },
            dragEnd: function (e) {
                if (g.colresize) {
                    if (g.colresize.newwidth == undefined) {
                        $('body').css('cursor', 'default');
                        return false;
                    }
                    var mincolumnwidth = 40;
                    var columnindex = g.colresize.columnindex;
                    var column = g.columns[columnindex];
                    if (column && column.minWidth) mincolumnwidth = column.minWidth;
                    var newwidth = g.colresize.newwidth;
                    newwidth = newwidth < mincolumnwidth ? mincolumnwidth : newwidth;
                    var diff = newwidth - g.colresize.width;
                    g.headers[g.toDragHeaderIndex].width += diff;
                    g.gridtablewidth += diff;


                    $("div:first", g.gridheader).width(g.gridtablewidth + 40);
                    $("div:first", g.gridbody).width(g.gridtablewidth);
                    $('td[columnindex=' + columnindex + ']', g.gridheader).css('width', newwidth);
                    if (g.recordNumber > 0) {
                        $('td[columnindex=' + columnindex + ']', g.totalRows).add(g.columnCells[columnindex]).each(function () {
                            $(this).css('width', newwidth);
                            $("div:first", this).css('width', newwidth - 8);
                        });

                    }
                    po.onResize();
                    g.draggingline.hide();

                    g.colresize = false;

                    p.onDragCol && p.onDragCol(columnindex, newwidth);
                }

                $('body').css('cursor', 'default');
                $.fn.ligerNoSelect && $('body').ligerNoSelect(false);
            }
        };
        var g = new $.ligerManagers.Grid(p, po);
        //头部
        g.header = $(".l-grid-panel-header:first", grid);
        //主体
        g.body = $(".l-panel-body:first", grid);
        //底部工具条         
        g.toolbar = $(".l-panel-bar:first", grid);
        //显示/隐藏列      
        g.popup = $(".l-grid-popup:first", grid);
        //编辑   
        g.grideditor = $(".l-grid-editor:first", grid);
        //加载中
        g.gridloading = $(".l-grid-loading:first", grid);
        //调整列宽层 
        g.draggingline = $(".l-grid-dragging-line", grid);
        //表头     
        g.gridheader = $(".l-grid-header:first", grid);
        //表主体     
        g.gridbody = $(".l-grid-body:first", grid);
        g.currentData = null;



        po.init();

        $.ligerui.addManager(grid, g);
    };

    var ligerGridSetParms = function (options, fixedP) {
        var p = $.extend({}, $.ligerDefaults.Grid, $.ligerDefaults.GridString, options || {});
        if (p.url && p.data) {
            p.dataType = "local";
        }
        else if (p.url && !p.data) {
            p.dataType = "server";
        }
        else if (!p.url && p.data) {
            p.dataType = "local";
        }
        else if (!p.url && !p.data) {
            p.dataType = "local";
            p.data = [];
        }
        if (p.dataType == "local")
            p.dataAction = "local";
        if (fixedP) {
            $.extend(p, fixedP);
        }
        return p;
    };

    $.fn.ligerGrid = function (options) {
        var fixedP = {};
        this.each(function () {
            var p = ligerGridSetParms(options, fixedP);
            $.ligerAddGrid(this, p);
        });
        return $.ligerui.getManager(this);
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
if (typeof (LigerUIManagers) == "undefined") LigerUIManagers = {};
(function ($) {
    ///	<param name="$" type="jQuery"></param>

    $.fn.ligerGetLayoutManager = function () {
        return LigerUIManagers[this[0].id + "_Layout"];
    };
    $.fn.ligerRemoveLayoutManager = function () {
        return this.each(function () {
            LigerUIManagers[this.id + "_Layout"] = null;
        });
    };
    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Layout = {
        topHeight: 50,
        bottomHeight: 50,
        leftWidth: 110,
        centerWidth: 300,
        rightWidth: 170,
        InWindow: true,     //是否以窗口的高度为准 height设置为百分比时可用
        heightDiff: 0,     //高度补差
        height: '100%',      //高度
        onHeightChanged: null,
        isLeftCollapse: false,      //初始化时 左边是否隐藏
        isRightCollapse: false,     //初始化时 右边是否隐藏
        allowLeftCollapse: true,      //是否允许 左边可以隐藏
        allowRightCollapse: true,     //是否允许 右边可以隐藏
        allowLeftResize: true,      //是否允许 左边可以调整大小
        allowRightResize: true,     //是否允许 右边可以调整大小
        allowTopResize: true,      //是否允许 头部可以调整大小
        allowBottomResize: true,     //是否允许 底部可以调整大小
        space: 3, //间隔,
        onHeightChange:null
    };
    $.fn.ligerLayout = function (p) {
        this.each(function () {
            p = $.extend({}, $.ligerDefaults.Layout, p || {});
            if (this.usedLayout) return;
            var g = {
                init: function () {
                    $("> .l-layout-left .l-layout-header,> .l-layout-right .l-layout-header", g.layout).hover(function () {
                        $(this).addClass("l-layout-header-over");
                    }, function () {
                        $(this).removeClass("l-layout-header-over");
                    });
                    $(".l-layout-header-toggle", g.layout).hover(function () {
                        $(this).addClass("l-layout-header-toggle-over");
                    }, function () {
                        $(this).removeClass("l-layout-header-toggle-over");

                    });
                    $(".l-layout-header-toggle", g.left).click(function () {
                        g.setLeftCollapse(true);
                    });
                    $(".l-layout-header-toggle", g.right).click(function () {
                        g.setRightCollapse(true);
                    });
                    //set top
                    g.middleTop = 0;
                    if (g.top) {
                        g.middleTop += g.top.height();
                        g.middleTop += parseInt(g.top.css('borderTopWidth'));
                        g.middleTop += parseInt(g.top.css('borderBottomWidth'));
                        g.middleTop += p.space;
                    }
                    if (g.left) {
                        g.left.css({ top: g.middleTop });
                        g.leftCollapse.css({ top: g.middleTop });
                    }
                    if (g.center) g.center.css({ top: g.middleTop });
                    if (g.right) {
                        g.right.css({ top: g.middleTop });
                        g.rightCollapse.css({ top: g.middleTop });
                    }
                    //set left
                    if (g.left) g.left.css({ left: 0 });
                    g.onResize();
                    g.onResize();
                },
                setCollapse: function () {

                    g.leftCollapse.hover(function () {
                        $(this).addClass("l-layout-collapse-left-over");
                    }, function () {
                        $(this).removeClass("l-layout-collapse-left-over");
                    });
                    g.leftCollapse.toggle.hover(function () {
                        $(this).addClass("l-layout-collapse-left-toggle-over");
                    }, function () {
                        $(this).removeClass("l-layout-collapse-left-toggle-over");
                    });
                    g.rightCollapse.hover(function () {
                        $(this).addClass("l-layout-collapse-right-over");
                    }, function () {
                        $(this).removeClass("l-layout-collapse-right-over");
                    });
                    g.rightCollapse.toggle.hover(function () {
                        $(this).addClass("l-layout-collapse-right-toggle-over");
                    }, function () {
                        $(this).removeClass("l-layout-collapse-right-toggle-over");
                    });
                    g.leftCollapse.toggle.click(function () {
                        g.setLeftCollapse(false);
                    });
                    g.rightCollapse.toggle.click(function () {
                        g.setRightCollapse(false);
                    });
                    if (g.left && g.isLeftCollapse) {
                        g.leftCollapse.show();
                        g.leftDropHandle && g.leftDropHandle.hide();
                        g.left.hide();
                    }
                    if (g.right && g.isRightCollapse) {
                        g.rightCollapse.show();
                        g.rightDropHandle && g.rightDropHandle.hide();
                        g.right.hide();
                    }
                },
                setLeftCollapse: function (isCollapse) {
                    if (!g.left) return false;
                    g.isLeftCollapse = isCollapse;
                    if (g.isLeftCollapse) {
                        g.leftCollapse.show();
                        g.leftDropHandle && g.leftDropHandle.hide();
                        g.left.hide();
                    }
                    else {
                        g.leftCollapse.hide();
                        g.leftDropHandle && g.leftDropHandle.show();
                        g.left.show();
                    }
                    g.onResize();
                },
                setRightCollapse: function (isCollapse) {
                    if (!g.right) return false;
                    g.isRightCollapse = isCollapse;
                    g.onResize();
                    if (g.isRightCollapse) {
                        g.rightCollapse.show();
                        g.rightDropHandle && g.rightDropHandle.hide();
                        g.right.hide();
                    }
                    else {
                        g.rightCollapse.hide();
                        g.rightDropHandle && g.rightDropHandle.show();
                        g.right.show();
                    }
                    g.onResize();
                },
                addDropHandle: function () {
                    if (g.left && p.allowLeftResize) {
                        g.leftDropHandle = $("<div class='l-layout-drophandle-left'></div>");
                        g.layout.append(g.leftDropHandle);
                        g.leftDropHandle && g.leftDropHandle.show();
                        g.leftDropHandle.mousedown(function (e) {
                            g.start('leftresize', e);
                        });
                    }
                    if (g.right && p.allowRightResize) {
                        g.rightDropHandle = $("<div class='l-layout-drophandle-right'></div>");
                        g.layout.append(g.rightDropHandle);
                        g.rightDropHandle && g.rightDropHandle.show();
                        g.rightDropHandle.mousedown(function (e) {
                            g.start('rightresize', e);
                        });
                    }
                    if (g.top && p.allowTopResize) {
                        g.topDropHandle = $("<div class='l-layout-drophandle-top'></div>");
                        g.layout.append(g.topDropHandle);
                        g.topDropHandle.show();
                        g.topDropHandle.mousedown(function (e) {
                            g.start('topresize', e);
                        });
                    }
                    if (g.bottom && p.allowBottomResize) {
                        g.bottomDropHandle = $("<div class='l-layout-drophandle-bottom'></div>");
                        g.layout.append(g.bottomDropHandle);
                        g.bottomDropHandle.show();
                        g.bottomDropHandle.mousedown(function (e) {
                            g.start('bottomresize', e);
                        });
                    }
                    g.draggingxline = $("<div class='l-layout-dragging-xline'></div>");
                    g.draggingyline = $("<div class='l-layout-dragging-yline'></div>");
                    g.layout.append(g.draggingxline).append(g.draggingyline);
                },
                setDropHandlePosition: function () {
                    if (g.leftDropHandle) {
                        g.leftDropHandle.css({ left: g.left.width() + parseInt(g.left.css('left')), height: g.middleHeight, top: g.middleTop });
                    }
                    if (g.rightDropHandle) {
                        g.rightDropHandle.css({ left: parseInt(g.right.css('left')) - p.space, height: g.middleHeight, top: g.middleTop });
                    }
                    if (g.topDropHandle) {
                        g.topDropHandle.css({ top: g.top.height() + parseInt(g.top.css('top')), width: g.top.width() });
                    }
                    if (g.bottomDropHandle) {
                        g.bottomDropHandle.css({ top: parseInt(g.bottom.css('top')) - p.space, width: g.bottom.width() });                       
                    }
                    p.onHeightChange && p.onHeightChange();                   
                },
                onResize: function () {
                    var oldheight = g.layout.height();
                    //set layout height 
                    var h = 0; 
                    var windowHeight = $(window).height();
                    var parentHeight = null;
                    if (typeof (p.height) == "string" && p.height.indexOf('%') > 0) {
                        var layoutparent = g.layout.parent();
                        if (p.InWindow || layoutparent[0].tagName.toLowerCase() == "body") {
                            parentHeight = windowHeight;
                            parentHeight -= parseInt($('body').css('paddingTop'));
                            parentHeight -= parseInt($('body').css('paddingBottom'));
                        }
                        else {
                            parentHeight = layoutparent.height();
                        }
                        h = parentHeight * parseFloat(p.height) * 0.01;
                        if (p.InWindow || layoutparent[0].tagName.toLowerCase() == "body")
                            h -= (g.layout.offset().top - parseInt($('body').css('paddingTop')));
                    }
                    else {
                        h = parseInt(p.height);
                    }
                    h += p.heightDiff;
                    g.layout.height(h);
                    g.layoutHeight = g.layout.height();
                    g.middleWidth = g.layout.width();
                    g.middleHeight = g.layout.height();
                    if (g.top) {
                        g.middleHeight -= g.top.height();
                        g.middleHeight -= parseInt(g.top.css('borderTopWidth'));
                        g.middleHeight -= parseInt(g.top.css('borderBottomWidth'));
                        g.middleHeight -= p.space;
                    }
                    if (g.bottom) {
                        g.middleHeight -= g.bottom.height();
                        g.middleHeight -= parseInt(g.bottom.css('borderTopWidth'));
                        g.middleHeight -= parseInt(g.bottom.css('borderBottomWidth'));
                        g.middleHeight -= p.space;
                    }
                    //specific
                    g.middleHeight -= 2;

                    if (p.onHeightChanged && g.layoutHeight != oldheight) {
                        p.onHeightChanged({ layoutHeight: g.layoutHeight, diff: g.layoutHeight - oldheight, middleHeight: g.middleHeight });
                    }

                    if (g.center) {
                        g.centerWidth = g.middleWidth;
                        if (g.left) {
                            if (g.isLeftCollapse) {
                                g.centerWidth -= g.leftCollapse.width();
                                g.centerWidth -= parseInt(g.leftCollapse.css('borderLeftWidth'));
                                g.centerWidth -= parseInt(g.leftCollapse.css('borderRightWidth'));
                                g.centerWidth -= parseInt(g.leftCollapse.css('left'));
                                g.centerWidth -= p.space;
                            }
                            else {
                                g.centerWidth -= g.leftWidth;
                                g.centerWidth -= parseInt(g.left.css('borderLeftWidth'));
                                g.centerWidth -= parseInt(g.left.css('borderRightWidth'));
                                g.centerWidth -= parseInt(g.left.css('left'));
                                g.centerWidth -= p.space;
                            }
                        }
                        if (g.right) {
                            if (g.isRightCollapse) {
                                g.centerWidth -= g.rightCollapse.width();
                                g.centerWidth -= parseInt(g.rightCollapse.css('borderLeftWidth'));
                                g.centerWidth -= parseInt(g.rightCollapse.css('borderRightWidth'));
                                g.centerWidth -= parseInt(g.rightCollapse.css('right'));
                                g.centerWidth -= p.space; g.centerWidth -= p.space;
                            }
                            else {
                                g.centerWidth -= g.rightWidth;
                                g.centerWidth -= parseInt(g.right.css('borderLeftWidth'));
                                g.centerWidth -= parseInt(g.right.css('borderRightWidth'));
                                g.centerWidth -= p.space;
                            }
                        }
                        g.centerLeft = 0;
                        if (g.left) {
                            if (g.isLeftCollapse) {
                                g.centerLeft += g.leftCollapse.width()-1;//
                                g.centerLeft += parseInt(g.leftCollapse.css('borderLeftWidth'));
                                g.centerLeft += parseInt(g.leftCollapse.css('borderRightWidth'));
                                g.centerLeft += parseInt(g.leftCollapse.css('left'));
                                g.centerLeft += p.space;
                            }
                            else {
                                g.centerLeft += g.left.width();
                                g.centerLeft += parseInt(g.left.css('borderLeftWidth'));
                                g.centerLeft += parseInt(g.left.css('borderRightWidth'));
                                g.centerLeft += p.space;
                            }
                        }
                        g.center.css({ left: g.centerLeft });
                        g.center.width(g.centerWidth);
                        g.center.height(g.middleHeight + 1);//
                        var contentHeight = g.middleHeight;
                        if (g.center.header) contentHeight -= g.center.header.height();
                        g.center.content.height(contentHeight);
                    }
                    if (g.left) {
                        g.leftCollapse.height(g.middleHeight + 1);//
                        g.left.height(g.middleHeight + 1);//
                        g.left.content.height(g.middleHeight - 35);
                    }
                    if (g.right) {
                        g.rightCollapse.height(g.middleHeight + 1);//
                        g.right.height(g.middleHeight + 1);//
                        g.right.content.height(g.middleHeight - 35);
                        g.rightLeft = 0;

                        if (g.left) {
                            if (g.isLeftCollapse) {
                                g.rightLeft += g.leftCollapse.width() + 2;//
                                g.rightLeft += parseInt(g.leftCollapse.css('borderLeftWidth'));
                                g.rightLeft += parseInt(g.leftCollapse.css('borderRightWidth'));
                                g.rightLeft += p.space;
                            }
                            else {
                                g.rightLeft += g.left.width();
                                g.rightLeft += parseInt(g.left.css('borderLeftWidth'));
                                g.rightLeft += parseInt(g.left.css('borderRightWidth'));
                                g.rightLeft += parseInt(g.left.css('left'));
                                g.rightLeft += p.space;
                            }
                        }
                        if (g.center) {
                            g.rightLeft += g.center.width();
                            g.rightLeft += parseInt(g.center.css('borderLeftWidth'));
                            g.rightLeft += parseInt(g.center.css('borderRightWidth'));
                            g.rightLeft += p.space;
                        }
                        g.right.css({ left: g.rightLeft });
                    }
                    if (g.bottom) {
                        g.bottomTop = g.layoutHeight - g.bottom.height() - 2;
                        g.bottom.css({ top: g.bottomTop });
                    }
                    g.setDropHandlePosition();

                },
                start: function (dragtype, e) {
                    g.dragtype = dragtype;
                    if (dragtype == 'leftresize' || dragtype == 'rightresize') {
                        g.xresize = { startX: e.pageX };
                        g.draggingyline.css({ left: e.pageX - g.layout.offset().left, height: g.middleHeight, top: g.middleTop }).show();
                        $('body').css('cursor', 'col-resize');
                    }
                    else if (dragtype == 'topresize' || dragtype == 'bottomresize') {
                        g.yresize = { startY: e.pageY };
                        g.draggingxline.css({ top: e.pageY - g.layout.offset().top, width: g.layout.width() }).show();
                        $('body').css('cursor', 'row-resize');
                    }
                    else {
                        return;
                    }

                    g.layout.lock.width(g.layout.width());
                    g.layout.lock.height(g.layout.height());
                    g.layout.lock.show();
                    if ($.browser.msie || $.browser.safari) $('body').bind('selectstart', function () { return false; }); // 不能选择

                    $(document).bind('mouseup', g.stop);
                    $(document).bind('mousemove', g.drag);
                },
                drag: function (e) {
                    if (g.xresize) {
                        g.xresize.diff = e.pageX - g.xresize.startX;
                        g.draggingyline.css({ left: e.pageX - g.layout.offset().left });
                        $('body').css('cursor', 'col-resize');
                    }
                    else if (g.yresize) {
                        g.yresize.diff = e.pageY - g.yresize.startY;
                        g.draggingxline.css({ top: e.pageY - g.layout.offset().top });
                        $('body').css('cursor', 'row-resize');
                    }
                },
                stop: function (e) {

                    if (g.xresize && g.xresize.diff != undefined) {
                        if (g.dragtype == 'leftresize') {
                            g.leftWidth += g.xresize.diff;
                            g.left.width(g.leftWidth);
                            if (g.center)
                                g.center.width(g.center.width() - g.xresize.diff).css({ left: parseInt(g.center.css('left')) + g.xresize.diff });
                            else if (g.right)
                                g.right.width(g.left.width() - g.xresize.diff).css({ left: parseInt(g.right.css('left')) + g.xresize.diff });
                        }
                        else if (g.dragtype == 'rightresize') {
                            g.rightWidth -= g.xresize.diff;
                            g.right.width(g.rightWidth).css({ left: parseInt(g.right.css('left')) + g.xresize.diff });
                            if (g.center)
                                g.center.width(g.center.width() + g.xresize.diff);
                            else if (g.left)
                                g.left.width(g.left.width() + g.xresize.diff);
                        }
                    }
                    else if (g.yresize && g.yresize.diff != undefined) {
                        if (g.dragtype == 'topresize') {
                            g.top.height(g.top.height() + g.yresize.diff);
                            g.middleTop += g.yresize.diff;
                            g.middleHeight -= g.yresize.diff;
                            if (g.left) {
                                g.left.css({ top: g.middleTop }).height(g.middleHeight);
                                g.leftCollapse.css({ top: g.middleTop }).height(g.middleHeight);
                            }
                            if (g.center) g.center.css({ top: g.middleTop }).height(g.middleHeight);
                            if (g.right) {
                                g.right.css({ top: g.middleTop }).height(g.middleHeight);
                                g.rightCollapse.css({ top: g.middleTop }).height(g.middleHeight);
                            }
                        }
                        else if (g.dragtype == 'bottomresize') {
                            g.bottom.height(g.bottom.height() - g.yresize.diff);
                            g.middleHeight += g.yresize.diff;
                            g.bottomTop += g.yresize.diff;
                            g.bottom.css({ top: g.bottomTop });
                            if (g.left) {
                                g.left.height(g.middleHeight);
                                g.leftCollapse.height(g.middleHeight);
                            }
                            if (g.center) {
                                g.center.height(g.middleHeight);
                                g.center.content.height(g.middleHeight);
                            }
                            if (g.right) {
                                g.right.height(g.middleHeight);
                                g.rightCollapse.height(g.middleHeight);
                            }                            
                        }
                    }
                    g.setDropHandlePosition();
                    g.draggingxline.hide();
                    g.draggingyline.hide();
                    g.xresize = g.yresize = g.dragtype = false;
                    g.layout.lock.hide();
                    if ($.browser.msie || $.browser.safari)
                        $('body').unbind('selectstart');
                    $(document).unbind('mousemove', g.drag);
                    $(document).unbind('mouseup', g.stop);
                    $('body').css('cursor', '');
                }
            };
            g.layout = $(this);
            if (!g.layout.hasClass("l-layout"))
                g.layout.addClass("l-layout");
            g.width = g.layout.width();
            //top
            if ($("> div[position=top]", g.layout).length > 0) {
                g.top = $("> div[position=top]", g.layout).wrap('<div class="l-layout-top" style="top:0px;"></div>').parent();
                g.top.content = $("> div[position=top]", g.top);
                if (!g.top.content.hasClass("l-layout-content"))
                    g.top.content.addClass("l-layout-content");
                g.topHeight = p.topHeight;
                if (g.topHeight) {
                    g.top.height(g.topHeight);
                }
            }

            //bottom
            if ($("> div[position=bottom]", g.layout).length > 0) {
                g.bottom = $("> div[position=bottom]", g.layout).wrap('<div class="l-layout-bottom"></div>').parent();
                g.bottom.content = $("> div[position=bottom]", g.top);
                if (!g.bottom.content.hasClass("l-layout-content"))
                    g.bottom.content.addClass("l-layout-content");

                g.bottomHeight = p.bottomHeight;
                if (g.bottomHeight) {
                    g.bottom.height(g.bottomHeight);
                }

            }
            //left
            if ($("> div[position=left]", g.layout).length > 0) {
                g.left = $("> div[position=left]", g.layout).wrap('<div class="l-layout-left" style="left:0px;"></div>').parent();
                g.left.header = $('<div class="l-layout-header"><div class="l-layout-header-toggle"></div><div class="l-layout-header-inner"></div></div>');
                g.left.prepend(g.left.header);
                g.left.header.toggle = $(".l-layout-header-toggle", g.left.header);
                g.left.content = $("> div[position=left]", g.left);
                if (!g.left.content.hasClass("l-layout-content"))
                    g.left.content.addClass("l-layout-content");
                if (!p.allowLeftCollapse) $(".l-layout-header-toggle", g.left.header).remove();
                //set title
                var lefttitle = g.left.content.attr("title");
                if (lefttitle) {
                    g.left.content.attr("title", "");
                    $(".l-layout-header-inner", g.left.header).html(lefttitle);
                }
                //set width
                g.leftWidth = p.leftWidth;
                if (g.leftWidth)
                    g.left.width(g.leftWidth);
            }
            //center
            if ($("> div[position=center]", g.layout).length > 0) {
                g.center = $("> div[position=center]", g.layout).wrap('<div class="l-layout-center" ></div>').parent();
                g.center.content = $("> div[position=center]", g.center);
                g.center.content.addClass("l-layout-content");
                //set title
                var centertitle = g.center.content.attr("title");
                if (centertitle) {
                    g.center.content.attr("title", "");
                    g.center.header = $('<div class="l-layout-header"></div>');
                    g.center.prepend(g.center.header);
                    g.center.header.html(centertitle);
                }
                //set width
                g.centerWidth = p.centerWidth;
                if (g.centerWidth)
                    g.center.width(g.centerWidth);
            }
            //right
            if ($("> div[position=right]", g.layout).length > 0) {
                g.right = $("> div[position=right]", g.layout).wrap('<div class="l-layout-right"></div>').parent();

                g.right.header = $('<div class="l-layout-header"><div class="l-layout-header-toggle"></div><div class="l-layout-header-inner"></div></div>');
                g.right.prepend(g.right.header);
                g.right.header.toggle = $(".l-layout-header-toggle", g.right.header);
                if (!p.allowRightCollapse) $(".l-layout-header-toggle", g.right.header).remove();
                g.right.content = $("> div[position=right]", g.right);
                if (!g.right.content.hasClass("l-layout-content"))
                    g.right.content.addClass("l-layout-content");

                //set title
                var righttitle = g.right.content.attr("title");
                if (righttitle) {
                    g.right.content.attr("title", "");
                    $(".l-layout-header-inner", g.right.header).html(righttitle);
                }
                //set width
                g.rightWidth = p.rightWidth;
                if (g.rightWidth)
                    g.right.width(g.rightWidth);
            }
            //lock
            g.layout.lock = $("<div class='l-layout-lock'></div>");
            g.layout.append(g.layout.lock);
            //DropHandle
            g.addDropHandle();

            //Collapse
            g.isLeftCollapse = p.isLeftCollapse;
            g.isRightCollapse = p.isRightCollapse;
            g.leftCollapse = $('<div class="l-layout-collapse-left" style="display: none; "><div class="l-layout-collapse-left-toggle"></div></div>');
            g.rightCollapse = $('<div class="l-layout-collapse-right" style="display: none; "><div class="l-layout-collapse-right-toggle"></div></div>');
            g.layout.append(g.leftCollapse).append(g.rightCollapse);
            g.leftCollapse.toggle = $("> .l-layout-collapse-left-toggle", g.leftCollapse);
            g.rightCollapse.toggle = $("> .l-layout-collapse-right-toggle", g.rightCollapse);
            g.setCollapse();

            //init
            g.init();
            $(window).resize(function () {
                g.onResize();
            });
            if (this.id == undefined) this.id = "LigerUI_" + new Date().getTime();
            LigerUIManagers[this.id + "_Layout"] = g;
            this.usedLayout = true;
        });
        if (this.length == 0) return null;
        if (this.length == 1) return LigerUIManagers[this[0].id + "_Layout"];
        var managers = [];
        this.each(function () {
            managers.push(LigerUIManagers[this.id + "_Layout"]);
        });
        return managers;
    };
})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/  
(function ($)
{
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager)
    {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr)
    {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Menu = {
        width: 120,
        top: 0,
        left: 0,
        items: null,
        shadow: true
    };

    //menu manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Menu = function (options, po)
    {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.Menu.prototype = {
        show: function (options, menu)
        {
            var g = this, po = this.po, p = this.options;
            if (menu == undefined) menu = g.menu;
            if (options && options.left != undefined)
            {
                menu.css({ left: options.left });
            }
            if (options && options.top != undefined)
            {
                menu.css({ top: options.top });
            }
            menu.show();
            g.updateShadow(menu);
        },
        updateShadow: function (menu)
        {
            var g = this, po = this.po, p = this.options;
            if (!p.shadow) return;
            menu.shadow.css({
                left: menu.css('left'),
                top: menu.css('top'),
                width: menu.outerWidth(),
                height: menu.outerHeight()
            });
            if (menu.is(":visible"))
                menu.shadow.show();
            else
                menu.shadow.hide();
        },
        hide: function (menu)
        {
            var g = this, po = this.po, p = this.options;
            if (menu == undefined) menu = g.menu;
            g.hideAllSubMenu(menu);
            menu.hide();
            g.updateShadow(menu);
        },
        toggle: function ()
        {
            var g = this, po = this.po, p = this.options;
            g.menu.toggle();
            g.updateShadow(g.menu);
        },
        removeItem: function (itemid)
        {
            var g = this, po = this.po, p = this.options;
            $("> .l-menu-item[menuitemid=" + itemid + "]", g.menu.items).remove();
            g.itemCount--;
        },
        setEnabled: function (itemid)
        {
            var g = this, po = this.po, p = this.options;
            $("> .l-menu-item[menuitemid=" + itemid + "]", g.menu.items).removeClass("l-menu-item-disable");
        },
        setDisabled: function (itemid)
        {
            var g = this, po = this.po, p = this.options;
            $("> .l-menu-item[menuitemid=" + itemid + "]", g.menu.items).addClass("l-menu-item-disable");
        },
        isEnable: function (itemid)
        {
            var g = this, po = this.po, p = this.options;
            return !$("> .l-menu-item[menuitemid=" + itemid + "]", g.menu.items).hasClass("l-menu-item-disable");
        },
        getItemCount: function ()
        {
            var g = this, po = this.po, p = this.options;
            return $("> .l-menu-item", g.menu.items).length;
        },
        addItem: function (item, menu)
        {
            var g = this, po = this.po, p = this.options;
            if (!item) return;
            if (menu == undefined) menu = g.menu;

            if (item.line)
            {
                menu.items.append('<div class="l-menu-item-line"></div>');
                return;
            }
            var ditem = $('<div class="l-menu-item"><div class="l-menu-item-text"></div> </div>');
            var itemcount = $("> .l-menu-item", menu.items).length;
            menu.items.append(ditem);
            item.id && ditem.attr("menuitemid", item.id);
            item.text && $(">.l-menu-item-text:first", ditem).html(item.text);
            item.icon && ditem.prepend('<div class="l-menu-item-icon l-icon-' + item.icon + '"></div>');
            if (item.disable || item.disabled)
                ditem.addClass("l-menu-item-disable");
            if (item.children)
            {
                if (ditem.attr("menuitemid") == undefined) ditem.attr("menuitemid", new Date().getTime());
                ditem.append('<div class="l-menu-item-arrow"></div>');
                var newmenu = g.createMenu(ditem.attr("menuitemid"));
                g.menus[ditem.attr("menuitemid")] = newmenu;
                newmenu.width(p.width);
                newmenu.hover(null, function ()
                {
                    if (!newmenu.showedSubMenu)
                        g.hide(newmenu);
                });
                $(item.children).each(function ()
                {
                    g.addItem(this, newmenu);
                });
            }
            item.click && ditem.click(function ()
            {
                if ($(this).hasClass("l-menu-item-disable")) return;
                item.click(item, itemcount);
            });
            item.dblclick && ditem.dblclick(function ()
            {
                if ($(this).hasClass("l-menu-item-disable")) return;
                item.dblclick(item, itemcount);
            });

            var menuover = $("> .l-menu-over:first", menu);
            ditem.hover(function ()
            {
                if ($(this).hasClass("l-menu-item-disable")) return;
                var itemtop = $(this).offset().top;
                var top = itemtop - menu.offset().top;
                menuover.css({ top: top });
                g.hideAllSubMenu(menu);
                if (item.children)
                {
                    var meniitemid = $(this).attr("menuitemid");
                    if (!meniitemid) return;
                    if (g.menus[meniitemid])
                    {
                        g.show({ top: itemtop, left: $(this).offset().left + $(this).width() - 5 }, g.menus[meniitemid]);
                        menu.showedSubMenu = true;
                    }
                }
            }, function ()
            {
                if ($(this).hasClass("l-menu-item-disable")) return;
                var meniitemid = $(this).attr("menuitemid");
                if (item.children)
                {
                    var meniitemid = $(this).attr("menuitemid");
                    if (!meniitemid) return;
                };
            });
        },
        hideAllSubMenu: function (menu)
        {
            var g = this, po = this.po, p = this.options;
            if (menu == undefined) menu = g.menu;
            $("> .l-menu-item", menu.items).each(function ()
            {
                if ($("> .l-menu-item-arrow", this).length > 0)
                {
                    var meniitemid = $(this).attr("menuitemid");
                    if (!meniitemid) return;
                    g.menus[meniitemid] && g.hide(g.menus[meniitemid]);
                }
            });
            menu.showedSubMenu = false;
        },
        createMenu: function (parentMenuItemID)
        {
            var g = this, po = this.po, p = this.options;
            g.menus = g.menus || {};
            var menu = $('<div class="l-menu" style="display:none"><div class="l-menu-yline"></div><div class="l-menu-over"><div class="l-menu-over-l"></div> <div class="l-menu-over-r"></div></div><div class="l-menu-inner"></div></div>');
            parentMenuItemID && menu.attr("parentmenuitemid", parentMenuItemID);
            menu.items = $("> .l-menu-inner:first", menu);
            menu.appendTo('body');
            if (p.shadow)
            {
                menu.shadow = $('<div class="l-menu-shadow"></div>').insertAfter(menu);
                g.updateShadow(menu);
            }
            menu.hover(null, function ()
            {
                if (!menu.showedSubMenu)
                    $("> .l-menu-over:first", menu).css({ top: -24 });
            });
            return menu;
        }
    };
    //旧写法保留
    $.ligerManagers.Menu.prototype.setEnable = $.ligerManagers.Menu.prototype.setEnabled;
    $.ligerManagers.Menu.prototype.setDisable = $.ligerManagers.Menu.prototype.setDisabled;
    $.ligerMenu = function (p)
    {
        p = $.extend({}, $.ligerDefaults.Menu, p || {});
        var po = {};
        var g = new $.ligerManagers.Menu(p, po);
        g.menu = g.createMenu();
        g.menu.css({ top: p.top, left: p.left, width: p.width });
        p.items && $(p.items).each(function (i, item)
        {
            g.addItem(item);
        });

        $.ligerui.addManager(g.menu[0], g);
        return g;
    };
    $(document).click(function ()
    {
        $(".l-menu,.l-menu-shadow").hide();
    });
})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
* Depend on:
* 1,LigerMenu
*/
(function ($)
{
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager)
    {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr)
    {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.fn.ligerGetMenuBarManager = function ()
    {
        return $.ligerui.getManager(this);
    };

    //default
    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.MenuBar = {};

    //MenuBar manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.MenuBar = function (options, po)
    {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.MenuBar.prototype = {
        addItem: function (item)
        {
            var po = this.po, g = this, p = this.options;
            var ditem = $('<div class="l-menubar-item l-panel-btn"><span></span><div class="l-panel-btn-l"></div><div class="l-panel-btn-r"></div><div class="l-menubar-item-down"></div></div>');
            g.menubar.append(ditem);
            item.id && ditem.attr("menubarid", item.id);
            item.text && $("span:first", ditem).html(item.text);
            item.disable && ditem.addClass("l-menubar-item-disable");
            item.click && ditem.click(function () { item.click(item); });
            if (item.menu)
            {
                var menu = $.ligerMenu(item.menu);
                ditem.hover(function ()
                {
                    g.actionMenu && g.actionMenu.hide();
                    var left = $(this).offset().left;
                    var top = $(this).offset().top + $(this).height();
                    menu.show({ top: top, left: left });
                    g.actionMenu = menu;
                    $(this).addClass("l-panel-btn-over l-panel-btn-selected").siblings(".l-menubar-item").removeClass("l-panel-btn-selected");
                }, function ()
                {
                    $(this).removeClass("l-panel-btn-over");
                });
            }
            else
            {
                ditem.hover(function ()
                {
                    $(this).addClass("l-panel-btn-over");
                }, function ()
                {
                    $(this).removeClass("l-panel-btn-over");
                });
                $(".l-menubar-item-down", ditem).remove();
            }

        }
    };
    $.fn.ligerMenuBar = function (options)
    {
        this.each(function ()
        {
            if (this.applyligerui) return;
            var p = $.extend({}, options || {});
            var po = {};
            var g = new $.ligerManagers.MenuBar(p, po);
            g.menubar = $(this);
            if (!g.menubar.hasClass("l-menubar")) g.menubar.addClass("l-menubar");
            if (p && p.items)
            {
                $(p.items).each(function (i, item)
                {
                    g.addItem(item);
                });
            }
            $(document).click(function ()
            {
                $(".l-panel-btn-selected", g.menubar).removeClass("l-panel-btn-selected");
            });
            $.ligerui.addManager(this, g);
        });
        return $.ligerui.getManager(this);
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.0.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($)
{
    ///	<param name="$" type="jQuery"></param>
    $.ligerMessageBox = {};
    $.ligerMessageBox.show = function (p)
    {
        p = p || {};
        p.isDrag == undefined && (p.isDrag = true);
        var messageBoxHTML = "";
        messageBoxHTML += '<div class="l-messagebox">';
        messageBoxHTML += '        <div class="l-messagebox-lt"></div><div class="l-messagebox-rt"></div>';
        messageBoxHTML += '        <div class="l-messagebox-l"></div><div class="l-messagebox-r"></div> ';
        messageBoxHTML += '        <div class="l-messagebox-image"></div>';
        messageBoxHTML += '        <div class="l-messagebox-title">';
        messageBoxHTML += '            <div class="l-messagebox-title-inner"></div>';
        messageBoxHTML += '            <div class="l-messagebox-close"></div>';
        messageBoxHTML += '        </div>';
        messageBoxHTML += '        <div class="l-messagebox-content">';
        messageBoxHTML += '        </div>';
        messageBoxHTML += '        <div class="l-messagebox-buttons"><div class="l-messagebox-buttons-inner">';
        messageBoxHTML += '        </div></div>';
        messageBoxHTML += '    </div>';
        var g = {
            applyWindowMask: function ()
            {
                $(".l-window-mask").remove();
                $("<div class='l-window-mask' style='display: block;'></div>").appendTo($("body"));
            },
            removeWindowMask: function ()
            {
                $(".l-window-mask").remove();
            },
            applyDrag: function ()
            {
                if (p.isDrag && $.fn.ligerDrag)
                    messageBox.ligerDrag({ handler: '.l-messagebox-title' });
            },
            setImage: function ()
            {
                if (p.type)
                {
                    if (p.type == 'success' || p.type == 'donne')
                    {
                        $(".l-messagebox-image", messageBox).addClass("l-messagebox-image-donne").show();
                        $(".l-messagebox-content", messageBox).css({ paddingLeft: 64, paddingBottom: 30 });
                    }
                    else if (p.type == 'error')
                    {
                        $(".l-messagebox-image", messageBox).addClass("l-messagebox-image-error").show();
                        $(".l-messagebox-content", messageBox).css({ paddingLeft: 64, paddingBottom: 30 });
                    }
                    else if (p.type == 'warn')
                    {
                        $(".l-messagebox-image", messageBox).addClass("l-messagebox-image-warn").show();
                        $(".l-messagebox-content", messageBox).css({ paddingLeft: 64, paddingBottom: 30 });
                    }
                    else if (p.type == 'question')
                    {
                        $(".l-messagebox-image", messageBox).addClass("l-messagebox-image-question").show();
                        $(".l-messagebox-content", messageBox).css({ paddingLeft: 64, paddingBottom: 40 });
                    }
                }
            }
        };
        var messageBox = $(messageBoxHTML);
        $('body').append(messageBox);
        messageBox.close = function ()
        {
            g.removeWindowMask();
            messageBox.remove();
        };
        //设置参数属性
        p.width && messageBox.width(p.width);
        p.title && $(".l-messagebox-title-inner", messageBox).html(p.title);
        p.content && $(".l-messagebox-content", messageBox).html(p.content);
        if (p.buttons)
        {
            $(p.buttons).each(function (i, item)
            {
                var btn = $('<div class="l-messagebox-btn"><div class="l-messagebox-btn-l"></div><div class="l-messagebox-btn-r"></div><div class="l-messagebox-btn-inner"></div></div>');
                $(".l-messagebox-btn-inner", btn).html(item.text);
                $(".l-messagebox-buttons-inner", messageBox).append(btn);
                item.width && btn.width(item.width);
                item.onclick && btn.click(function () { item.onclick(item, i, messageBox) });
            });
            $(".l-messagebox-buttons-inner", messageBox).append("<div class='l-clear'></div>");
        }
        var boxWidth = messageBox.width();
        var sumBtnWidth = 0;
        $(".l-messagebox-buttons-inner .l-messagebox-btn", messageBox).each(function ()
        {
            sumBtnWidth += $(this).width();
        });
        $(".l-messagebox-buttons-inner", messageBox).css({ marginLeft: parseInt((boxWidth - sumBtnWidth) * 0.5) });
        //设置背景、拖动支持 和设置图片
        g.applyWindowMask();
        g.applyDrag();
        g.setImage();
        //设置事件
        $(".l-messagebox-btn", messageBox).hover(function ()
        {
            $(this).addClass("l-messagebox-btn-over");
            $(".l-messagebox-btn-l", this).addClass("l-messagebox-btn-l-over");
            $(".l-messagebox-btn-r", this).addClass("l-messagebox-btn-r-over");
        }, function ()
        {
            $(this).removeClass("l-messagebox-btn-over");
            $(".l-messagebox-btn-l", this).removeClass("l-messagebox-btn-l-over");
            $(".l-messagebox-btn-r", this).removeClass("l-messagebox-btn-r-over");
        });
        $(".l-messagebox-close", messageBox).hover(function ()
        {
            $(this).addClass("l-messagebox-close-over");
        }, function ()
        {
            $(this).removeClass("l-messagebox-close-over");
        }).click(function ()
        {
            messageBox.close();
        });
    };
    $.ligerMessageBox.alert = function (title, content, type, onBtnClick)
    {
        title = title || "";
        content = content || title;
        var g = {
            onclick: function (item, index, messageBox)
            {
                messageBox.close();
                if (onBtnClick)
                    onBtnClick(item, index, messageBox);
            }
        };
        p = {
            title: title,
            content: content,
            buttons: [{ text: '确定', onclick: g.onclick}]
        };
        if (type) p.type = type;
        $.ligerMessageBox.show(p);
    };
    $.ligerMessageBox.confirm = function (title, content, callback)
    {
        var g = {
            onclick: function (item, index, messageBox)
            {
                messageBox.close();
                if (callback)
                {
                    callback(index == 0);
                }
            }
        };
        p = {
            type: 'question',
            title: title,
            content: content,
            buttons: [{ text: '是', onclick: g.onclick }, { text: '否', onclick: g.onclick}]
        };
        $.ligerMessageBox.show(p);
    };
    $.ligerMessageBox.success = function (title, content, onBtnClick)
    {
        $.ligerMessageBox.alert(title, content, 'success', onBtnClick);
    };
    $.ligerMessageBox.error = function (title, content, onBtnClick)
    {
        $.ligerMessageBox.alert(title, content, 'error', onBtnClick);
    };
    $.ligerMessageBox.warn = function (title, content, onBtnClick)
    {
        $.ligerMessageBox.alert(title, content, 'warn', onBtnClick);
    };
    $.ligerMessageBox.question = function (title, content)
    {
        $.ligerMessageBox.alert(title, content, 'question');
    };



    function preLoadImage()
    {
        var imagePath = '../lib/ligerUI/skins/Aqua/images/';
        var imageArr = ['box/box-btn-done.gif', 'box/box-btn-error.gif', 'box/box-btn-l.gif', 'box/box-btn-l-over.gif',
             'box/box-btn-question.gif', 'box/box-btn-over.gif', 'box/box-righttop.gif', 'box/box-lefttop.gif', 'box/box-top.gif', 'box/box-btn-r.gif', 'box/box-btn-r-over.gif', 'box/box-btn-warn.gif', 'box/box-close.gif', 'box/box-close-over.gif', 'box/tabs-item-left-bg.gif'];
        for (i = 0; i < imageArr.length; i++)
        {
            new Image().src = imagePath + imageArr[i];
        }
    }
    //preLoadImage();
})(jQuery);/**
* jQuery ligerUI 1.0.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($)
{
    $.fn.ligerNoSelect = function (p)
    {
        if (p == null)
            prevent = true;
        else
            prevent = p;
        if (prevent)
        {
            return this.each(function ()
            {
                if ($.browser.msie || $.browser.safari) $(this).bind('selectstart', function () { return false; });
                else if ($.browser.mozilla)
                {
                    $(this).css('MozUserSelect', 'none');
                    $('body').trigger('focus');
                }
                else if ($.browser.opera) $(this).bind('mousedown', function () { return false; });
                else $(this).attr('unselectable', 'on');
            });
        } else
        {
            return this.each(function ()
            {
                if ($.browser.msie || $.browser.safari) $(this).unbind('selectstart');
                else if ($.browser.mozilla) $(this).css('MozUserSelect', 'inherit');
                else if ($.browser.opera) $(this).unbind('mousedown');
                else $(this).removeAttr('unselectable', 'on');
            });
        }
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.2.3
* 
* http://ligerui.com
*  
* Author daomi 2014 [ gd_star@163.com ] 
* 
*/
(function ($)
{
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager) {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr) {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.fn.ligerGetPanelManager = function () {
        return $.ligerui.getManager(this);
    };

    $.ligerDefaults = $.ligerDefaults || {};

    $.ligerDefaults.Panel = {
        width: 400,
        height : 300,
        title: 'Panel',
        content: null,      //内容
        content_id:null,
        url: null,          //远程内容Url
        frameName: null,     //创建iframe时 作为iframe的name和id 
        data: null,          //可用于传递到iframe的数据
        showClose: false,    //是否显示关闭按钮
        showToggle: true,    //是否显示收缩按钮 
        icon: null,          //左侧按钮
        onClose:null,       //关闭前事件
        onClosed:null,      //关闭事件
        onLoaded:null           //url模式 加载完事件
    };
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Panel = function (options, po) {
        this.options = options;
        this.po = po;
    };
    
    $.ligerManagers.Panel.prototype = {
        getType: function ()
        {
            return 'Panel';
        },
        idPrev: function ()
        {
            return 'Panel';
        },
        extendMethods: function ()
        {
            return $.ligerMethos.Panel;
        },
        init: function ()
        {
            var g = this, p = this.options;
            $.ligerui.controls.Panel.base._init.call(this);
            p.content = p.content || $(g.element).html(); 
        },
        render: function ()
        {
            var g = this, p = this.options;
            g.panel = $(this);
            //g.panel = $(g.element).addClass("l-panel").html("");
            g.panel.append('<div class="l-panel-header"><span></span><div class="icons"></div></div><div class="l-panel-content"></div>');
             
           // g.set(p);
 
            g.panel.bind("click.panel", function (e)
            { 
                var obj = (e.target || e.srcElement), jobj = $(obj);
                if (jobj.hasClass("l-panel-header-toggle"))
                {
                    g.toggle();
                } else if (jobj.hasClass("l-panel-header-close"))
                {
                    g.close();
                }
            });
            alert(g.panel.length);
        },
        collapse: function ()
        {
            var g = this, p = this.options;
            var toggle = g.panel.find(".l-panel-header .l-panel-header-toggle:first");
            if (toggle.hasClass("l-panel-header-toggle-hide")) return;
            g.toggle();
        },
        expand: function ()
        {
            var g = this, p = this.options;
            var toggle = g.panel.find(".l-panel-header .l-panel-header-toggle:first");
            if (!toggle.hasClass("l-panel-header-toggle-hide")) return;
            g.toggle();
        },
        toggle : function()
        {
            var g = this, p = this.options;
            var toggle = g.panel.find(".l-panel-header .l-panel-header-toggle:first");
            if (toggle.hasClass("l-panel-header-toggle-hide"))
            {
                toggle.removeClass("l-panel-header-toggle-hide");
            } else
            {
                toggle.addClass("l-panel-header-toggle-hide");
            }
            g.panel.find(".l-panel-content:first").slideToggle("normal");
        },
        setShowToggle:function(v)
        {
            var g = this, p = this.options;
            var header = g.panel.find(".l-panel-header:first");
            if (v)
            {
                var toggle = $("<div class='l-panel-header-toggle'></div>"); 
                toggle.appendTo(header.find(".icons")); 
            } else
            {
                header.find(".l-panel-header-toggle").remove();
            }
        },
        setContent: function (v)
        {
            var g = this, p = this.options;
            var content = g.panel.find(".l-panel-content:first");
            if (v)
            {
                content.html(v);
            }
        },
        setUrl: function (url)
        {
            var g = this, p = this.options;
            var content = g.panel.find(".l-panel-content:first");
            if (url)
            {
                g.jiframe = $("<iframe frameborder='0'></iframe>");
                var framename = p.frameName ? p.frameName : "ligerpanel" + new Date().getTime();
                g.jiframe.attr("name", framename);
                g.jiframe.attr("id", framename);
                content.prepend(g.jiframe); 

                setTimeout(function ()
                {
                    if (content.find(".l-panel-loading:first").length == 0)
                        content.append("<div class='l-panel-loading' style='display:block;'></div>");
                    var iframeloading = $(".l-panel-loading:first", content);
                    g.jiframe[0].panel = g;//增加窗口对panel对象的引用
                    /*
                    可以在子窗口这样使用：
                    var panel = frameElement.panel;
                    var panelData = dialog.get('data');//获取data参数
                    panel.set('title','新标题'); //设置标题
                    panel.close();//关闭dialog 
                    */
                    g.jiframe.attr("src", p.url);
                    g.frame = window.frames[g.jiframe.attr("name")];
                }, 0); 
            }
        },
        setShowClose: function (v)
        {
            var g = this, p = this.options;
            var header = g.panel.find(".l-panel-header:first");
            if (v)
            {
                var btn = $("<div class='l-panel-header-close'></div>"); 
                btn.appendTo(header.find(".icons"));
            } else
            {
                header.find(".l-panel-header-close").remove();
            }
        },
        close:function()
        {
            var g = this, p = this.options;
            if (g.trigger('close') == false) return;
            g.panel.remove();
            g.trigger('closed');
        }, 
        show: function ()
        {
            this.panel.show();
        },
        setIcon : function(url)
        {
            var g = this;
            if (!url)
            {
                g.panel.removeClass("l-panel-hasicon");
                g.panel.find('img').remove();
            } else
            {
                g.panel.addClass("l-panel-hasicon");
                g.panel.append('<img src="' + url + '" />');
            }
        }, 
        setWidth: function (value)
        { 
            value && this.panel.width(value);
        },
        setHeight: function (value)
        { 
            var g = this, p = this.options;
            var header = g.panel.find(".l-panel-header:first");
            this.panel.find(".l-panel-content:first").height(value - header.height());
        },
        setTitle: function (value)
        {
            this.panel.find(".l-panel-header span:first").text(value);
        } 
    };

    $.fn.ligerPanel = function (p) {
        this.each(function () {
            p = $.extend({}, $.ligerDefaults.Panel, p || {});
            var po = {};
            var g = new $.ligerManagers.Panel(p, po);

            g.panel = $(this);
            var text = $(this).text()||p.content;
            $(this).text("");
            //g.panel = $(g.element).addClass("l-panel").html("");
            g.panel.append('<div class="l-panel-header"><table style="width:100%;"><tr><td><div class="l-panel-header-drag"><span></span></div></td><td style="width:22px;"><div class="icons"></div></td></tr></table></div><div class="l-panel-content"></div>');

            g.setContent(text);
            g.panel.width(p.width);
            $(".l-panel-content", g.panel).height(p.height);
            p.content_id && $(".l-panel-content", g.panel).attr("id",p.content_id);
            g.setTitle(p.title);
            g.setShowToggle(true);

            p.url && g.setUrl(p.url);

            g.panel.bind("click.panel", function (e) {
                var obj = (e.target || e.srcElement), jobj = $(obj);
                if (jobj.hasClass("l-panel-header-toggle")) {
                    g.toggle();
                } else if (jobj.hasClass("l-panel-header-close")) {
                    g.close();
                }
            });

            $.ligerui.addManager(this, g);
        })
        return $.ligerui.getManager(this);
    }


})(jQuery);﻿/**
* jQuery ligerUI 1.2.3
* 
* http://ligerui.com
*  
* Author daomi 2014 [ gd_star@163.com ] 
* 
*/
(function ($) {

    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager) {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr) {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.fn.ligerGetPanelManager = function () {
        return $.ligerui.getManager(this);
    };

    $.ligerDefaults = $.ligerDefaults || {};

    $.ligerDefaults.Portal = {
        width: null,
        /*行元素：组件允许以纵向方式分割为几块
        每一块(行)允许自定义N个列(column)
        每一列允许自定义N个Panel(最小元素)
        rows:[
            {columns:[ 
                {
                   width : '50%',
                   panels : [{width:'100%',content:'内容'},{width:'100%',url:@url1}]
                },{
                   width : '50%',
                   panels : [{width:'100%',url:@url2}]
                }
            ]}
        ]
        */
        rows: null,
        /* 列元素： 组件将认为只存在一个row(块),
       这一块 允许自定义N个列(column),结构同上
        */
        columns: null,
        url: null,          //portal结构定义URL   
        method: 'get',                         //获取数据http方式
        parms: null,                         //提交到服务器的参数
        draggable: false,   //是否允许拖拽
        onLoaded: null       //url模式 加载完事件
    };
    $.ligerDefaults.Portal_rows = {
        width: null,
        height: null
    };
    $.ligerDefaults.Portal_columns = {
        width: null,
        height: null
    };
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Portal = function (options, po) {
        this.options = options;
        this.po = po;
    };

    $.ligerManagers.Portal.prototype={
        __getType: function () {
            return 'Portal';
        },
        __idPrev: function () {
            return 'Portal';
        },
        _extendMethods: function () {
            return $.ligerMethos.Portal;
        },
        _init: function () {
            var g = this, p = this.options;
            $.ligerui.controls.Portal.base._init.call(this);
            if ($(">div", g.element).length) //如果已经定义了DIV子元素,那么这些元素将会转换为columns,这里暂时保存到tempInitPanels
            {
                p.columns = [];
                $(">div", g.element).each(function (i, jpanel) {
                    p.columns[i] = {
                        panels: []
                    };
                });

                g.tempInitPanels = $("<div></div>");
                $(">div", g.element).appendTo(g.tempInitPanels);
            }
            if (!p.rows && p.columns) {
                p.rows = [{
                    columns: p.columns
                }];
            }
        },
        _render: function () {
            var g = this, p = this.options;

            g.portal = $(g.element).addClass("l-portal").html("");

            g.set(p);

        },
        _setRows: function (rows) {
            var g = this, p = this.options;
            g.rows = [];
            if (rows && rows.length) {
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    var jrow = $('<div class="l-row"></div>').appendTo(g.portal);
                    g.rows[i] = g._renderRow({
                        row: row,
                        rowIndex: i,
                        jrow: jrow
                    });
                    jrow.append('<div class="l-clear"></div>');
                }
            }
        },
        _renderRow: function (e) {
            var row = e.row, rowIndex = e.rowIndex, jrow = e.jrow;
            var g = this, p = this.options;
            var rowObj = {
                element: jrow[0]
            };
            if (row.width) jrow.width(row.width);
            if (row.height) jrow.height(row.height);
            if (row.columns) rowObj.columns = [];
            if (row.columns && row.columns.length) {
                for (var i = 0; i < row.columns.length; i++) {
                    var column = row.columns[i];
                    var jcolumn = $('<div class="l-column"></div>').appendTo(jrow);
                    rowObj.columns[i] = g._renderColumn({
                        column: column,
                        columnIndex: i,
                        jcolumn: jcolumn,
                        rowIndex: rowIndex
                    });
                }
            }
            return rowObj;
        },
        remove: function (e) {
            var g = this, p = this.options;
            var rowIndex = e.rowIndex, columnIndex = e.columnIndex, index = e.index;
            if (index == null) index = -1;
            if (index >= 0 && g.rows[rowIndex] && g.rows[rowIndex].columns && g.rows[rowIndex].columns[columnIndex] && g.rows[rowIndex].columns[columnIndex].panels) {
                var panel = g.rows[rowIndex].columns[columnIndex].panels[index];
                panel && panel.close();
                g._updatePortal();
            }
        },
        add: function (e) {
            var g = this, p = this.options;
            var rowIndex = e.rowIndex, columnIndex = e.columnIndex, index = e.index, panel = e.panel;
            if (index == null) index = -1;
            if (!(g.rows[rowIndex] && g.rows[rowIndex].columns && g.rows[rowIndex].columns[columnIndex])) return;
            var gColumn = g.rows[rowIndex].columns[columnIndex], pColumn = p.rows[rowIndex].columns[columnIndex], ligerPanel, jcolumn = $(gColumn.element);
            pColumn.panels = pColumn.panels || [];
            gColumn.panels = gColumn.panels || [];
            pColumn.panels.splice(index, 0, panel);
            if (index < 0) {
                var jpanel = $('<div></div>').insertBefore(gColumn.jplace);
                ligerPanel = jpanel.ligerPanel(panel);
            } else if (gColumn.panels[index]) {
                var jpanel = $('<div></div>').insertBefore(gColumn.panels[index].panel);
                ligerPanel = jpanel.ligerPanel(panel);
            }
            if (ligerPanel) {
                ligerPanel.bind('closed', g._createPanelClosed());
                g.setPanelEvent({
                    panel: ligerPanel
                });
                gColumn.panels.splice(index, 0, ligerPanel);
            }
            g._updatePortal();
        },
        _createPanelClosed: function () {
            var g = this, p = this.options;
            return function () {
                var panel = this;//ligerPanel对象
                var panels = g.getPanels();
                var rowIndex, columnIndex, index;
                $(panels).each(function () {
                    if (this.panel == panel) {
                        rowIndex = this.rowIndex;
                        columnIndex = this.columnIndex;
                        index = this.index;
                    }
                });
                p.rows[rowIndex].columns[columnIndex].panels.splice(index, 1);
                g.rows[rowIndex].columns[columnIndex].panels.splice(index, 1);
            };
        },
        _renderColumn: function (e) {
            var column = e.column, columnIndex = e.columnIndex, jcolumn = e.jcolumn;
            var rowIndex = e.rowIndex;
            var g = this, p = this.options;
            var columnObj = {
                element: jcolumn[0]
            };
            if (column.width) jcolumn.width(column.width);
            if (column.height) jcolumn.height(column.height);
            if (column.panels) columnObj.panels = [];
            if (column.panels && column.panels.length) {
                for (var i = 0; i < column.panels.length; i++) {
                    var panel = column.panels[i];
                    var jpanel = $('<div class="l-panel"></div>').appendTo(jcolumn);
                    columnObj.panels[i] = jpanel.ligerPanel(panel);
                    //columnObj.panels[i].bind('closed', g._createPanelClosed());
                    g.setPanelEvent({
                        panel: columnObj.panels[i]
                    });
                }
            } else if (g.tempInitPanels) {

                var tempPanel = g.tempInitPanels.find(">div:eq(" + columnIndex + ")");
                if (tempPanel.length) {
                    columnObj.panels = [];
                    var panelOptions = {};
                    var jelement = tempPanel.clone();
                    if (liger.inject && liger.inject.getOptions) {
                        panelOptions = liger.inject.getOptions({
                            jelement: jelement,
                            defaults: $.ligerDefaults.Panel,
                            config: liger.inject.config.Panel
                        });
                    }
                    columnObj.panels[0] = jelement.appendTo(jcolumn).ligerPanel(panelOptions);
                    columnObj.panels[0].bind('closed', g._createPanelClosed());
                    g.setPanelEvent({
                        panel: columnObj.panels[0]
                    });
                }
            }
            columnObj.jplace = $('<div class="l-column-place"></div>').appendTo(jcolumn);
            return columnObj;
        },
        setPanelEvent: function (e) {
            //panel:ligerui对象,jpanel:jQuery dom对象
            var panel = e.panel, jpanel = panel.panel;
            var g = this, p = this.options;
  
            //拖拽支持
            if ($.fn.ligerDrag && p.draggable) {
                jpanel.addClass("l-panel-draggable").ligerDrag({
                    proxy: false, revert: true,
                    handler: ".l-panel-header-drag",
                    onRendered: function () {
                    },
                    onStartDrag: function (current, e) {          //alert(JSON.stringify(jpanel));
                        g.portal.find(">.l-row").addClass("l-row-dragging");
                        this.jplace = $('<div class="l-panel-place"></div>');
                        this.jplace.height(jpanel.height());
                        jpanel.width(jpanel.width());
                        jpanel.addClass("l-panel-dragging");
                        jpanel.css("position", "absolute");
                        jpanel.after(this.jplace);
                        g._updatePortal();
                    },
                    onDrag: function (current, e) {
                        var pageX = e.pageX || e.screenX, pageY = e.pageY || e.screenY;
                        var height = jpanel.height(), width = jpanel.width(), offset = jpanel.offset();
                        var centerX = offset.left + width / 2, centerY = offset.top + 10;
                        var panels = g.getPanels(), emptyColumns = g.getEmptyColumns();
                        var result = getPositionIn(panels, emptyColumns, centerX, centerY);
                        if (result) {
                            //判断是否跟上次匹配的位置一致
                            if (this.placeStatus) {
                                if (this.placeStatus.panel && result.panel) {
                                    if (this.placeStatus.panel.rowIndex == result.panel.rowIndex &&
                                this.placeStatus.panel.columnIndex == result.panel.columnIndex &&
                                this.placeStatus.panel.index == result.panel.index &&
                                this.placeStatus.position == result.position) {
                                        return;
                                    }
                                }
                                if (this.placeStatus.column && result.column) //定位到空元素行
                                {
                                    if (this.placeStatus.column.rowIndex == result.column.rowIndex && this.placeStatus.column.columnIndex == result.column.columnIndex && this.placeStatus.position == result.position) {
                                        return;
                                    }
                                }
                            }
                            if (result.position == "top") {
                                this.jplace.insertBefore(result.panel ? result.panel.jpanel : result.column.jplace);
                                this.savedPosition = result.panel ? result.panel : result.column
                                this.savedPosition.inTop = true;
                            } else if (result.position == "bottom") {
                                this.jplace.insertAfter(result.panel.jpanel);
                                this.savedPosition = result.panel;
                                this.savedPosition.inTop = false;
                            }
                            this.placeStatus = result;
                        }
                        else//没有匹配到
                        {
                            this.placeStatus = null;
                        }

                        //从指定的元素集合匹配位置
                        function getPositionIn(panels, columns, x, y) {
                            for (i = 0, l = panels.length; i < l; i++) {
                                var o = panels[i];
                                if (o.panel == panel) //如果是本身
                                {
                                    continue;
                                }
                                var r = positionIn(o, null, x, y);
                                if (r) return r;
                            }
                            for (i = 0, l = columns.length; i < l; i++) {
                                var column = columns[i];
                                var r = positionIn(null, column, x, y);
                                if (r) return r;
                            }
                            return null;
                        }
                        //坐标在目标区域范围内 x,y为panel标题栏中间的位置
                        function positionIn(panel, column, x, y) {
                            var jelement = panel ? panel.jpanel : column.jplace;
                            if (!jelement) return null;
                            var height = jelement.height(), width = jelement.width();
                            var left = jelement.offset().left, top = jelement.offset().top;
                            var diff = 3;
                            if (x > left - diff && x < left + width + diff) {
                                if (y > top - diff && y < top + height / 2 + diff) {
                                    return {
                                        panel: panel,
                                        column: column,
                                        position: "top"
                                    };
                                }
                                if (y > top + height / 2 - diff && y < top + height + diff) {
                                    return {
                                        panel: panel,
                                        column: column,
                                        position: panel ? "bottom" : "top"
                                    };
                                }
                            }
                            return null;
                        }
                    },
                    onStopDrag: function (current, e) {
                        g.portal.find(">.l-row").removeClass("l-row-dragging");
                        //alert(JSON.stringify($(panel.panel).width()));
                        $(panel.panel).width(this.jplace.width());
                        jpanel.removeClass("l-panel-dragging");
                        //将jpanel替换到jplace的位置 
                        if (this.jplace) {
                            jpanel.css({
                                "position": "relative",
                                "left": null,
                                "top": null
                            });
                            jpanel.insertAfter(this.jplace);
                            g.portal.find(">.l-row > .l-column >.l-panel-place").remove();

                            if (this.savedPosition) {
                                var panels = g.getPanels();
                                var rowIndex, columnIndex, index;
                                $(panels).each(function () {
                                    if (this.panel == panel) {
                                        rowIndex = this.rowIndex;
                                        columnIndex = this.columnIndex;
                                        index = this.index;
                                    }
                                });
                                var oldPanelOptions = p.rows[rowIndex].columns[columnIndex].panels[index];
                                var oldPanel = g.rows[rowIndex].columns[columnIndex].panels[index];
                                p.rows[rowIndex].columns[columnIndex].panels.splice(index, 1);
                                g.rows[rowIndex].columns[columnIndex].panels.splice(index, 1);

                                if (this.savedPosition.panel) {

                                    p.rows[this.savedPosition.rowIndex].columns[this.savedPosition.columnIndex].panels.splice(this.savedPosition.index + this.savedPosition.inTop ? -1 : 0, 0, oldPanelOptions);
                                    g.rows[this.savedPosition.rowIndex].columns[this.savedPosition.columnIndex].panels.splice(this.savedPosition.index + this.savedPosition.inTop ? -1 : 0, 0, oldPanel);
                                } else {
                                    p.rows[this.savedPosition.rowIndex].columns[this.savedPosition.columnIndex].panels = [oldPanelOptions];
                                    g.rows[this.savedPosition.rowIndex].columns[this.savedPosition.columnIndex].panels = [oldPanel];
                                }
                            }
                        }
                        g._updatePortal();

                        return false;
                    }
                });
            }

        },
        _updatePortal: function () {
            var g = this, p = this.options;
            $(g.rows).each(function (rowIndex) {
                $(this.columns).each(function (columnIndex) {
                    if (this.panels && this.panels.length) {
                        $(this.element).removeClass("l-column-empty");
                    } else {
                        $(this.element).addClass("l-column-empty");
                    }
                    
                });
            });
        },
        getPanels: function () {
            var g = this, p = this.options;
            var panels = [];
            $(g.rows).each(function (rowIndex) {
                $(this.columns).each(function (columnIndex) {
                    $(this.panels).each(function (index) {
                        panels.push({
                            rowIndex: rowIndex,
                            columnIndex: columnIndex,
                            index: index,
                            panel: this,
                            jpanel: this.panel
                        });
                    });
                });
            });
            return panels;
        },
        getPanel: function (e) {
            var g = this, p = this.options;
            e = $.extend({
                rowIndex: 0,
                columnIndex: 0,
                index: 0
            }, e);
            var panel = null;
            $(g.rows).each(function (rowIndex) {
                $(this.columns).each(function (columnIndex) {
                    $(this.panels).each(function (index) {
                        if (panel) return;
                        if (rowIndex == e.rowIndex && columnIndex == e.columnIndex && index == e.index) {
                            panel = this;
                        }
                    });
                });
            });
            return panel;
        },
        getEmptyColumns: function () {
            var g = this, p = this.options;
            var columns = [];
            $(g.rows).each(function (rowIndex) {
                $(this.columns).each(function (columnIndex) {
                    if (!this.panels || !this.panels.length) {
                        columns.push({
                            rowIndex: rowIndex,
                            columnIndex: columnIndex,
                            jplace: this.jplace
                        });
                    }
                });
            });
            return columns;
        },
        _setUrl: function (url) {
            var g = this, p = this.options;
            if (!url) return;
            $.ajax({
                url: url, data: p.parms, type: p.method, dataType: 'json',
                success: function (rows) {
                    g.set('rows', rows);
                }
            });
        },
        _setWidth: function (value) {
            value && this.portal.width(value);
        },
        collapseAll: function () {
            var g = this, p = this.options;
            var panels = g.getPanels();
            $(panels).each(function (i, o) {
                var panel = o.panel;
                panel.collapse();
            });
        },
        expandAll: function () {
            var g = this, p = this.options;
            var panels = g.getPanels();
            $(panels).each(function (i, o) {
                var panel = o.panel;
                panel.expand();
            });
        }
    };

    $.fn.ligerPortal = function (p) {
        this.each(function () {
            p = $.extend({}, $.ligerDefaults.Panel, p || {});
            var po = {};
            var g = new $.ligerManagers.Portal(p, po);

            g.portal = $(this).addClass("l-portal").html("");
            if ($(">div", g.element).length) //如果已经定义了DIV子元素,那么这些元素将会转换为columns,这里暂时保存到tempInitPanels
            {
                p.columns = [];
                $(">div", g.element).each(function (i, jpanel) {
                    p.columns[i] = {
                        panels: []
                    };
                });

                g.tempInitPanels = $("<div></div>");
                $(">div", g.element).appendTo(g.tempInitPanels);
            }
            if (!p.rows && p.columns) {
                p.rows = [{
                    columns: p.columns
                }];
            }

            g._setRows(p.rows);

            $.ligerui.addManager(this, g);
        })
        return $.ligerui.getManager(this);
    }
})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/

(function ($)
{
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager)
    {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr)
    {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;


    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Radio = { disabled: false };

    //CheckBox manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Radio = function (options, po)
    {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.Radio.prototype = {
        setValue: function (value)
        {
            var g = this;
            if (!value)
            {
                g.input[0].checked = false;
                g.link.removeClass('l-radio-checked');
            }
            else
            {
                g.input[0].checked = true;
                g.link.addClass('l-radio-checked');
            }
        },
        getValue: function ()
        {
            return this.input[0].checked;
        },
        setEnabled: function ()
        {
            this.input.attr('disabled', false);
            this.wrapper.removeClass("l-disabled");
            this.options.disabled = false;
        },
        setDisabled: function ()
        {
            this.input.attr('disabled', true);
            this.wrapper.addClass("l-disabled");
            this.options.disabled = true;
        },
        updateStyle: function ()
        {
            if (this.input.attr('disabled'))
            {
                this.wrapper.addClass("l-disabled");
                this.options.disabled = true;
            }
            if (this.input[0].checked)
            {
                this.link.addClass('l-checkbox-checked');
            }
            else
            {
                this.link.removeClass('l-checkbox-checked');
            }
        }
    };

    $.fn.ligerRadio = function (options)
    {
        this.each(function ()
        {
            if (this.applyligerui) return;
            var p = $.extend({}, $.ligerDefaults.CheckBox, options || {});
            var po = {
                doclick: function ()
                {
                    if (g.input.attr('disabled')) { return false; }
                    g.input.trigger('click').trigger('change');
                    var formEle;
                    if (g.input[0].form) formEle = g.input[0].form;
                    else formEle = document;
                    $("input:radio[name=" + g.input[0].name + "]", formEle).not(g.input).trigger("change");
                    return false;
                }
            };
            var g = new $.ligerManagers.Radio(p, po);

            g.input = $(this);
            g.link = $('<a href="javascript:void(0)" class="l-radio"></a>');
            g.wrapper = g.input.addClass('l-hidden').wrap('<div class="l-radio-wrapper"></div>').parent();
            g.wrapper.prepend(g.link);
            g.input.change(function ()
            {
                if (this.checked)
                {
                    g.link.addClass('l-radio-checked');
                }
                else
                {
                    g.link.removeClass('l-radio-checked');
                }
                return true;
            });
            g.link.click(function ()
            {
                po.doclick();
            });
            g.wrapper.hover(function ()
            {
                if (!p.disabled)
                    $(this).addClass("l-over");
            }, function ()
            {
                $(this).removeClass("l-over");
            });
            this.checked && g.link.addClass('l-radio-checked');

            if (this.id)
            {
                $("label[for=" + this.id + "]").click(function ()
                {
                    po.doclick();
                });
            }
            $.ligerui.addManager(this, g);
        });
        return $.ligerui.getManager(this);
    };

})(jQuery);/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($)
{
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager)
    {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr)
    {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;


    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Resizable = {
        handles: 'n, e, s, w, ne, se, sw, nw',
        maxWidth: 2000,
        maxHeight: 2000,
        minWidth: 20,
        minHeight: 20,
        onStartResize: function (e) { },
        onResize: function (e) { },
        onStopResize: function (e) { },
        onEndResize: null
    };

    //Resizable manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Resizable = function (options, po)
    {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.Resizable.prototype = {};


    $.fn.ligerResizable = function (options)
    {
        this.each(function ()
        {
            var p = $.extend({}, $.ligerDefaults.Resizable, options || {});
            var po = {
                init: function ()
                {
                    g.target.append('<div class="l-resizable"></div>');
                    //add handler dom elements 
                    var handles = p.handles.split(',');
                    for (var i = 0; i < handles.length; i++)
                    {
                        switch (handles[i].replace(/(^\s*)|(\s*$)/g, "")) //trim
                        {
                            case "nw":
                                g.target.append('<div class="l-resizable-h-l" direction="nw"></div>');
                                break;
                            case "ne":
                                g.target.append('<div class="l-resizable-h-r" direction="ne"></div>');
                                break;
                            case "n":
                                g.target.append('<div class="l-resizable-h-c" direction="n"></div>');
                                break;
                            case "w":
                                g.target.append('<div class="l-resizable-c-l" direction="w"></div>');
                                break;
                            case "e":
                                g.target.append('<div class="l-resizable-c-r" direction="e"></div>');
                                break;
                            case "sw":
                                g.target.append('<div class="l-resizable-f-l" direction="sw"></div>');
                                break;
                            case "se":
                                g.target.append('<div class="l-resizable-f-r" direction="se"></div>');
                                break;
                            case "s":
                                g.target.append('<div class="l-resizable-f-c" direction="s"></div>');
                                break;
                        }
                    }
                    $("> .l-resizable-h-c , > .l-resizable-f-c", g.target).width(g.target.width());
                    $("> .l-resizable-c-l , > .l-resizable-c-r", g.target).height(g.target.height());
                    g.target.resizable = $("> .l-resizable", g.target);
                },
                start: function (e, dir)
                {
                    if ($.browser.msie || $.browser.safari)
                        $('body').bind('selectstart', function () { return false; }); // ����ѡ��
                    $(".l-window-mask-nobackground").remove();
                    $("<div class='l-window-mask-nobackground' style='display: block;'></div>").appendTo($("body"));
                    g.target.resizable.css({
                        width: g.target.width(),
                        height: g.target.height(),
                        left: 0,
                        top: 0
                    });
                    g.current = {
                        dir: dir,
                        left: g.target.offset().left,
                        top: g.target.offset().top,
                        width: g.target.width(),
                        height: g.target.height()
                    };
                    $(document).bind('mouseup', po.stop);
                    $(document).bind('mousemove', po.drag);
                    g.target.resizable.show();
                    if (p.onStartResize) p.onStartResize(g.current, e);
                },
                drag: function (e)
                {
                    var dir = g.current.dir;
                    var resizableObj = g.target.resizable[0];
                    var width = g.current.width;
                    var height = g.current.height;
                    var moveWidth = (e.pageX || e.screenX) - g.current.left;
                    var moveHeight = (e.pageY || e.clientY) - g.current.top;
                    if (dir.indexOf("e") >= 0) moveWidth -= width;
                    if (dir.indexOf("s") >= 0) moveHeight -= height;
                    if (dir != "n" && dir != "s")
                    {
                        width += (dir.indexOf("w") >= 0) ? -moveWidth : moveWidth;
                    }
                    if (width >= p.minWidth)
                    {
                        if (dir.indexOf("w") >= 0)
                        {
                            resizableObj.style.left = moveWidth + 'px';
                        }
                        if (dir != "n" && dir != "s")
                        {
                            resizableObj.style.width = width + 'px';
                        }
                    }
                    if (dir != "w" && dir != "e")
                    {
                        height += (dir.indexOf("n") >= 0) ? -moveHeight : moveHeight;
                    }
                    if (height >= p.minHeight)
                    {
                        if (dir.indexOf("n") >= 0)
                        {
                            resizableObj.style.top = moveHeight + 'px';
                        }
                        if (dir != "w" && dir != "e")
                        {
                            resizableObj.style.height = height + 'px';
                        }
                    }
                    g.current.newWidth = width;
                    g.current.newHeight = height;
                    g.current.diffTop = parseInt(resizableObj.style.top);
                    if (isNaN(g.current.diffTop)) g.current.diffTop = 0;
                    g.current.diffLeft = parseInt(resizableObj.style.left);
                    if (isNaN(g.current.diffLeft)) g.current.diffLeft = 0;
                    $("body").css("cursor", dir + '-resize');
                    if (p.onResize) p.onResize(g.current, e);
                },
                stop: function (e)
                {
                    if ($.browser.msie || $.browser.safari)
                        $('body').unbind('selectstart');
                    $(".l-window-mask-nobackground").remove();
                    if (!p.onStopResize) po.applyResize();
                    else if (p.onStopResize(g.current, e) != false) po.applyResize();
                    p.onEndResize && p.onEndResize(g.current, e);
                    $("body").css("cursor", "");
                    g.target.resizable.hide();
                    $(document).unbind('mousemove', po.drag);
                    $(document).unbind('mouseup', po.stop);
                },
                applyResize: function ()
                {
                    var top = 0;
                    var left = 0;
                    if (!isNaN(parseInt(g.target.css('top'))))
                        top = parseInt(g.target.css('top'));
                    if (!isNaN(parseInt(g.target.css('left'))))
                        left = parseInt(g.target.css('left'));
                    if (g.current.diffTop != undefined)
                    {
                        if (g.current.diffTop != 0)
                            g.target.css({ top: top + g.current.diffTop });
                        if (g.current.diffLeft != 0)
                            g.target.css({ left: left + g.current.diffLeft });
                        g.target.css({
                            width: g.current.newWidth,
                            height: g.current.newHeight
                        });
                    }
                }
            };
            var g = new $.ligerManagers.Resizable(p, po);
            g.target = $(this);
            po.init();
            $(">div", g.target).mousedown(function (e)
            {
                if (!$(this).attr("direction")) return;
                po.start(e, $(this).attr("direction"));
                return false;
            });
            $.ligerui.addManager(this, g);
        });
        return $.ligerui.getManager(this);
    };
})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($) {
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager) {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr) {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;


    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Spinner = {
        type: 'float',     //类型 float:浮点数 int:整数 time:时间
        isNegative: true, //是否负数
        decimalplace: 2,   //小数位 type=float时起作用
        step: 0.1,         //每次增加的值
        interval: 50,      //间隔，毫秒
        onChangeValue: false,    //改变值事件
        minValue: null,        //最小值
        maxValue: null,         //最大值
        disabled: false
    };

    //Spinner manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Spinner = function (options, po) {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.Spinner.prototype = {
        setValue: function (value) {
            this.inputText.val(value);
        },
        getValue: function () {
            return this.inputText.val();
        },
        setEnabled: function () {
            this.wrapper.removeClass("l-text-disabled");
            this.options.disabled = false;
        },
        setDisabled: function () {
            this.wrapper.addClass("l-text-disabled");
            this.options.disabled = true;
        }
    };


    $.fn.ligerSpinner = function (options) {
        this.each(function () {
            if (this.applyligerui) return;
            var p = $.extend({}, options || {});
            if ($(this).attr("ligerui")) {
                try {
                    var attroptions = $(this).attr("ligerui");
                    if (attroptions.indexOf('{') != 0) attroptions = "{" + attroptions + "}";
                    eval("attroptions = " + attroptions + ";");
                    if (attroptions) p = $.extend({}, attroptions, p || {});
                }
                catch (e) { }
            }
            p = $.extend({}, $.ligerDefaults.Spinner, p || {});
            if (p.type == 'float') {
                p.step = 0.1;
                p.interval = 50;
            } else if (p.type == 'int') {
                p.step = 1;
                p.interval = 100;
            } else if (p.type == 'time') {
                p.step = 1;
                p.interval = 100;
            }
            var po = {
                round: function (v, e) {
                    var t = 1;
                    for (; e > 0; t *= 10, e--);
                    for (; e < 0; t /= 10, e++);
                    return Math.round(v * t) / t;
                },
                isInt: function (str) {
                    var strP = p.isNegative ? /^-?\d+$/ : /^\d+$/;
                    if (!strP.test(str)) return false;
                    if (parseFloat(str) != str) return false;
                    return true;
                },
                isFloat: function (str) {
                    var strP = p.isNegative ? /^-?\d+(\.\d+)?$/ : /^\d+(\.\d+)?$/;
                    if (!strP.test(str)) return false;
                    if (parseFloat(str) != str) return false;
                    return true;
                },
                isTime: function (str) {
                    var a = str.match(/^(\d{1,2}):(\d{1,2})$/);
                    if (a == null) return false;
                    if (a[1] > 24 || a[2] > 60) return false;
                    return true;

                },
                isVerify: function (str) {
                    if (p.type == 'float') {
                        return po.isFloat(str);
                    } else if (p.type == 'int') {
                        return po.isInt(str);
                    } else if (p.type == 'time') {
                        return po.isTime(str);
                    }
                    return false;
                },
                getVerifyValue: function (value) {
                    var newvalue = null;
                    if (p.type == 'float') {
                        newvalue = po.round(value, p.decimalplace);
                    } else if (p.type == 'int') {
                        newvalue = parseInt(value);
                    } else if (p.type == 'time') {
                        newvalue = value;
                    }
                    if (!po.isVerify(newvalue)) {
                        return g.value;
                    } else {
                        return newvalue;
                    }
                },
                isOverValue: function (value) {
                    if (p.minValue != null && p.minValue > value) return true;
                    if (p.maxValue != null && p.maxValue < value) return true;
                    return false;
                },
                getDefaultValue: function () {
                    if (p.type == 'float' || p.type == 'int') { return 0; }
                    else if (p.type == 'time') { return "00:00"; }
                },
                addValue: function (num) {
                    var value = g.inputText.val();
                    value = parseFloat(value) + num;
                    if (po.isOverValue(value)) return;
                    g.inputText.val(value);
                    g.inputText.trigger("change");
                },
                addTime: function (minute) {
                    var value = g.inputText.val();
                    var a = value.match(/^(\d{1,2}):(\d{1,2})$/);
                    newminute = parseInt(a[2]) + minute;
                    if (newminute < 10) newminute = "0" + newminute;
                    value = a[1] + ":" + newminute;
                    if (po.isOverValue(value)) return;
                    g.inputText.val(value);
                    g.inputText.trigger("change");
                },
                uping: function () {
                    if (p.type == 'float' || p.type == 'int') {
                        po.addValue(p.step);
                    } else if (p.type == 'time') {
                        po.addTime(p.step);
                    }
                },
                downing: function () {
                    if (p.type == 'float' || p.type == 'int') {
                        po.addValue(-1 * p.step);
                    } else if (p.type == 'time') {
                        po.addTime(-1 * p.step);
                    }
                },
                isDateTime: function (dateStr) {
                    var r = dateStr.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
                    if (r == null) return false;
                    var d = new Date(r[1], r[3] - 1, r[4]);
                    if (d == "NaN") return false;
                    return (d.getFullYear() == r[1] && (d.getMonth() + 1) == r[3] && d.getDate() == r[4]);
                },
                isLongDateTime: function (dateStr) {
                    var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2})$/;
                    var r = dateStr.match(reg);
                    if (r == null) return false;
                    var d = new Date(r[1], r[3] - 1, r[4], r[5], r[6]);
                    if (d == "NaN") return false;
                    return (d.getFullYear() == r[1] && (d.getMonth() + 1) == r[3] && d.getDate() == r[4] && d.getHours() == r[5] && d.getMinutes() == r[6]);
                }
            };
            var g = new $.ligerManagers.Spinner(p, po);
            g.interval = null;
            g.inputText = null;
            g.value = null;
            g.textFieldID = "";
            if (this.tagName.toLowerCase() == "input" && this.type && this.type == "text") {
                g.inputText = $(this);
                if (this.id)
                    g.textFieldID = this.id;
            }
            else {
                g.inputText = $('<input type="text"/>');
                g.inputText.appendTo($(this));
            }
            if (g.textFieldID == "" && p.textFieldID)
                g.textFieldID = p.textFieldID;

            g.link = $('<div class="l-trigger"><div class="l-spinner-up"><div class="l-spinner-icon"></div></div><div class="l-spinner-split"></div><div class="l-spinner-down"><div class="l-spinner-icon"></div></div></div>');
            g.wrapper = g.inputText.wrap('<div class="l-text"></div>').parent();
            g.wrapper.append('<div class="l-text-l"></div><div class="l-text-r"></div>');
            g.wrapper.append(g.link).after(g.selectBox).after(g.valueField);
            g.link.up = $(".l-spinner-up", g.link);
            g.link.down = $(".l-spinner-down", g.link);
            g.inputText.addClass("l-text-field");

            //数据初始化
            if (p.width) {
                g.wrapper.css({ width: p.width });
                g.inputText.css({ width: p.width - 22 });
            }
            if (p.height) {
                g.wrapper.height(p.height);
                g.inputText.height(p.height - 2);
                g.link.height(p.height - 4);
            }
            if (p.disabled) {
                g.wrapper.addClass("l-text-disabled");
            }
            //初始化
            if (!po.isVerify(g.inputText.val())) {
                g.value = po.getDefaultValue();
                g.inputText.val(g.value);
            }
            //事件
            g.link.up.hover(function () {
                if (!p.disabled)
                    $(this).addClass("l-spinner-up-over");
            }, function () {
                clearInterval(g.interval);
                $(document).unbind("selectstart.spinner");
                $(this).removeClass("l-spinner-up-over");
            }).mousedown(function () {
                if (!p.disabled) {
                    po.uping();
                    g.interval = setInterval(po.uping, p.interval);
                    $(document).bind("selectstart.spinner", function () { return false; });
                }
            }).mouseup(function () {
                clearInterval(g.interval);
                //g.inputText.trigger("change").focus();
                $(document).unbind("selectstart.spinner");
            });
            g.link.down.hover(function () {
                if (!p.disabled)
                    $(this).addClass("l-spinner-down-over");
            }, function () {
                clearInterval(g.interval);
                $(document).unbind("selectstart.spinner");
                $(this).removeClass("l-spinner-down-over");
            }).mousedown(function () {
                if (!p.disabled) {
                    g.interval = setInterval(po.downing, p.interval);
                    $(document).bind("selectstart.spinner", function () { return false; });
                }
            }).mouseup(function () {
                clearInterval(g.interval);
                //g.inputText.trigger("change").focus();
                $(document).unbind("selectstart.spinner");
            });

            g.inputText.change(function () {
                var value = g.inputText.val();
                g.value = po.getVerifyValue(value);
                if (p.onChangeValue) {
                    p.onChangeValue(g.value);
                }
                g.inputText.val(g.value);
            }).blur(function () {
                g.wrapper.removeClass("l-text-focus");
            }).focus(function () {
                g.wrapper.addClass("l-text-focus");
            });
            g.wrapper.hover(function () {
                if (!p.disabled)
                    g.wrapper.addClass("l-text-over");
            }, function () {
                g.wrapper.removeClass("l-text-over");
            });
            $.ligerui.addManager(this, g);
        });
        return $.ligerui.getManager(this);
    };
})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($)
{
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager)
    {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr)
    {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.fn.ligerGetTabManager = function ()
    {
        return $.ligerui.getManager(this);
    };

    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Tab = {
        height: null,
        heightDiff: 0, // 高度补差 
        changeHeightOnResize: false,
        contextmenu: true,
        dblClickToClose: false, //是否双击时关闭
        closeMessage: "关闭当前页",
        closeOtherMessage: "关闭其他",
        closeAllMessage: "关闭所有",
        reloadMessage: "刷新",
        onBeforeOverrideTabItem: null,
        onAfterOverrideTabItem: null,
        onBeforeRemoveTabItem: null,
        onAfterRemoveTabItem: null,
        onBeforeAddTabItem: null,
        onAfterAddTabItem: null,
        onBeforeSelectTabItem: null,
        onAfterSelectTabItem: null,
        showSwitch: false,       //显示切换窗口按钮
        showSwitchInTab: false //切换窗口按钮显示在最后一项
    };
    //Tab manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.Tab = function (options, po)
    {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.Tab.prototype = {
        setShowSwitch: function (value) {
            var g = this, p = this.options;
            if (value) {
                if (!$(".l-tab-switch", g.tab.links).length) {
                    $("<div class='l-tab-switch'></div>").appendTo(g.tab.links);
                }
                $(g.tab).addClass("l-tab-switchable");
                $(".l-tab-switch", g.tab).click(function (e) {
                    g.toggleSwitch(this);
                });                
            }
            else {
                $(g.tab).removeClass("l-tab-switchable");
                $("body > .l-tab-windowsswitch").remove();
            }
        },
        setShowSwitchInTab: function (value) {
            var g = this, p = this.options;
            if (p.showSwitch && value) {
                $(g.tab).removeClass("l-tab-switchable");
                $(".l-tab-switch", g.tab).remove();
                var tabitem = $("<li class='l-tab-itemswitch'><a></a><div class='l-tab-links-item-left'></div><div class='l-tab-links-item-right'></div></li>");
                tabitem.appendTo(g.tab.links.ul);
                tabitem.click(function () {
                    g.toggleSwitch(this);
                });               
            } else {
                $(".l-tab-itemswitch", g.tab.ul).remove();
            }
        },
        toggleSwitch: function (btn) {
            var g = this, p = this.options;
            if ($("body > .l-tab-windowsswitch").length) {
                $("body > .l-tab-windowsswitch").remove();
                return;
            }
            if (btn == null) return;
            var windowsswitch = $("<div class='l-tab-windowsswitch'></div>").appendTo('body');
            var tabItems = g.tab.links.ul.find('>li');
            var selectedTabItemID = g.getSelectedTabItemID();
            var switchitems = [
                    { text: "关闭当前页", id: 'close'},
                    { text: "关闭全部", id: 'closeall' },
                    { text: "刷新当前页", id: 'reload' }
            ]
            windowsswitch.append($("<a href='javascript:void(0)' data-opt='closeall'>关闭全部页</a>"));
            windowsswitch.append($("<a href='javascript:void(0)' data-opt='closecur'>关闭当前页</a>"));
            windowsswitch.append($("<a href='javascript:void(0)' data-opt='reload'>刷新当前页</a>"));
           
            windowsswitch.css({
                top: $(btn).offset().top + $(btn).height(),
                left: $(btn).offset().left +$(btn).width() - windowsswitch.width()
            });
            windowsswitch.bind("click", function (e) {
                var obj = (e.target || e.srcElement);
                if (obj.tagName.toLowerCase() == "a") {
                    var opt = $(obj).attr("data-opt");
                                     
                    switch (opt)
                    {
                        case "closeall": g.removeAll(); break;
                        case "closecur": selectedTabItemID!= "home" &&  g.removeTabItem(selectedTabItemID); break;
                        case "reload": g.reload(selectedTabItemID); break;
                    }
                    
                    $("body > .l-tab-windowsswitch").remove();
                    return;
                }
            });
            $(document).click(function (e) {
                if (($(e.target).hasClass("l-tab-itemswitch")) || ($(e.target).parent().hasClass("l-tab-itemswitch"))) {
                    //$("body > .l-tab-windowsswitch").remove();
                }
                else {
                    $("body > .l-tab-windowsswitch").remove();
                }                
                e.stopPropagation();
            });
        },
        //设置tab按钮(左和右),显示返回true,隐藏返回false
        setTabButton: function ()
        {
            var po = this.po, g = this, p = this.options;
            var sumwidth = 0;
            $("li", g.tab.links.ul).each(function ()
            {
                sumwidth += $(this).width() + 2;
            });
            var mainwidth = g.tab.width();
            if (sumwidth > mainwidth)
            {
                if (!$(".l-tab-links-left", g.tab).length) {
                    g.tab.links.append('<div class="l-tab-links-left"></div><div class="l-tab-links-right"></div>');
                    g.setTabButtonEven();
                }
                return true;
            } else
            {
                g.tab.links.ul.animate({ left: 0 });
                $(".l-tab-links-left,.l-tab-links-right", g.tab.links).remove();
                return false;
            }
        },
        //设置左右按钮的事件 标签超出最大宽度时，可左右拖动
        setTabButtonEven: function ()
        {
            var po = this.po, g = this, p = this.options;
            $(".l-tab-links-left", g.tab.links).hover(function ()
            {
                $(this).addClass("l-tab-links-left-over");
            }, function ()
            {
                $(this).removeClass("l-tab-links-left-over");
            }).click(function ()
            {
                g.moveToPrevTabItem();
            });
            $(".l-tab-links-right", g.tab.links).hover(function ()
            {
                $(this).addClass("l-tab-links-right-over");
            }, function ()
            {
                $(this).removeClass("l-tab-links-right-over");
            }).click(function ()
            {
                g.moveToNextTabItem();
            });
        },
        //切换到上一个tab
        moveToPrevTabItem: function (tabid)
        {
            var po = this.po, g = this, p = this.options;
            var tabItems = $("> li", g.tab.links.ul),
                 nextBtn = $(".l-tab-links-right", g.tab),
                 prevBtn = $(".l-tab-links-left", g.tab);
            if (!nextBtn.length || !prevBtn.length) return false;
            var nextBtnOffset = nextBtn.offset(), prevBtnOffset = prevBtn.offset();
            //计算应该移动到的标签项,并计算从第一项到这个标签项的上一项的宽度总和
            var moveToTabItem = null, currentWidth = 0;
            var prevBtnLeft = prevBtnOffset.left + prevBtn.outerWidth();
            for (var i = 0, l = tabItems.length; i < l; i++) {
                var tabitem = $(tabItems[i]);
                var offset = tabitem.offset();
                var start = offset.left, end = offset.left + tabitem.outerWidth();
                if (tabid != null) {
                    if (start < prevBtnLeft && tabitem.attr("tabid") == tabid) {
                        moveToTabItem = tabitem;
                        break;
                    }
                }
                else if (start < prevBtnLeft && end >= prevBtnLeft) {
                    moveToTabItem = tabitem;
                    break;
                }
                currentWidth += tabitem.outerWidth() + parseInt(tabitem.css("marginLeft"))
                    + parseInt(tabitem.css("marginRight"));
            }
            if (moveToTabItem == null) return false;
            //计算出正确的移动位置
            var left = currentWidth - prevBtn.outerWidth();
            g.tab.links.ul.animate({ left: -1 * left });
            return true;
        },
        //切换到下一个tab
        moveToNextTabItem: function (tabid)
        {
            var po = this.po, g = this, p = this.options;
            var tabItems = $("> li", g.tab.links.ul),
                nextBtn = $(".l-tab-links-right", g.tab),
                prevBtn = $(".l-tab-links-left", g.tab);
            if (!nextBtn.length || !prevBtn.length) return false;
            var nextBtnOffset = nextBtn.offset(), prevBtnOffset = prevBtn.offset();
            //计算应该移动到的标签项,并计算从第一项到这个标签项的宽度总和
            var moveToTabItem = null, currentWidth = 0;
            for (var i = 0, l = tabItems.length; i < l; i++) {
                var tabitem = $(tabItems[i]);
                currentWidth += tabitem.outerWidth()
                    + parseInt(tabitem.css("marginLeft"))
                    + parseInt(tabitem.css("marginRight"));
                var offset = tabitem.offset();
                var start = offset.left, end = offset.left + tabitem.outerWidth();
                if (tabid != null) {
                    if (end > nextBtnOffset.left && tabitem.attr("tabid") == tabid) {
                        moveToTabItem = tabitem;
                        break;
                    }
                }
                else if (start <= nextBtnOffset.left && end > nextBtnOffset.left) {
                    moveToTabItem = tabitem;
                    break;
                }
            }
            if (moveToTabItem == null) return false;
            //计算出正确的移动位置
            var left = currentWidth - (nextBtnOffset.left - prevBtnOffset.left)
                + parseInt(moveToTabItem.css("marginLeft")) + parseInt(moveToTabItem.css("marginRight"));
            g.tab.links.ul.animate({ left: -1 * left });
            return true;
        },
        //切换到指定的项目项
        moveToTabItem: function (tabid) {
            var g = this, p = this.options;
            if (!g.moveToPrevTabItem(tabid)) {
                g.moveToNextTabItem(tabid);
            }
        },
        getTabItemCount: function ()
        {
            var po = this.po, g = this, p = this.options;
            return $("li", g.tab.links.ul).length;
        },
        getSelectedTabItemID: function ()
        {
            var po = this.po, g = this, p = this.options;
            return $("li.l-selected", g.tab.links.ul).attr("tabid");
        },
        removeSelectedTabItem: function ()
        {
            var po = this.po, g = this, p = this.options;
            g.removeTabItem(g.getSelectedTabItemID());
        },
        //覆盖选择的tabitem
        overrideSelectedTabItem: function (options)
        {
            var po = this.po, g = this, p = this.options;
            g.overrideTabItem(g.getSelectedTabItemID(), options);
        },
        //覆盖
        overrideTabItem: function (targettabid, options)
        {
            var po = this.po, g = this, p = this.options;
            if (p.onBeforeOverrideTabItem && p.onBeforeOverrideTabItem(targettabid) == false) return false;

            var tabid = options.tabid;
            if (tabid == undefined) tabid = g.getNewTabid();
            var url = options.url;
            var content = options.content;
            var target = options.target;
            var text = options.text;
            var showClose = options.showClose;
            var height = options.height;
            //如果已经存在
            if (g.isTabItemExist(tabid))
            {
                return;
            }
            var tabitem = $("li[tabid=" + targettabid + "]", g.tab.links.ul);
            var contentitem = $(".l-tab-content-item[tabid=" + targettabid + "]", g.tab.content);
            if (!tabitem || !contentitem) return;
            tabitem.attr("tabid", tabid);
            contentitem.attr("tabid", tabid);
            if ($("iframe", contentitem).length == 0 && url)
            {
                contentitem.html("<iframe frameborder='0'></iframe>");
            }
            else if (content)
            {
                contentitem.html(content);
            }
            $("iframe", contentitem).attr("name", tabid);
            if (showClose == undefined) showClose = true;
            if (showClose == false) $(".l-tab-links-item-close", tabitem).remove();
            else
            {
                if ($(".l-tab-links-item-close", tabitem).length == 0)
                    tabitem.append("<div class='l-tab-links-item-close'></div>");
            }
            if (text == undefined) text = tabid;
            if (height) contentitem.height(height);
            $("a", tabitem).text(text);
            $("iframe", contentitem).attr("src", url);


            p.onAfterOverrideTabItem && p.onAfterOverrideTabItem(targettabid);
        },
        //选中tab项
        selectTabItem: function (tabid)
        {
            var po = this.po, g = this, p = this.options;
            if (p.onBeforeSelectTabItem && p.onBeforeSelectTabItem(tabid) == false) return false;
            g.selectedTabId = tabid;
            $("> .l-tab-content-item[tabid=" + tabid + "]", g.tab.content).show().siblings().hide();
            $("li[tabid=" + tabid + "]", g.tab.links.ul).addClass("l-selected").siblings().removeClass("l-selected");
            p.onAfterSelectTabItem && p.onAfterSelectTabItem(tabid);
        },
        //移动到最后一个tab
        moveToLastTabItem: function ()
        {
            var po = this.po, g = this, p = this.options;
            var sumwidth = 0;
            $("li", g.tab.links.ul).each(function ()
            {
                sumwidth += $(this).width() + 2;
            });
            var mainwidth = g.tab.width();
            if (sumwidth > mainwidth)
            {
                var btnWitdth = $(".l-tab-links-right", g.tab.links).width();
                g.tab.links.ul.animate({ left: -1 * (sumwidth - mainwidth + btnWitdth + 2) });
            }
        },
        //判断tab是否存在
        isTabItemExist: function (tabid)
        {
            var po = this.po, g = this, p = this.options;
            return $("li[tabid=" + tabid + "]", g.tab.links.ul).length > 0;
        },
        //增加一个tab
        addTabItem: function (options)
        {
            var po = this.po, g = this, p = this.options;
            if (p.onBeforeAddTabItem && p.onBeforeAddTabItem(tabid) == false) return false;

            var tabid = options.tabid;
            if (tabid == undefined) tabid = g.getNewTabid();
            var url = options.url;
            var content = options.content;
            var text = options.text;
            var showClose = options.showClose;
            var height = options.height;
            //如果已经存在
            if (g.isTabItemExist(tabid))
            {
                g.selectTabItem(tabid);
                return;
            }
            var tabitem = $("<li><a></a><div class='l-tab-links-item-left'></div><div class='l-tab-links-item-right'></div><div class='l-tab-links-item-close'></div></li>");
            var contentitem = $("<div class='l-tab-content-item'><iframe frameborder='0'></iframe></div>");
            if (g.makeFullHeight)
            {
                var newheight = g.tab.height() - g.tab.links.height();
                contentitem.height(newheight);
            }
            tabitem.attr("tabid", tabid);
            contentitem.attr("tabid", tabid);
            $("iframe", contentitem).attr("name", tabid);
            if (showClose == undefined) showClose = true;
            if (showClose == false) $(".l-tab-links-item-close", tabitem).remove();
            if (text == undefined) text = tabid;
            if (height) contentitem.height(height);
            $("a", tabitem).text(text);
            $("iframe", contentitem).attr("src", url);
            //g.tab.links.ul.append(tabitem);

            if ($(".l-tab-itemswitch", g.tab.links.ul).length) {
                tabitem.insertBefore($(".l-tab-itemswitch", g.tab.links.ul));
            } else {
                g.tab.links.ul.append(tabitem);
            }

            g.tab.content.append(contentitem);
            g.selectTabItem(tabid);
            if (g.setTabButton())
            {
                g.moveToLastTabItem();
            }
            //增加事件
            g.addTabItemEvent(tabitem);
            g.toggleSwitch();
            p.onAfterAddTabItem && p.onAfterAddTabItem(tabid);
        },
        //增加一个div tab
        addDivTabItem: function (options) {
            var po = this.po, g = this, p = this.options;
            if (p.onBeforeAddTabItem && p.onBeforeAddTabItem(tabid) == false) return false;

            var tabid = options.tabid;
            if (tabid == undefined) tabid = g.getNewTabid();

            var content = options.content;
            var text = options.text;
            var showClose = options.showClose;
            var height = options.height;
            //如果已经存在
            if (g.isTabItemExist(tabid)) {
                g.selectTabItem(tabid);
                return;
            }
            var tabitem = $("<li><a></a><div class='l-tab-links-item-left'></div><div class='l-tab-links-item-right'></div><div class='l-tab-links-item-close'></div></li>");
            var contentitem = $("<div class='l-tab-content-item'><div class='new-content-div'></div></div>");
            if (g.makeFullHeight) {
                var newheight = g.tab.height() - g.tab.links.height();
                contentitem.height(newheight);
            }
            tabitem.attr("tabid", tabid);
            contentitem.attr("tabid", tabid);
            $(".new-content-div", contentitem).attr("id", "contentdiv-"+ tabid);
            if (showClose == undefined) showClose = true;
            if (showClose == false) $(".l-tab-links-item-close", tabitem).remove();
            if (text == undefined) text = tabid;
            if (height) contentitem.height(height);
            $("a", tabitem).text(text);
   
            //g.tab.links.ul.append(tabitem);
            if ($(".l-tab-itemswitch", g.tab.links.ul).length) {
                tabitem.insertBefore($(".l-tab-itemswitch", g.tab.links.ul));
            } else {
                g.tab.links.ul.append(tabitem);
            }

            g.tab.content.append(contentitem);
            g.selectTabItem(tabid);
            if (g.setTabButton()) {
                g.moveToLastTabItem();
            }
            //增加事件
            g.addTabItemEvent(tabitem);
            p.onAfterAddTabItem && p.onAfterAddTabItem(tabid);
        },
        addTabItemEvent: function (tabitem)
        {
            var po = this.po, g = this, p = this.options;
            tabitem.click(function ()
            {
                var tabid = $(this).attr("tabid");
                g.selectTabItem(tabid);
            });
            //过滤选择键
            if (!$(tabitem).attr("tabid")) return;  
            //右键事件支持
            g.tab.menu && po.addTabItemContextMenuEven(tabitem);
            $(".l-tab-links-item-close", tabitem).hover(function ()
            {
                $(this).addClass("l-tab-links-item-close-over");
            }, function ()
            {
                $(this).removeClass("l-tab-links-item-close-over");
            }).click(function ()
            {
                var tabid = $(this).parent().attr("tabid");
                g.removeTabItem(tabid);
            });

        },
        //移除tab项
        removeTabItem: function (tabid)
        {
            var po = this.po, g = this, p = this.options;
            if (p.onBeforeRemoveTabItem && p.onBeforeRemoveTabItem(tabid) == false) return false;
            var currentIsSelected = $("li[tabid=" + tabid + "]", g.tab.links.ul).hasClass("l-selected");
            if (currentIsSelected)
            {
                $(".l-tab-content-item[tabid=" + tabid + "]", g.tab.content).prev().show();
                $("li[tabid=" + tabid + "]", g.tab.links.ul).prev().addClass("l-selected").siblings().removeClass("l-selected");
            }
            var contentItem = $(".l-tab-content-item[tabid=" + tabid + "]", g.tab.content);
            var jframe = $('iframe', contentItem);
            if (jframe.length) {
                var frame = jframe[0];
                frame.src = "about:blank";
                try {
                    frame.contentWindow.document.write('');
                } catch (e) {
                }
                $.browser.msie && CollectGarbage();
                jframe.remove();
            }
            contentItem.remove();

            $("li[tabid=" + tabid + "]", g.tab.links.ul).remove();
            g.setTabButton();
            p.onAfterRemoveTabItem && p.onAfterRemoveTabItem(tabid);
        },
        addHeight: function (heightDiff)
        {
            var po = this.po, g = this, p = this.options;
            var newHeight = g.tab.height() + heightDiff;
            g.setHeight(newHeight);
        },
        setHeight: function (height)
        {
            var po = this.po, g = this, p = this.options;
            g.tab.height(height);
            g.setContentHeight();
        },
        setContentHeight: function ()
        {
            var po = this.po, g = this, p = this.options;
            var newheight = g.tab.height() - g.tab.links.height();
            g.tab.content.height(newheight);
            $("> .l-tab-content-item", g.tab.content).height(newheight);
        },
        getNewTabid: function ()
        {
            var po = this.po, g = this, p = this.options;
            g.getnewidcount = g.getnewidcount || 0;
            return 'tabitem' + (++g.getnewidcount);
        },
        //notabid 过滤掉tabid的
        //noclose 过滤掉没有关闭按钮的
        getTabidList: function (notabid, noclose)
        {
            var po = this.po, g = this, p = this.options;
            var tabidlist = [];
            $("> li", g.tab.links.ul).each(function ()
            {
                if ($(this).attr("tabid")
                        && $(this).attr("tabid") != notabid
                        && (!noclose || $(".l-tab-links-item-close", this).length > 0))
                {
                    tabidlist.push($(this).attr("tabid"));
                }
            });
            return tabidlist;
        },
        removeOther: function (tabid, compel)
        {
            var po = this.po, g = this, p = this.options;
            var tabidlist = g.getTabidList(tabid, true);
            $(tabidlist).each(function ()
            {
                g.removeTabItem(this);
            });
        },
        reload: function (tabid)
        {
            var po = this.po, g = this, p = this.options;
            $(".l-tab-content-item[tabid=" + tabid + "] iframe", g.tab.content).each(function (i, iframe)
            {
                $(iframe).attr("src", $(iframe).attr("src"));
            });
        },
        //add function
        //flushiframegrid: function (tabid) {
        //    var po = this.po, g = this, p = this.options;
        //    $(".l-tab-content-item[tabid=" + tabid + "] iframe", g.tab.content).each(function (i, iframe) {
        //        if (g.isTabItemExist(tabid))
        //        {
        //            //alert($(iframe));
        //            window.frames[tabid].f_reload();
        //        } 
        //    });
        //},
        removeAll: function (compel)
        {
            var po = this.po, g = this, p = this.options;
            var tabidlist = g.getTabidList(null, true);
            $(tabidlist).each(function ()
            {
                g.removeTabItem(this);
            });
        },
        onResize: function ()
        {
            var po = this.po, g = this, p = this.options;
            if (!p.height || typeof (p.height) != 'string' || p.height.indexOf('%') == -1) return false;
            //set tab height
            if (g.tab.parent()[0].tagName.toLowerCase() == "body")
            {
                var windowHeight = $(window).height();
                windowHeight -= parseInt(g.tab.parent().css('paddingTop'));
                windowHeight -= parseInt(g.tab.parent().css('paddingBottom'));
                g.height = p.heightDiff + windowHeight * parseFloat(g.height) * 0.01;
            }
            else
            {
                g.height = p.heightDiff + (g.tab.parent().height() * parseFloat(p.height) * 0.01);
            }
            g.tab.height(g.height);
            g.setContentHeight();
        }
    };

    $.fn.ligerTab = function (options)
    {

        this.each(function ()
        {
            if (this.applyligerui) return;
            var p = $.extend({}, $.ligerDefaults.Tab, options || {});
            var po = {
                menuItemClick: function (item)
                {
                    if (!item.id || !g.actionTabid) return;
                    switch (item.id)
                    {
                        case "close":
                            g.removeTabItem(g.actionTabid);
                            g.actionTabid = null;
                            break;
                        case "closeother":
                            g.removeOther(g.actionTabid);
                            break;
                        case "closeall":
                            g.removeAll();
                            g.actionTabid = null;
                            break;
                        case "reload":
                            g.selectTabItem(g.actionTabid);
                            g.reload(g.actionTabid);
                            break;
                    }
                },
                addTabItemContextMenuEven: function (tabitem)
                {
                    tabitem.bind("contextmenu", function (e)
                    {
                        if (!g.tab.menu) return;
                        g.actionTabid = tabitem.attr("tabid");
                        g.tab.menu.show({ top: e.pageY, left: e.pageX });
                        if ($(".l-tab-links-item-close", this).length == 0)
                        {
                            g.tab.menu.setDisabled('close');
                        }
                        else
                        {
                            g.tab.menu.setEnabled('close');
                        }
                        return false;
                    });
                }
            };
            var g = new $.ligerManagers.Tab(p, po);
            if (p.height) g.makeFullHeight = true;
            g.tab = $(this);
            if (!g.tab.hasClass("l-tab")) g.tab.addClass("l-tab");

            if (p.contextmenu && $.ligerMenu)
            {
                g.tab.menu = $.ligerMenu({ width: 100, items: [
                    { text: p.closeMessage, id: 'close', click: po.menuItemClick },
                    { text: p.closeOtherMessage, id: 'closeother', click: po.menuItemClick },
                    { text: p.closeAllMessage, id: 'closeall', click: po.menuItemClick },
                    { text: p.reloadMessage, id: 'reload', click: po.menuItemClick }
                ]
                });
            }

            g.tab.content = $('<div class="l-tab-content"></div>');
            $("> div", g.tab).appendTo(g.tab.content);
            g.tab.content.appendTo(g.tab);
            g.tab.links = $('<div class="l-tab-links"><ul style="left: 0px; "></ul><div class="l-tab-switch"></div></div>');
            g.tab.links.prependTo(g.tab);
            g.tab.links.ul = $("ul", g.tab.links);
            var haslselected = $("> div[lselected=true]", g.tab.content).length > 0;
            g.selectedTabId = $("> div[lselected=true]", g.tab.content).attr("tabid");
            $("> div", g.tab.content).each(function (i, box)
            {
                var li = $('<li class=""><a></a><div class="l-tab-links-item-left"></div><div class="l-tab-links-item-right"></div></li>');
                if ($(box).attr("title"))
                {
                    $("> a", li).html($(box).attr("title"));
                }
                var tabid = $(box).attr("tabid");
                if (tabid == undefined)
                {
                    tabid = g.getNewTabid();
                    $(box).attr("tabid", tabid);
                    if ($(box).attr("lselected"))
                    {
                        g.selectedTabId = tabid;
                    }
                }
                li.attr("tabid", tabid);
                if (!haslselected && i == 0) g.selectedTabId = tabid;
                var showClose = $(box).attr("showClose");
                if (showClose)
                {
                    li.append("<div class='l-tab-links-item-close'></div>");
                }
                $("> ul", g.tab.links).append(li);
                if (!$(box).hasClass("l-tab-content-item")) $(box).addClass("l-tab-content-item");
            });
            //init 
            g.selectTabItem(g.selectedTabId);
            g.setShowSwitch(p.showSwitch);
            g.setShowSwitchInTab(p.showSwitchInTab);
            g.toggleSwitch();

            //set content height
            if (p.height)
            {
                if (typeof (p.height) == 'string' && p.height.indexOf('%') > 0)
                {
                    g.onResize();
                    if (p.changeHeightOnResize)
                    {
                        $(window).resize(function ()
                        {
                            g.onResize();
                        });
                    }
                } else
                {
                    g.setHeight(p.height);
                }
            }
            if (g.makeFullHeight)
                g.setContentHeight();


            //add even 
            $("li", g.tab.links).each(function ()
            {
                g.addTabItemEvent($(this));
            });

            g.tab.bind('dblclick.tab', function (e) {
                if (!p.dblClickToClose) return;
                g.dblclicking = true;
                var obj = (e.target || e.srcElement);
                var tagName = obj.tagName.toLowerCase();
                if (tagName == "a") {
                    var tabid = $(obj).parent().attr("tabid");
                    var allowClose = $(obj).parent().find("div.l-tab-links-item-close").length ? true : false;
                    if (allowClose) {
                        g.removeTabItem(tabid);
                    }
                }
                g.dblclicking = false;
            });

            $.ligerui.addManager(this, g);
        });
        return $.ligerui.getManager(this);
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($) {
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager) {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr) {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.fn.ligerGetTextBoxManager = function () {
        return $.ligerui.getManager(this);
    };

    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.TextBox = {
        onChangeValue: null,
        width: null,
        disabled: false,
        value: null,     //初始化值 
        nullText: null,   //不能为空时的提示
        digits: false,     //是否限定为数字输入框
        number: false    //是否限定为浮点数格式输入框
    };

    //TextBox manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.TextBox = function (options, po) {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.TextBox.prototype = {
        checkValue: function () {
            var g = this, p = this.options;
            var v = g.inputText.val();
            if (p.number && !/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(v) || p.digits && !/^\d+$/.test(v)) {
                g.inputText.val(g.value || 0);
                return;
            }
            g.value = v;
        },
        setValue: function (value) {
            this.inputText.val(value);
        },
        getValue: function () {
            return this.inputText.val();
        },
        setEnabled: function () {
            this.inputText.removeAttr("readonly");
            this.wrapper.removeClass('l-text-disabled');
            this.options.disabled = false;
        },
        setDisabled: function () {
            this.inputText.attr("readOnly", "true");
            this.wrapper.addClass('l-text-disabled');
            this.options.disabled = true;
        }
    };

    ///	<param name="$" type="jQuery"></param>
    $.fn.ligerTextBox = function (options) {
        this.each(function () {
            if (typeof (this.applyligerui) == Boolean && this.applyligerui) return;
            var p = $.extend({}, options || {});
            if ($(this).attr("ligerui")) {
                try {
                    var attroptions = $(this).attr("ligerui");
                    if (attroptions.indexOf('{') != 0) attroptions = "{" + attroptions + "}";
                    eval("attroptions = " + attroptions + ";");
                    if (attroptions) p = $.extend({}, attroptions, p || {});
                }
                catch (e) { }
            }
            p = $.extend({}, $.ligerDefaults.TextBox, p || {});
            var po = {};
            var g = new $.ligerManagers.TextBox(p, po);

            g.inputText = $(this);

            //nulltext
            if (p.nullText && !p.disabled) {
                if (!g.inputText.val()) {
                    g.inputText.addClass("l-text-field-null").val(p.nullText);
                }
            }
            g.inputText.bind('blur.textBox', function () {
                if (p.nullText && !p.disabled) {
                    if (!g.inputText.val()) {
                        g.inputText.addClass("l-text-field-null").val(p.nullText);
                    }
                }
            }).bind('focus.textBox', function () {
                if (p.nullText) {
                    if ($(this).hasClass("l-text-field-null")) {
                        $(this).removeClass("l-text-field-null").val("");
                    }
                }
            });


            //外层
            g.wrapper = g.inputText.wrap('<div class="l-text"></div>').parent();
            g.wrapper.append('<div class="l-text-l"></div><div class="l-text-r"></div>');
            if (!g.inputText.hasClass("l-text-field"))
                g.inputText.addClass("l-text-field");
            if (!p.width) {
                p.width = g.inputText.width();
                //p.width = 140;
            }
            if (p.disabled) {
                g.inputText.attr("readonly", "readonly");
                g.wrapper.addClass("l-text-disabled");
            }
            g.wrapper.css({ width: p.width });
            g.inputText.css({ width: p.width - 4 });
            if (p.height) {
                g.wrapper.height(p.height);
                g.inputText.height(p.height - 2);
            }
            g.inputText
            .bind('blur.ligerTextBox', function () {
                g.checkValue();
                g.wrapper.removeClass("l-text-focus");
            }).bind('focus.ligerTextBox', function () {
                g.wrapper.addClass("l-text-focus");
            })
            .change(function () {
                if (p.onChangeValue) {
                    p.onChangeValue(this.value);
                }
            });
            g.wrapper.hover(function () {
                g.wrapper.addClass("l-text-over");
            }, function () {
                g.wrapper.removeClass("l-text-over");
            });

            if (p.initValue) {
                g.inputText.val(p.initValue);
            }
            if (p.disabled) {
                g.inputText.attr("readonly", "readonly");
                g.wrapper.addClass('l-text-disabled');
            }

            if (p.label) {
                g.labelwrapper = g.wrapper.wrap('<div class="l-labeltext"></div>').parent();
                g.labelwrapper.prepend('<div class="l-text-label" style="float:left;">' + p.label + '：&nbsp</div>');
                g.wrapper.css('float', 'left');
                if (!p.labelWidth) {
                    p.labelWidth = $('.l-text-label', g.labelwrapper).width();
                } else {
                    $('.l-text-label', g.labelwrapper).width(p.labelWidth);
                }
                $('.l-text-label', g.labelwrapper).height(g.wrapper.height());
                if (p.labelAlign) {
                    $('.l-text-label', g.labelwrapper).css('text-align', p.labelAlign);
                }
                g.labelwrapper.append('<br style="clear:both;" />');
                g.labelwrapper.width(p.labelWidth + p.width + 2);
            }

            $.ligerui.addManager(this, g);
        });
        return $.ligerui.getManager(this);
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/

(function ($)
{

    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Tip = {
        content: null,
        callback: null,
        width: 90,
        height: null,
        distanceX: 1,
        distanceY: -3,
        appendIdTo: null       //保存ID到那一个对象(jQuery)
    };

    var tipnumber = 0; 
    $.fn.ligerTip = function (options)
    { 
        this.each(function ()
        {
            var p = $.extend({}, $.ligerDefaults.Tip, options || {});
            var tip = null;
            p.content = p.content || this.title;
            var tipid = $(this).attr("ligerTipId");
            if (tipid)
            {
                tip = $("#" + tipid);
                if (p.content == "") tip.remove();
                else $(".l-verify-tip-content", tip).html(p.content);
            }
            else if (p.content)
            {
                tip = $('<div class="l-verify-tip"><div class="l-verify-tip-corner"></div><div class="l-verify-tip-content">' + p.content + '</div></div>');
                tip.attr("id", "ligeruitip" + tipnumber++);
                tip.appendTo('body');
            }
            if (!tip) return;
            tip.css({ left: $(this).offset().left + $(this).width() + p.distanceX, top: $(this).offset().top + p.distanceY }).show();
            $(this).attr("ligerTipId", tip.attr("id"));
            p.width && $("> .l-verify-tip-content", tip).width(p.width - 8);
            p.height && $("> .l-verify-tip-content", tip).width(p.height);
            p.appendIdTo && p.appendIdTo.attr("ligerTipId", tip.attr("id"));
            p.callback && p.callback(tip);
        });
        if (this.length == 0) return null;
        if (this.length == 1) return this[0].ligerTip;
        var tips = [];
        this.each(function ()
        {
            tips.push(this.ligerTip);
        });
        return tips;
    };
    $.fn.ligerHideTip = function (p)
    {
        return this.each(function ()
        {
            var tipid = $(this).attr("ligerTipId");
            if (tipid)
            {
                $("#" + tipid).remove();
                $("[ligerTipId=" + tipid + "]").removeAttr("ligerTipId");
            }
        });
    };
})(jQuery);

﻿/**
* jQuery ligerUI 1.1.0.1
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($) {
    //manager base
    $.ligerui = $.ligerui || {};
    $.ligerui.addManager = function (dom, manager) {
        if (dom.id == undefined || dom.id == "")
            dom.id = "ligerui" + (1000 + $.ligerui.ManagerCount);
        $.ligerui.ManagerCount++;
        $.ligerui.Managers[dom.id] = manager;
        dom.applyligerui = true;
    };
    $.ligerui.getManager = function (domArr) {
        if (domArr.length == 0) return null;
        return $.ligerui.Managers[domArr[0].id];
    };
    $.ligerui.Managers = $.ligerui.Managers || {};
    $.ligerui.ManagerCount = $.ligerui.ManagerCount || 0;

    $.fn.ligerGetToolBarManager = function () {
        return $.ligerui.getManager(this);
    };
    //Spinner manager design
    $.ligerManagers = $.ligerManagers || {};
    $.ligerManagers.ToolBar = function (options, po) {
        this.options = options;
        this.po = po;
    };
    $.ligerManagers.ToolBar.prototype = {
        addItem: function (item) {
            var po = this.po, g = this, p = this.options;
            if (item.type == "line") {
                g.toolBar.append('<div class="l-bar-separator"></div>');
                return;
            }
            else if (item.type == "textbox") {
                if (item.text)
                    g.toolBar.append('<div class="l-toolbar-item  l-panel-label">' + item.text + '</div>');
                g.toolBar.append('<div class="l-toolbar-item"><input type="text" id="' + item.id + '" name="' + item.id + '" style="width:' + item.width + 'px;"></div>');
                return;
            }
            else if (item.type == "text") {
                if (item.text)
                    g.toolBar.append('<div class="l-toolbar-item  l-panel-label">' + item.text + '</div>');
                return;
            }
            else if (item.type == "filter") {
                var ditem = $('<div class="l-toolbar-filter"><span></span><div class="l-panel-btn-l"></div><div class="l-panel-btn-r"></div></div>');
                if (item.icon) {
                    ditem.append("<div class='l-icon'></div>");
                    //ditem.css("background", "url(" + item.icon + ") no-repeat 3px 3px");
                    $(".l-icon", ditem).css({ "background": "url(" + item.icon + ") no-repeat 1px 3px", width: "18px", height: "18px" });
                }

                ditem.hover(function () {
                    $(this).addClass("l-panel-btn-over");
                    $(this).attr("title", item.title)
                }, function () {
                    $(this).removeClass("l-panel-btn-over");
                });

                g.toolBar.filter.append(ditem);
                item.click && ditem.click(function () { item.click(item); });

                return;
            }
            var ditem = $('<div class="l-toolbar-item l-panel-btn"><span></span><div class="l-panel-btn-l"></div><div class="l-panel-btn-r"></div></div>');
            g.toolBar.append(ditem);
            item.id && ditem.attr("toolbarid", item.id);
            if (item.icon) {
                ditem.append("<div class='l-icon'></div>");
                //ditem.css("background", "url(" + item.icon + ") no-repeat 3px 3px");
                $(".l-icon", ditem).css({ "background": "url(" + item.icon + ") no-repeat 1px 1px", width: "18px", height: "18px" });
                ditem.addClass("l-toolbar-item-hasicon");
            }
            item.text && $("span:first", ditem).html(item.text);
            item.expid && $("span:first", ditem).attr("id",item.expid);

            if (!item.disable) {
                ditem.addClass("l-toolbar-item-disable");
                ditem.attr("disabled", true)
            }
            else {
                item.click && ditem.click(function () { item.click(item); });

                if (item.type == "button") {
                    ditem.hover(function () {
                        $(this).addClass("l-panel-btn-over");
                    }, function () {
                        $(this).removeClass("l-panel-btn-over");
                    });
                }

            }
            if (item.type == "serchbtn") {
                ditem.hover(function () {
                    $(this).addClass("l-panel-btn-over");
                }, function () {
                    $(this).removeClass("l-panel-btn-over");
                }).click(function () {
                    var serchpanel = $(".az");
                    //serchpanel.css({ 'border-bottom': 'solid 1px #8DB2E3', 'border-top': 'solid 1px #8DB2E3' });
                    serchpanel.css('top', ditem.offset().top + ditem.height() + 4);

                    serchpanel.appendTo($(document.body));
                    if (serchpanel.css('display') == 'none') {
                        $(this).addClass("l-panel-btn-selected");
                        serchpanel.fadeIn(100)
                    }
                    else {
                        $(this).removeClass("l-panel-btn-selected");
                        serchpanel.fadeOut(100)
                    }
                    //this.click;
                })
            }
        },
        removeItem: function (id) {
            var po = this.po, g = this, p = this.options;
            if (!id) return;

            $("[toolbarid =" + id + "]", g.toolBar).remove();
        },
        removeAll: function () {
            var po = this.po, g = this, p = this.options;

            var idlist = g.getItemIdList();
           
            $(idlist).each(function (i, item) {                
                g.removeItem(this);
            })
        },
        getItemIdList: function ()
        {
            var po = this.po, g = this, p = this.options;
            var itemidlist = [];
            $(".l-toolbar-item", g.toolBar).each(function () {
                if ($(this).attr("toolbarid"))
                    itemidlist.push($(this).attr("toolbarid"));
            });
            return itemidlist;
        }
        
    };

    $.fn.ligerToolBar = function (options) {
        this.each(function () {
            if (this.applyligerui) return;
            var p = $.extend({}, options || {});
            var po = {};
            var g = new $.ligerManagers.ToolBar(p, po);
            g.toolBar = $(this);
            
            g.toolBar.append('<div class="l-toolbar-filters" ></div>');
            g.toolBar.filter = $(".l-toolbar-filters", g.toolBar);
            
            if (!g.toolBar.hasClass("l-toolbar")) g.toolBar.addClass("l-toolbar");
            if (p.background == false) g.toolBar.removeClass("l-toolbar");
            if (p.items) {

                $(p.items).each(function (i, item) {
                    g.addItem(item);
                });
            }
            $.ligerui.addManager(this, g);
            this.applyligerui = true;
        });
        return $.ligerui.getManager(this);
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
if (typeof (LigerUIManagers) == "undefined") LigerUIManagers = {};
(function ($) {
    ///	<param name="$" type="jQuery"></param>

    $.fn.ligerGetTreeManager = function () {
        return LigerUIManagers[this[0].id + "_Tree"];
    };
    $.ligerDefaults = $.ligerDefaults || {};
    $.ligerDefaults.Tree = {
        url: null,
        data: null,
        checkbox: true,
        autoCheckboxEven: true,
        parentIcon: 'folder',
        childIcon: 'leaf',
        textFieldName: 'text',
        attribute: ['id', 'url'],
        treeLine: true,            //是否显示line
        nodeWidth: 150,
        statusName: '__status',
        isLeaf: null,              //是否子节点的判断函数
        single: false,               //是否单选
        onBeforeExpand: function () { },
        onContextmenu: function () { },
        onExpand: function () { },
        onBeforeCollapse: function () { },
        onCollapse: function () { },
        onBeforeSelect: function () { },
        onSelect: function () { },
        onBeforeCancelSelect: function () { },
        onCancelselect: function () { },
        onCheck: function () { },
        onSuccess: function () { },
        onError: function () { },
        onClick: function () { },
        idFieldName: 'id',
        parentIDFieldName: null,
        topParentIDValue: 0,
        onBeforeAppend: function () { },        //加载数据前事件，可以通过return false取消操作
        onAppend: function () { },             //加载数据时事件，对数据进行预处理以后
        onAfterAppend: function () { },         //加载数据完事件
        slide: true,           //是否以动画的形式显示
        itemopen: false, //是否点击节点打开
        tabid: null,
        usericon: null,
        iconpath: '',
        canCancle: true//是否可以取消选择

    };

    $.fn.ligerTree = function (p) {
        if (p.single) p.autoCheckboxEven = false;
        this.each(function () {
            p = $.extend({}, $.ligerDefaults.Tree, p || {});
            if (this.usedTree) return;
            if ($(this).hasClass('l-hidden')) { return; }
            //public Object
            var g = {
                getData: function () {
                    return g.data;
                },
                gettree: function () {
                    return g.tree;
                },
                //是否包含子节点
                hasChildren: function (treenodedata) {
                    if (p.isLeaf) return p.isLeaf(treenodedata);
                    return treenodedata.children ? true : false;
                },
                isChildren: function (node) {
                    return node.data.children
                },
                //获取父节点
                getParentTreeItem: function (treenode, level) {
                    var treeitem = $(treenode);
                    if (treeitem.parent().hasClass("l-tree"))
                        return null;
                    if (level == undefined) {
                        if (treeitem.parent().parent("li").length == 0)
                            return null;
                        return treeitem.parent().parent("li")[0];
                    }
                    var currentLevel = parseInt(treeitem.attr("outlinelevel"));
                    var currenttreeitem = treeitem;
                    for (var i = currentLevel - 1; i >= level; i--) {
                        currenttreeitem = currenttreeitem.parent().parent("li");
                    }
                    return currenttreeitem[0];
                },
                getChecked: function () {
                    if (!p.checkbox) return null;
                    var nodes = [];
                    $(".l-checkbox-checked", g.tree).parent().parent("li").each(function () {
                        var treedataindex = parseInt($(this).attr("treedataindex"));
                        nodes.push({ target: this, data: po.getDataNodeByTreeDataIndex(g.data, treedataindex) });
                    });
                    return nodes;
                },
                getSelected: function () {
                    var node = {};
                    node.target = $(".l-selected", g.tree).parent("li")[0];
                    if (node.target) {
                        var treedataindex = parseInt($(node.target).attr("treedataindex"));
                        node.data = po.getDataNodeByTreeDataIndex(g.data, treedataindex);
                        return node;
                    }
                    return null;
                },
                //升级为父节点级别
                upgrade: function (treeNode) {
                    $(".l-note", treeNode).each(function () {
                        $(this).removeClass("l-note").addClass("l-expandable-open");
                    });
                    $(".l-note-last", treeNode).each(function () {
                        $(this).removeClass("l-note-last").addClass("l-expandable-open");
                    });
                    $("." + po.getChildNodeClassName(), treeNode).each(function () {
                        $(this)
                        .removeClass(po.getChildNodeClassName())
                        .addClass(po.getParentNodeClassName(true));
                    });
                },
                //降级为叶节点级别
                demotion: function (treeNode) {
                    if (!treeNode && treeNode[0].tagName.toLowerCase() != 'li') return;
                    var islast = $(treeNode).hasClass("l-last");
                    $(".l-expandable-open", treeNode).each(function () {
                        $(this).removeClass("l-expandable-open")
                        .addClass(islast ? "l-note-last" : "l-note");
                    });
                    $(".l-expandable-close", treeNode).each(function () {
                        $(this).removeClass("l-expandable-close")
                        .addClass(islast ? "l-note-last" : "l-note");
                    });
                    $("." + po.getParentNodeClassName(true), treeNode).each(function () {
                        $(this)
                        .removeClass(po.getParentNodeClassName(true))
                        .addClass(po.getChildNodeClassName());
                    });
                },
                collapseAll: function () {
                    $(".l-expandable-open", g.tree).click();
                },
                expandAll: function () {
                    $(".l-expandable-close", g.tree).click();
                },
                loadData: function (node, url, param) {
                    g.loading.show();
                    var ajaxtype = param ? "post" : "get";
                    param = param || [];
                    //请求服务器
                    $.ajax({
                        type: ajaxtype,
                        url: url,
                        data: param,
                        dataType: 'json',
                        success: function (data) {
                            if (!data) return;
                            g.loading.hide();
                            g.append(node, data);
                            if (p.onSuccess) p.onSuccess(data);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            try {
                                g.loading.hide();
                                if (p.onError)
                                    p.onError(XMLHttpRequest, textStatus, errorThrown);
                            }
                            catch (e) {

                            }
                        }
                    });
                },

                //add function flushData
                FlushNodeIcon: function () {
                    g.loading.show();
                    param = {};
                    //请求服务器
                    $.ajax({
                        type: 'post',
                        url: p.url,
                        data: param,
                        dataType: 'json',
                        success: function (data) {
                            g.loading.hide();
                            if (p.onSuccess) p.onSuccess(data);

                            for (var a in data) {
                                $("li", g.tree).each(function () {
                                    //alert($("> .l-body >img", $(this)).attr("src"));
                                    if (data[a].id == $(this).attr("id"))
                                        $("> .l-body >img", $(this)).attr("src", data[a].d_icon);
                                });
                                //alert($("> .l-body > img", nodedata[attr]).attr("src"));
                                //alert(nodedata[attr].id + "," + data[attr].d_icon);
                                //$("> .l-body >img", nodedata[attr]).attr("src", data[attr].d_icon);
                                //alert($("> .l-body >img", nodedata[attr]).attr("src"));


                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            try {
                                g.loading.hide();
                                if (p.onError)
                                    p.onError(XMLHttpRequest, textStatus, errorThrown);
                            }
                            catch (e) {

                            }
                        }
                    });

                },
                //add function flushData
                FlushData: function () {
                    g.loading.show();
                    param = {};
                    //请求服务器
                    $.ajax({
                        type: 'post',
                        url: p.url,
                        data: param,
                        dataType: 'json',
                        success: function (data) {
                            if (!data) return;
                            g.loading.hide();
                            g.append(null, data);
                            if (p.onSuccess) p.onSuccess(data);
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            try {
                                g.loading.hide();
                                if (p.onError)
                                    p.onError(XMLHttpRequest, textStatus, errorThrown);
                            }
                            catch (e) {

                            }
                        }
                    });

                },
                //清空
                clear: function () {
                    //g.tree.html("");
                    $("> li", g.tree).each(function () { g.remove(this); });
                },
                remove: function (treeNode) {
                    var treedataindex = parseInt($(treeNode).attr("treedataindex"));
                    var treenodedata = po.getDataNodeByTreeDataIndex(g.data, treedataindex);
                    if (treenodedata) po.setTreeDataStatus([treenodedata], 'delete');
                    var parentNode = g.getParentTreeItem(treeNode);
                    //复选框处理
                    if (p.checkbox) {
                        $(".l-checkbox", treeNode).remove();
                        po.setParentCheckboxStatus($(treeNode));
                    }
                    $(treeNode).remove();
                    if (parentNode == null) //代表顶级节点
                    {
                        parentNode = g.tree.parent();
                    }
                    //set parent
                    var treeitemlength = $("> ul > li", parentNode).length;
                    if (treeitemlength > 0) {
                        //遍历设置子节点
                        $("> ul > li", parentNode).each(function (i, item) {
                            if (i == 0 && !$(this).hasClass("l-first"))
                                $(this).addClass("l-first");
                            if (i == treeitemlength - 1 && !$(this).hasClass("l-last"))
                                $(this).addClass("l-last");
                            if (i == 0 && i == treeitemlength - 1 && !$(this).hasClass("l-onlychild"))
                                $(this).addClass("l-onlychild");
                            $("> div .l-note,> div .l-note-last", this)
                           .removeClass("l-note l-note-last")
                           .addClass(i == treeitemlength - 1 ? "l-note-last" : "l-note");
                            po.setTreeItem(this, { isLast: i == treeitemlength - 1 });
                        });
                    }

                },
                update: function (domnode, newnodedata) {
                    var treedataindex = parseInt($(domnode).attr("treedataindex"));
                    nodedata = po.getDataNodeByTreeDataIndex(g.data, treedataindex);
                    for (var attr in newnodedata) {
                        nodedata[attr] = newnodedata[attr];
                        if (attr == p.textFieldName) {
                            $("> .l-body > span", domnode).text(newnodedata[attr]);
                        }
                    }
                },
                //增加节点集合
                append: function (parentNode, newdata) {
                    if (p.onBeforeAppend && p.onBeforeAppend(parentNode, newdata) == false) return false;
                    if (!newdata || !newdata.length) return false;
                    if (p.idFieldName && p.parentIDFieldName)
                        newdata = po.convertData(newdata);
                    po.addTreeDataIndexToData(newdata);
                    po.setTreeDataStatus(newdata, 'add');

                    p.onAppend && p.onAppend(parentNode, newdata);

                    po.appendData(parentNode, newdata);
                    if (!parentNode)//增加到根节点
                    {
                        //remove last node class
                        if ($("> li:last", g.tree).length > 0)
                            po.setTreeItem($("> li:last", g.tree)[0], { isLast: false });

                        var gridhtmlarr = po.getTreeHTMLByData(newdata, 1, [], true);
                        gridhtmlarr[gridhtmlarr.length - 1] = gridhtmlarr[0] = "";
                        g.tree.append(gridhtmlarr.join(''));

                        $(".l-body", g.tree).hover(function () {
                            $(this).addClass("l-over");
                        }, function () {
                            $(this).removeClass("l-over");
                        });

                        po.upadteTreeWidth();
                        p.onAfterAppend && p.onAfterAppend(parentNode, newdata);
                        return;
                    }
                    var treeitem = $(parentNode);
                    var outlineLevel = parseInt(treeitem.attr("outlinelevel"));

                    var hasChildren = $("> ul", treeitem).length > 0;
                    if (!hasChildren) {
                        treeitem.append("<ul class='l-children'></ul>");
                        //设置为父节点
                        g.upgrade(parentNode);
                    }
                    //remove last node class  
                    if ($("> .l-children > li:last", treeitem).length > 0)
                        po.setTreeItem($("> .l-children > li:last", treeitem)[0], { isLast: false });

                    var isLast = [];
                    for (var i = 1; i <= outlineLevel - 1; i++) {
                        var currentParentTreeItem = $(g.getParentTreeItem(parentNode, i));
                        isLast.push(currentParentTreeItem.hasClass("l-last"));
                    }
                    isLast.push(treeitem.hasClass("l-last"));
                    var gridhtmlarr = po.getTreeHTMLByData(newdata, outlineLevel + 1, isLast, true);
                    gridhtmlarr[gridhtmlarr.length - 1] = gridhtmlarr[0] = "";
                    $(">.l-children", parentNode).append(gridhtmlarr.join(''));

                    po.upadteTreeWidth();

                    $(">.l-children .l-body", parentNode).hover(function () {
                        $(this).addClass("l-over");
                    }, function () {
                        $(this).removeClass("l-over");
                    });
                    p.onAfterAppend && p.onAfterAppend(parentNode, newdata);
                },
                cancelSelect: function (domNode) {
                    var treeitem = $(domNode);
                    var treedataindex = parseInt(treeitem.attr("treedataindex"));
                    var treenodedata = po.getDataNodeByTreeDataIndex(g.data, treedataindex);
                    var treeitembody = $(">div:first", treeitem);
                    if (p.checkbox)
                        $(".l-checkbox", treeitembody).removeClass("l-checkbox-checked").addClass("l-checkbox-unchecked");
                    else
                        treeitembody.removeClass("l-selected");
                    p.onCancelSelect && p.onCancelSelect({ data: treenodedata, target: treeitem[0] });
                },
                //选择节点(参数：条件函数、Dom节点或ID值)
                selectNode: function (selectNodeParm) {
                    var clause = null;
                    if (typeof (selectNodeParm) == "function") {
                        clause = selectNodeParm;
                    }
                    else if (typeof (selectNodeParm) == "object") {
                        var treeitem = $(selectNodeParm);
                        var treedataindex = parseInt(treeitem.attr("treedataindex"));
                        var treenodedata = po.getDataNodeByTreeDataIndex(g.data, treedataindex);
                        var treeitembody = $(">div:first", treeitem);
                        if (p.checkbox)
                            $(".l-checkbox", treeitembody).removeClass("l-checkbox-unchecked").addClass("l-checkbox-checked");
                        else
                            treeitembody.addClass("l-selected");

                        p.onSelect && p.onSelect({ data: treenodedata, target: treeitem[0] });
                        p.onBeforeSelect && p.onBeforeSelect({ data: treenodedata, target: treeitem[0] });
                        return;
                    }
                    else {
                        clause = function (data) {
                            if (!data[p.idFieldName]) return false;
                            return data[p.idFieldName].toString() == selectNodeParm.toString();
                        };
                    }
                    $("li", g.tree).each(function () {
                        var treeitem = $(this);
                        var treedataindex = parseInt(treeitem.attr("treedataindex"));
                        var treenodedata = po.getDataNodeByTreeDataIndex(g.data, treedataindex);
                        if (clause(treenodedata, treedataindex)) {
                            g.selectNode(this);
                        }
                        //                        else {
                        //                            g.cancelSelect(this);
                        //                        }
                    });

                },
                getTextByID: function (id) {
                    var data = g.getDataByID(id);
                    if (!data) return null;
                    return data[p.textFieldName];
                },
                getDataByID: function (id) {
                    var data = null;
                    $("li", g.tree).each(function () {
                        if (data) return;
                        var treeitem = $(this);
                        var treedataindex = parseInt(treeitem.attr("treedataindex"));
                        var treenodedata = po.getDataNodeByTreeDataIndex(g.data, treedataindex);
                        if (treenodedata[p.idFieldName].toString() == id.toString()) {
                            data = treenodedata;
                        }
                    });
                    return data;
                }
            };
            //private Object
            var po = {
                //根据数据索引获取数据
                getDataNodeByTreeDataIndex: function (data, treedataindex) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].treedataindex == treedataindex)
                            return data[i];
                        if (data[i].children) {
                            var targetData = po.getDataNodeByTreeDataIndex(data[i].children, treedataindex);
                            if (targetData) return targetData;
                        }
                    }
                    return null;
                },
                //根据ID获取数据
                getDataNodeByTreeDataID: function (data, treedataindex) {
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].id == treedataindex)
                            return data[i];
                        if (data[i].children) {
                            var targetData = po.getDataNodeByTreeDataID(data[i].children, treedataindex);
                            if (targetData) return targetData;
                        }
                    }
                    return null;
                },
                //设置数据状态
                setTreeDataStatus: function (data, status) {
                    $(data).each(function () {
                        this[p.statusName] = status;
                        if (this.children) {
                            po.setTreeDataStatus(this.children, status);
                        }
                    });
                },
                //设置data 索引
                addTreeDataIndexToData: function (data) {
                    $(data).each(function () {
                        if (this.treedataindex != undefined) return;
                        this.treedataindex = g.treedataindex++;
                        if (this.children) {
                            po.addTreeDataIndexToData(this.children);
                        }
                    });
                },
                //添加项到g.data
                appendData: function (treeNode, data) {
                    var treedataindex = parseInt($(treeNode).attr("treedataindex"));
                    var treenodedata = po.getDataNodeByTreeDataIndex(g.data, treedataindex);
                    if (g.treedataindex == undefined) g.treedataindex = 0;
                    if (treenodedata && treenodedata.children == undefined) treenodedata.children = [];
                    $(data).each(function (i, item) {
                        if (treenodedata)
                            treenodedata.children[treenodedata.children.length] = $.extend({}, item);
                        else
                            g.data[g.data.length] = $.extend({}, item);
                    });
                },
                setTreeItem: function (treeNode, options) {
                    if (!options) return;
                    var treeItem = $(treeNode);
                    var outlineLevel = parseInt(treeItem.attr("outlinelevel"));
                    if (options.isLast != undefined) {
                        if (options.isLast == true) {
                            treeItem.removeClass("l-last").addClass("l-last");
                            $("> div .l-note", treeItem).removeClass("l-note").addClass("l-note-last");
                            $(".l-children li", treeItem)
                            .find(".l-box:eq(" + (outlineLevel - 1) + ")")
                            .removeClass("l-line");
                        }
                        else if (options.isLast == false) {
                            treeItem.removeClass("l-last");
                            $("> div .l-note-last", treeItem).removeClass("l-note-last").addClass("l-note");

                            $(".l-children li", treeItem)
                            .find(".l-box:eq(" + (outlineLevel - 1) + ")")
                            .removeClass("l-line")
                            .addClass("l-line");
                        }
                    }
                },
                upadteTreeWidth: function () {
                    var treeWidth = g.maxOutlineLevel * 14;
                    if (p.checkbox) treeWidth += 14;
                    if (p.parentIcon || p.childIcon) treeWidth += 14;
                    treeWidth += p.nodeWidth;
                    g.tree.width(treeWidth);
                },
                getChildNodeClassName: function () {
                    return 'l-tree-icon-' + p.childIcon;
                },
                getParentNodeClassName: function (isOpen) {
                    var nodeclassname = 'l-tree-icon-' + p.parentIcon;
                    if (isOpen) nodeclassname += '-open';
                    return nodeclassname;
                },
                //根据data生成最终完整的tree html
                getTreeHTMLByData: function (data, outlineLevel, isLast, isExpand) {
                    if (g.maxOutlineLevel < outlineLevel)
                        g.maxOutlineLevel = outlineLevel;
                    isLast = isLast || [];
                    outlineLevel = outlineLevel || 1;
                    var treehtmlarr = [];
                    if (!isExpand) treehtmlarr.push('<ul class="l-children" style="display:none">');
                    else treehtmlarr.push("<ul class='l-children'>");
                    for (var i = 0; i < data.length; i++) {
                        var isFirst = i == 0;
                        var isLastCurrent = i == data.length - 1;
                        var isExpandCurrent = true;
                        if (data[i].isexpand == false || data[i].isexpand == "false") isExpandCurrent = false;

                        treehtmlarr.push('<li ');
                        if (data[i].treedataindex != undefined)
                            treehtmlarr.push('treedataindex="' + data[i].treedataindex + '" ');
                        if (isExpandCurrent)
                            treehtmlarr.push('isexpand=' + data[i].isexpand + ' ');
                        treehtmlarr.push('outlinelevel=' + outlineLevel + ' ');
                        //增加属性支持
                        for (var j = 0; j < g.sysAttribute.length; j++) {
                            if ($(this).attr(g.sysAttribute[j]))
                                data[dataindex][g.sysAttribute[j]] = $(this).attr(g.sysAttribute[j]);
                        }
                        for (var j = 0; j < p.attribute.length; j++) {
                            if (data[i][p.attribute[j]] != null)
                                treehtmlarr.push(p.attribute[j] + '="' + data[i][p.attribute[j]] + '" ');
                        }

                        treehtmlarr.push(' tabid="' + data[i][p.tabid] + '" ');

                        //css class
                        treehtmlarr.push('class="');
                        isFirst && treehtmlarr.push('l-first ');
                        isLastCurrent && treehtmlarr.push('l-last ');
                        isFirst && isLastCurrent && treehtmlarr.push('l-onlychild ');
                        treehtmlarr.push('"');
                        treehtmlarr.push('>');
                        treehtmlarr.push('<div class="l-body">');
                        for (var k = 0; k <= outlineLevel - 2; k++) {
                            if (isLast[k]) treehtmlarr.push('<div class="l-box"></div>');
                            else treehtmlarr.push('<div class="l-box l-line"></div>');
                        }
                        if (g.hasChildren(data[i])) {
                            if (isExpandCurrent) treehtmlarr.push('<div class="l-box l-expandable-open"></div>');
                            else treehtmlarr.push('<div class="l-box l-expandable-close"></div>');
                            if (p.checkbox) {
                                if (data[i].ischecked)
                                    treehtmlarr.push('<div class="l-box l-checkbox l-checkbox-checked"></div>');
                                else
                                    treehtmlarr.push('<div class="l-box l-checkbox l-checkbox-unchecked"></div>');
                            }
                            if (p.usericon == null) {
                                p.parentIcon && !isExpandCurrent && treehtmlarr.push('<div class="l-box ' + po.getParentNodeClassName() + '"></div>');
                                p.parentIcon && isExpandCurrent && treehtmlarr.push('<div class="l-box ' + po.getParentNodeClassName(true) + '"></div>');
                            } else {
                                treehtmlarr.push('<img style="height:16px;width:16px" src="' + p.iconpath + data[i][p.usericon] + '"/>');
                            }
                        }
                        else {
                            if (isLastCurrent) treehtmlarr.push('<div class="l-box l-note-last"></div>');
                            else treehtmlarr.push('<div class="l-box l-note"></div>');
                            if (p.checkbox) {
                                if (data[i].ischecked)
                                    treehtmlarr.push('<div class="l-box l-checkbox l-checkbox-checked"></div>');
                                else
                                    treehtmlarr.push('<div class="l-box l-checkbox l-checkbox-unchecked"></div>');
                            }
                            if (p.usericon == null) {
                                p.childIcon && treehtmlarr.push('<div class="l-box ' + po.getChildNodeClassName() + '"></div>');
                            } else {
                                treehtmlarr.push('<img style="height:16px;width:16px" src="' + p.iconpath + data[i][p.usericon] + '"/>');
                            }
                        }

                        treehtmlarr.push('<span>' + data[i][p.textFieldName] + '</span></div>');
                        if (g.hasChildren(data[i])) {
                            var isLastNew = [];
                            for (var k = 0; k < isLast.length; k++) {
                                isLastNew.push(isLast[k]);
                            }
                            isLastNew.push(isLastCurrent);
                            treehtmlarr.push(po.getTreeHTMLByData(data[i].children, outlineLevel + 1, isLastNew, isExpandCurrent).join(''));
                        }
                        treehtmlarr.push('</li>');
                    }
                    treehtmlarr.push("</ul>");
                    return treehtmlarr;

                },
                //根据简洁的html获取data
                getDataByTreeHTML: function (treeDom) {
                    var data = [];
                    $("> li", treeDom).each(function (i, item) {
                        var dataindex = data.length;
                        data[dataindex] =
                        {
                            treedataindex: g.treedataindex++
                        };
                        data[dataindex][p.textFieldName] = $("> span,> a", this).html();
                        for (var j = 0; j < g.sysAttribute.length; j++) {
                            if ($(this).attr(g.sysAttribute[j]))
                                data[dataindex][g.sysAttribute[j]] = $(this).attr(g.sysAttribute[j]);
                        }
                        for (var j = 0; j < p.attribute.length; j++) {
                            if ($(this).attr(p.attribute[j]))
                                data[dataindex][p.attribute[j]] = $(this).attr(p.attribute[j]);
                        }
                        if ($("> ul", this).length > 0) {
                            data[dataindex].children = po.getDataByTreeHTML($("> ul", this));
                        }
                    });
                    return data;
                },
                applyTree: function () {
                    g.data = po.getDataByTreeHTML(g.tree);
                    var gridhtmlarr = po.getTreeHTMLByData(g.data, 1, [], true);
                    gridhtmlarr[gridhtmlarr.length - 1] = gridhtmlarr[0] = "";
                    g.tree.html(gridhtmlarr.join(''));
                    po.upadteTreeWidth();
                    $(".l-body", g.tree).hover(function () {
                        $(this).addClass("l-over");
                    }, function () {
                        $(this).removeClass("l-over");
                    });
                },
                applyTreeEven: function (treeNode) {
                    $("> .l-body", treeNode).hover(function () {
                        $(this).addClass("l-over");
                    }, function () {
                        $(this).removeClass("l-over");
                    });
                },
                setTreeEven: function () {
                    p.onContextmenu && g.tree.bind("contextmenu", function (e) {
                        var obj = (e.target || e.srcElement);
                        var treeitem = null;
                        if (obj.tagName.toLowerCase() == "a" || obj.tagName.toLowerCase() == "span" || $(obj).hasClass("l-box"))
                            treeitem = $(obj).parent().parent();
                        else if ($(obj).hasClass("l-body"))
                            treeitem = $(obj).parent();
                        else if (obj.tagName.toLowerCase() == "li")
                            treeitem = $(obj);
                        if (!treeitem) return;
                        var treedataindex = parseInt(treeitem.attr("treedataindex"));
                        var treenodedata = po.getDataNodeByTreeDataIndex(g.data, treedataindex);
                        return p.onContextmenu({ data: treenodedata, target: treeitem[0] }, e);
                    });
                    g.tree.click(function (e) {
                        var obj = (e.target || e.srcElement);
                        var treeitem = null;
                        if (obj.tagName.toLowerCase() == "a" || obj.tagName.toLowerCase() == "span" || $(obj).hasClass("l-box") || obj.tagName.toLowerCase() == "img")
                            treeitem = $(obj).parent().parent();
                        else if ($(obj).hasClass("l-body"))
                            treeitem = $(obj).parent();
                        else
                            treeitem = $(obj);
                        if (!treeitem) return;
                        var treedataindex = parseInt(treeitem.attr("treedataindex"));
                        var treenodedata = po.getDataNodeByTreeDataIndex(g.data, treedataindex);
                        var treeitembtn = $(".l-body:first .l-expandable-open:first,.l-body:first .l-expandable-close:first", treeitem);
                        if (!$(obj).hasClass("l-checkbox")) {
                            if ($(">div:first", treeitem).hasClass("l-selected")) {
                                if (!p.canCancle) return;
                                if (p.onBeforeCancelSelect
                                && p.onBeforeCancelSelect({ data: treenodedata, target: treeitem[0] }) == false)
                                    return false;
                                $(">div:first", treeitem).removeClass("l-selected");
                                p.onCancelSelect && p.onCancelSelect({ data: treenodedata, target: treeitem[0] });
                            }
                            else {
                                if (p.onBeforeSelect
                                && p.onBeforeSelect({ data: treenodedata, target: treeitem[0] }) == false)
                                    return false;
                                $(".l-body", g.tree).removeClass("l-selected");
                                $(">div:first", treeitem).addClass("l-selected");
                                p.onSelect && p.onSelect({ data: treenodedata, target: treeitem[0] });
                            }
                        }
                        var itemopen;
                        if (p.itemopen) {
                            itemopen = treeitembtn;
                        }
                        else {
                            itemopen = $(obj);
                        }
                        //chekcbox even
                        if ($(obj).hasClass("l-checkbox")) {
                            if (p.autoCheckboxEven) {
                                //状态：未选中
                                if ($(obj).hasClass("l-checkbox-unchecked")) {
                                    $(obj).removeClass("l-checkbox-unchecked").addClass("l-checkbox-checked");
                                    $(".l-children .l-checkbox", treeitem)
                                    .removeClass("l-checkbox-incomplete l-checkbox-unchecked")
                                    .addClass("l-checkbox-checked");
                                    p.onCheck && p.onCheck({ data: treenodedata, target: treeitem[0] }, true);
                                }
                                    //状态：选中
                                else if ($(obj).hasClass("l-checkbox-checked")) {
                                    $(obj).removeClass("l-checkbox-checked").addClass("l-checkbox-unchecked");
                                    $(".l-children .l-checkbox", treeitem)
                                    .removeClass("l-checkbox-incomplete l-checkbox-checked")
                                    .addClass("l-checkbox-unchecked");
                                    p.onCheck && p.onCheck({ data: treenodedata, target: treeitem[0] }, false);
                                }
                                    //状态：未完全选中
                                else if ($(obj).hasClass("l-checkbox-incomplete")) {
                                    $(obj).removeClass("l-checkbox-incomplete").addClass("l-checkbox-checked");
                                    $(".l-children .l-checkbox", treeitem)
                                    .removeClass("l-checkbox-incomplete l-checkbox-unchecked")
                                    .addClass("l-checkbox-checked");
                                    p.onCheck && p.onCheck({ data: treenodedata, target: treeitem[0] }, true);
                                }
                                po.setParentCheckboxStatus(treeitem);
                            }
                        }
                            //判断是否点击图标打开  
                            //状态：已经张开
                        else if (itemopen.hasClass("l-expandable-open")) {
                            if (p.onBeforeCollapse
                            && p.onBeforeCollapse({ data: treenodedata, target: treeitem[0] }) == false)
                                return false;
                            itemopen
                            .removeClass("l-expandable-open")
                            .addClass("l-expandable-close");
                            $("> .l-children", treeitem).slideToggle('fast');
                            $("> div ." + po.getParentNodeClassName(true), treeitem)
                            .removeClass(po.getParentNodeClassName(true))
                            .addClass(po.getParentNodeClassName());
                            p.onCollapse && p.onCollapse({ data: treenodedata, target: treeitem[0] });
                        }
                            //状态：没有张开
                        else if (itemopen.hasClass("l-expandable-close")) {
                            if (p.onBeforeExpand
                            && p.onBeforeExpand({ data: treenodedata, target: treeitem[0] }) == false)
                                return false;
                            itemopen
                            .removeClass("l-expandable-close")
                            .addClass("l-expandable-open");
                            $("> .l-children", treeitem).slideToggle('fast', function () {
                                p.onExpand && p.onExpand({ data: treenodedata, target: treeitem[0] });
                            });
                            $("> div ." + po.getParentNodeClassName(), treeitem)
                            .removeClass(po.getParentNodeClassName())
                            .addClass(po.getParentNodeClassName(true));
                        }

                        p.onClick && p.onClick({ data: treenodedata, target: treeitem[0] });
                    });
                },
                //递归设置父节点的状态
                setParentCheckboxStatus: function (treeitem) {
                    //当前同级别或低级别的节点是否都选中了
                    var isCheckedComplete = $(".l-checkbox-unchecked", treeitem.parent()).length == 0;
                    //当前同级别或低级别的节点是否都没有选中
                    var isCheckedNull = $(".l-checkbox-checked", treeitem.parent()).length == 0;
                    if (isCheckedComplete) {
                        treeitem.parent().prev().find(".l-checkbox")
                                    .removeClass("l-checkbox-unchecked l-checkbox-incomplete")
                                    .addClass("l-checkbox-checked");
                    }
                    else if (isCheckedNull) {
                        treeitem.parent().prev().find("> .l-checkbox")
                                    .removeClass("l-checkbox-checked l-checkbox-incomplete")
                                    .addClass("l-checkbox-unchecked");
                    }
                    else {
                        treeitem.parent().prev().find("> .l-checkbox")
                                    .removeClass("l-checkbox-unchecked l-checkbox-checked")
                                    .addClass("l-checkbox-incomplete");
                    }
                    if (treeitem.parent().parent("li").length > 0)
                        po.setParentCheckboxStatus(treeitem.parent().parent("li"));
                },
                convertData: function (data)      //将ID、ParentID这种数据格式转换为树格式
                {
                    if (!data || !data.length) return [];
                    var isolate = function (pid)//根据ParentID判断是否孤立
                    {
                        if (pid == p.topParentIDValue) return false;
                        for (var i = 0; i < data.length; i++) {
                            if (data[i][p.idFieldName] == pid) return false;
                        }
                        return true;
                    };
                    //计算孤立节点的个数
                    var isolateLength = 0;
                    for (var i = 0; i < data.length; i++) {
                        if (isolate(data[i][p.parentIDFieldName])) isolateLength++;
                    }
                    var targetData = [];                    //存储数据的容器(返回)
                    var itemLength = data.length;           //数据集合的个数
                    var insertedLength = 0;                 //已插入的数据个数
                    var currentIndex = 0;                   //当前数据索引
                    var getItem = function (container, id)    //获取数据项(为空时表示没有插入)
                    {
                        if (!container.length) return null;
                        for (var i = 0; i < container.length; i++) {
                            if (container[i][p.idFieldName] == id) return container[i];
                            if (container[i].children) {
                                var finditem = getItem(container[i].children, id);
                                if (finditem) return finditem;
                            }
                        }
                        return null;
                    };
                    var addItem = function (container, item)  //插入数据项
                    {
                        container.push($.extend({}, item));
                        insertedLength++;
                    };
                    //判断已经插入的节点和孤立节点 的个数总和是否已经满足条件
                    while (insertedLength + isolateLength < itemLength) {
                        var item = data[currentIndex];
                        var id = item[p.idFieldName];
                        var pid = item[p.parentIDFieldName];
                        if (pid == p.topParentIDValue)//根节点
                        {
                            getItem(targetData, id) == null && addItem(targetData, item);
                        }
                        else {
                            var pitem = getItem(targetData, pid);
                            if (pitem && getItem(targetData, id) == null)//找到父节点数据并且还没插入
                            {
                                pitem.children = pitem.children || [];
                                addItem(pitem.children, item);
                            }
                        }
                        currentIndex = (currentIndex + 1) % itemLength;
                    }
                    return targetData;
                }
            };
            if (!$(this).hasClass('l-tree')) $(this).addClass('l-tree');
            g.tree = $(this);
            if (!p.treeLine) g.tree.addClass("l-tree-noline");
            g.sysAttribute = ['isexpand', 'ischecked', 'href', 'style', 'tabid'];
            g.loading = $("<div class='l-tree-loading'></div>");
            g.tree.after(g.loading);
            g.data = [];
            g.maxOutlineLevel = 1;
            g.treedataindex = 0;
            po.applyTree();
            po.setTreeEven();
            if (p.data) {
                g.append(null, p.data);
            }
            if (p.url) {
                g.loadData(null, p.url);
            }
            if (this.id == undefined || this.id == "") this.id = "LigerUI_" + new Date().getTime();
            LigerUIManagers[this.id + "_Tree"] = g;
            this.usedTree = true;
        });
        if (this.length == 0) return null;
        if (this.length == 1) return LigerUIManagers[this[0].id + "_Tree"];
        var managers = [];
        this.each(function () {
            managers.push(LigerUIManagers[this.id + "_Tree"]);
        });
        return managers;
    };

})(jQuery);﻿/**
* jQuery ligerUI 1.1.0
* 
* Author leoxie [ gd_star@163.com ] 
* 
*/
(function ($)
{
    ///	<param name="$" type="jQuery"></param>
    $.fn.ligerApplyWindow = function (p)
    {
        return this.each(function ()
        {
            p = $.extend({
                showClose: true,
                showMax: true,
                showToggle: true
            }, p || {});
            var g = {};
            g.window = $('<div class="l-window"><div class="l-window-header"><div class="l-window-header-buttons"><div class="l-window-toggle"></div><div class="l-window-max"></div><div class="l-window-close"></div><div class="l-clear"></div></div><div class="l-window-header-inner"></div></div><div class="l-window-content"></div></div>');
            g.window.content = $(".l-window-content", g.window);
            g.window.header = $(".l-window-header", g.window);
            $(this).appendTo(g.window.content);
            $.ligerWindow.switchWindow(g.window[0]);
            $('body').append(g.window);
            //设置参数属性
            p.left && g.window.css('left', p.left);
            p.right && g.window.css('right', p.right);
            p.top && g.window.css('top', p.top);
            p.bottom && g.window.css('bottom', p.bottom);
            p.width && g.window.width(p.width);
            p.height && g.window.content.height(p.height - 28);
            p.title && $(".l-window-header-inner", g.window.header).html(p.title);
            p.framename && $(">iframe", g.window.content).attr('name', p.framename);
            if (!p.showToggle) $(".l-window-toggle", g.window).remove();
            if (!p.showMax) $(".l-window-max", g.window).remove();
            if (!p.showClose) $(".l-window-close", g.window).remove();
            //拖动支持
            if ($.fn.ligerDrag)
            {
                g.window.ligerDrag({ handler: '.l-window-header', onStartDrag: function ()
                {
                    $.ligerWindow.switchWindow(g.window[0]);
                    g.window.addClass("l-window-dragging");
                    g.window.content.children().hide();
                }, onStopDrag: function ()
                {
                    g.window.removeClass("l-window-dragging");
                    g.window.content.children().show();
                }
                });
            }
            //改变大小支持
            if ($.fn.ligerResizable)
            {

                g.window.ligerResizable({
                    onStartResize: function ()
                    {
                        $.ligerWindow.switchWindow(g.window[0]);
                        if ($(".l-window-max", g.window).hasClass("l-window-regain"))
                        {
                            $(".l-window-max", g.window).removeClass("l-window-regain");
                        }
                    },
                    onStopResize: function (current, e)
                    {
                        var top = 0;
                        var left = 0;
                        if (!isNaN(parseInt(g.window.css('top'))))
                            top = parseInt(g.window.css('top'));
                        if (!isNaN(parseInt(g.window.css('left'))))
                            left = parseInt(g.window.css('left'));
                        if (current.diffTop != undefined)
                        {
                            g.window.css({
                                top: top + current.diffTop,
                                left: left + current.diffLeft,
                                width: current.newWidth
                            });
                            g.window.content.height(current.newHeight - 28);
                        }
                        return false;
                    }
                });
                g.window.append("<div class='l-btn-nw-drop'></div>");
            }
            //设置事件 
            $(".l-window-toggle", g.window).click(function ()
            {
                if ($(this).hasClass("l-window-toggle-close"))
                {
                    $(this).removeClass("l-window-toggle-close");
                } else
                {
                    $(this).addClass("l-window-toggle-close");
                }
                g.window.content.slideToggle();
            });
            $(".l-window-close", g.window).click(function ()
            {
                if (p.onClose && p.onClose() == false) return false;
                g.window.hide();
            });
            $(".l-window-max", g.window).click(function ()
            {
                if ($(this).hasClass("l-window-regain"))
                {
                    if (p.onRegain && p.onRegain() == false) return false;
                    g.window.width(g.lastWindowWidth).css({ left: g.lastWindowLeft, top: g.lastWindowTop });
                    g.window.content.height(g.lastWindowHeight - 28);
                    $(this).removeClass("l-window-regain");
                }
                else
                {
                    if (p.onMax && p.onMax() == false) return false;
                    g.lastWindowWidth = g.window.width();
                    g.lastWindowHeight = g.window.height();
                    g.lastWindowLeft = g.window.css('left');
                    g.lastWindowTop = g.window.css('top');
                    g.window.width($(window).width() - 2).css({ left: 0, top: 0 });
                    g.window.content.height($(window).height() - 28);
                    $(this).addClass("l-window-regain");
                }
            });
        });
    }

    $.ligerWindow = {};
    $.ligerWindow.switchWindow = function (window)
    {
        $(window).css("z-index", "101").siblings(".l-window").css("z-index", "100");
    };
    $.ligerWindow.show = function (p)
    {
        p = p || {};
        if (p.url)
        {
            var iframe = $("<iframe frameborder='0' src='" + p.url + "'></iframe>");
            var framename = "window" + new Date().getTime();
            if (p.name) framename = p.name;
            iframe.attr("name", framename);
            p.framename = framename;
            iframe.ligerApplyWindow($.extend({}, p));
        }
        else if (p.content)
        {
            var content = $("<div>" + p.content + "</div>");
            content.ligerApplyWindow($.extend({}, p));
        }
        else if (p.target)
        {
            p.target.ligerApplyWindow($.extend({}, p));
        }
    };
})(jQuery);